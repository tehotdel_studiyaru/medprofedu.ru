<?php
ini_set('display_errors',0);

error_reporting(E_ALL);
if (get_magic_quotes_gpc()) {
	$process = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
	while (list($key, $val) = each($process)) {
		foreach ($val as $k => $v) {
			unset($process[$key][$k]);
			if (is_array($v)) {
				$process[$key][stripslashes($k)] = $v;
				$process[] = &$process[$key][stripslashes($k)];
			} else {
				$process[$key][stripslashes($k)] = stripslashes($v);
			}
		}
	}
	unset($process);
}
$path = getenv ( 'REQUEST_URI' );
function htmlspecialchar($string, $quote_style=null, $charset='cp1251'){
	return htmlspecialchars($string, $quote_style, $charset);
}


session_start ();
session_cache_limiter ( 'nocache' );
header ( 'Content-Type: text/html; charset=windows-1251' );
require ("includes/Engine.php");

$glb = new Redactor_Action ();

$glb->view ();