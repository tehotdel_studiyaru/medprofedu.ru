<?php 
$this->setLastModified();
$queryParams = array();
$this->setFileTemplate('catalog');
$mainParams = array('module'=>'catalog');
if (isset($_GET['showall'])){
	$this->setLimitRecords(false);
}
if ($this->getCurrentID()){
	//$this->setFileTemplate('card');
	$this->setView('card');
}
elseif ($this->getCurrentCategory()!=0){
	
	$mainParams['catid'] = $this->getCurrentCategory();
}

$this->setBreadcrumbs();
$this->set('mainParams', $mainParams);
$this->set('queryParams', $queryParams);