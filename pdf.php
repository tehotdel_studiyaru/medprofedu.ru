<?php
session_start ();
session_cache_limiter ( 'nocache' );
include ('MPDF/mpdf.php');
require ("includes/Engine.php");
$glb = new Redactor_Action ();
$glb->ConnectDB (); 
$catalog =<<<HTML
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="EN" lang="EN"
	dir="ltr">
<head profile="http://gmpg.org/xfn/11">

<meta http-equiv="Content-Type" content="text/html; charset=windows-1251" />
<meta http-equiv="imagetoolbar" content="no" />

<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1251">


<link rel="stylesheet" href="/template/default/styles/layout.css"
	type="text/css" />
<!--  <link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700' rel='stylesheet' type='text/css'>  -->
<!-- Homepage Specific Elements -->
<script type="text/javascript"
	src="/template/default/scripts/jquery-1.4.1.min.js"></script>
<script type="text/javascript"
	src="/template/default/scripts/jquery.easing.1.3.js"></script>
<script type="text/javascript"
	src="/template/default/scripts/jquery.timers.1.2.js"></script>
<script type="text/javascript"
	src="/template/default/scripts/jquery.galleryview.2.1.1.min.js"></script>
<script type="text/javascript"
	src="/template/default/scripts/jquery.galleryview.setup.js"></script>
<!-- End Homepage Specific Elements -->
<style>
.schedule td, .schedule th {
	vertical-align: top;
	padding: 10px;
	border: 1px solid grey;
	color: #333;
	font-size:14px;
}
.schedule th {
	font-weight:bold;
	font-size:14px;
	text-align:center;
}
.schedule table {
	border-collapse: collapse;
	border-spacing: 0;
	border: 0px;
	width: 100%;
}
.schedule .firstCat {
		font-size:25px !important;
	text-align:center;
	font-weight:bold;
}
.schedule .secondCat {
	font-size:18px !important;
	text-align:left;
	font-weight:bold;
}
.mainLabel {
	font-size:20px !important;
	text-align:center;
	font-weight:bold;
	
}

.schedule .descriptionSecondCategory p {
		margin: 0px;
margin-bottom: 10px
		}
.mainLabel span {
	font-size:12px  !important;
}
</style>
</head>

<body id="top">
<div class="wrapper row3">
	<div id="container" class="clear">
HTML;
$c = $glb->getModule ( 'catalog' );
$c->noPDF = true;
$c->View = 'list';
$c->getView();

$catalog .= $c->over;
$catalog.="</div></div></body></html>";
 

$pdf = new mPDF ( 'utf-8' );
$pdf->charset_in = 'cp1251';
	
$pdf->SetDisplayMode ( 'fullpage' );
$pdf->writeHTML ( $catalog );
$pdf->Output ( "schedule.pdf", 'D' );


