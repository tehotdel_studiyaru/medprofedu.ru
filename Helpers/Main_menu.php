<?php
class Redactor_Helper_Main_menu extends Redactor_Helper {
	var $over = "";
	var $li = array ();
	var $a = array ();
	var $idPages = array ();
	static $Pages = array ();
	function isLiSelect($id) {
		if (in_array ( $id, $this->idPages )) {
			//return " style='background: url(/bg33a.gif) no-repeat left top'";
		}
		return '';
	}
	
	function isASelect($id) {
		if (in_array ( $id, $this->idPages )) {
			return " class='curr2'";
		}
		return '';
	}
	function getChilds($id) {
		$id = ( int ) $id;
		if ($id == 0) {
			return true;
		}
		$this->idPages [] = ( int ) $id;
		$sql = mysql_query ( "select `parentId` from `pages` where `Id`='{$id}'" );
		if (is_resource ( $sql ) && mysql_num_rows ( $sql ) > 0) {
			while ( ($row = mysql_fetch_assoc ( $sql )) != false ) {
				$this->getChilds ( $row ['parentId'] );
			}
		}
	}
	
	function __construct() {
		if (class_exists ( 'pages' ) && ! is_null ( pages::$PageRow )) {
			if (pages::$PageRow ['OnIndex'] == 1) {
				$this->idPages [] = '/';
			} else {
				$this->getChilds ( pages::$PageRow ['Id'] );
			}
		} elseif (class_exists('shop')){
			if (shop::$New==true){
				$this->idPages[] = 'new';
				$this->idPages[] = 'new'.shop::$FirstCat;
				foreach (shop::$Cats as $cat){
					$this->idPages[] ='new'.$cat;
				}
			}
			elseif (shop::$Sale==true){
				$this->idPages[] = 'sale';
				$this->idPages[] = 'sale'.shop::$FirstCat;
				foreach (shop::$Cats as $cat){
					$this->idPages[] ='sale'.$cat;
				}
			}
			else {
				$this->idPages[] = 'cat'.shop::$FirstCat;
				foreach (shop::$Cats as $cat){
					$this->idPages[] ='cat'.$cat;
				}
			}
			
			
		}
		elseif (class_exists('comments')){
			
				$this->idPages[] = 'reviews';
			
		}
		elseif (class_exists('help')){
				
			$this->idPages[] = 'help';
				
		}
		
	   
		self::$Pages = $this->idPages;
		
		$this->over =<<<HTML
		<div id="tabs">
            <ul>
                <li><a href="#tabs-1" onclick="location.replace('/')" ><div class="home-tab"></div></a></li>
                <li><a href="#tabs-2" class="first-tab">��������</a></li>
                <li><a href="#tabs-3">��������</a></li>
                <li><a href="#tabs-4">����������</a></li>
            </ul>
            <nav class="tabs-div">
                <ul id="tabs-1">
                <li></li>
                </ul>
                <ul id="tabs-2">
                	{$this->getHTMLChilds(1)}
                </ul>
                <ul id="tabs-3">
                	{$this->getHTMLChilds(7)}
                </ul>
                <ul id="tabs-4">
                	{$this->getHTMLChilds(22)}
                </ul>
            </nav>
        </div>
		
HTML;
		
	
	}
	function getHTMLChilds($id){
		$out = '';
		$sql = $this->query("select `id`, `name` from `shop_cat` where `parentId`='{$id}' order by `pos`");
		$cats = class_exists('shop')?shop::$Cats:array();
		if ($sql && $sql->rowCount()>0){
			foreach ( $sql->fetchAll ( PDO::FETCH_ASSOC ) as $row ) {
				$url = rewriteUrls::getUrl ( array (
						'module' => 'shop',
						'catid' => $row ['id']
				) );
				$cls = '';
				if (in_array($row['id'], $cats)){
					$cls = ' class="active"';
				}
				$out.='<li><a href="'.$url.'"'.$cls.'>'.$row['name'].'</a></li>';
			}
		}
		return $out;
	}
}