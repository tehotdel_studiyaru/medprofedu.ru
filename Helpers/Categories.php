<?php
class Redactor_Helper_Categories extends Redactor_Helper {
	var $over = '';
	function __construct() {
		$out = '';
		$module = $this->getModule ( 'Catalog' );
		if ($module != false) {
			$cats = $module->getCategories ( 0 );
			
			if ($cats != false) {
				$out .= '	<p class="lTitle">���� ����</p>
			<ul>';
				$catsIds = array();
				$module->getParentsId($module->getCurrentCategory(), $catsIds);
				foreach ( $cats as $row ) {
					$cls = '';
					if ($module->getCurrentCategory()==$row->Id){
						$cls = ' class="open"';
					}
					$out .= '<li'.$cls.'><a href="'.$this->getUrl(array('module'=>'catalog', 'catid'=>$row->Id)).'">'.$row->Title.'</a>';
					if (in_array($row->Id,$catsIds) && $module->hasCategoryChildren($row->Id) > 0) {
						$out .= $this->getChilds ( $row->Id );
					}
					$out .= '</li>';
				}
				$out .= '	</ul>';
			}
		}
		$this->over = $out;
	}
	function getChilds($id, $sub=false) {
		$out = '';
		$module = $this->getModule ( 'Catalog' );
		
		if ($module != false) {
			
			$cats = $module->getCategories ( $id );
			
			if ($cats != false) {
				$out.='<ul>';
				foreach ( $cats as $row ) {
					$out .= '<li><a href="'.$this->getUrl(array('module'=>'catalog', 'catid'=>$row->Id)).'">'.$row->Title.'</a>';
					if ($module->hasCategoryChildren($row->Id) > 0) {
						$out .= $this->getChilds ( $row->Id );
					}
					$out .= '</li>';
				}
				$out .= '	</ul>';
			}
		}
		return $out;
	}
} 