<?php
require_once 'modules/Catalog.cls.php';
class Redactor_Helper_Catalog_index extends Redactor_Helper {
	var $over = "";
	function __construct() {
		$sql = $this->query ( "select `Id`,`Title` from `Catalog_Items` where `CategoryID`!=0  and LENGTH(TRIM(`uf_2`))>0 order by rand() limit 12" );
		$out = "";
		if ($sql != false && $sql->rowCount () > 0) {
			
			$out .= '<div class="galleryHolder">
		<ul id="galleryMainPage" class="jcarousel-skin-tango">';
			$i = 0;
			$v = 0;
			
			foreach ( $sql->fetchAll (  ) as $row ) {
				$row = new Redactor_Module_Catalog_Item($row);
				$image = $row->getImage ();
				
				if ($image != false) {
					$image = '<a href="' . $row->getUrl () . '"><img src="' . $image->getThumbUrl ( 192, 113 ) . '" alt="' . htmlspecialchars ( $row->Title, null, 'cp1251' ) . '"/></a>';
				} else {
					$image = '';
				}
				
				$price = '';
				if ($row->Price>0){
					$price = '<div>' . $row->Price . ' �</div>';
				}
				elseif (($fields = $row->getUFGroup ()) != false) {
					foreach ( $fields as $field ) {
						$field->Value = trim ( $field->Value );
				
						if (! empty ( $field->Value )) {
							$price = '<div>' . $field->Value . ' �</div><span>' . $field->Title . '</span>';
							break;
						}
					}
				}
				
				$out.= '<li>' . $image . '<p><a href="' . $row->getUrl () . '">' . $row->Title . '</a></p>'.$price.'</li>';
				
			}
			$out .= '
 	</ul>
	</div>';
		}
		$this->over = $out;
	}
	function getItemImage($row, $mass = 0) {
		$sql = $this->query ( "select * from `shop_images` where `iditem`='$row[id]' order by `main` desc" );
		if ($sql!=false && $sql->rowCount()>0) {
			foreach ( $sql->fetchAll ( PDO::FETCH_ASSOC ) as $row2 ) {
				if (file_exists ( "files/shop/{$row2['filename']}" )) {
					if ($mass == 0) {
						return "files/shop/{$row2['filename']}";
					} else {
						return array (
								"id" => $row2 ['id'],
								"min" => "files/shop/{$row2['filename']}",
								"big" => "files/shop/{$row2['filename']}",
								"orig" => "files/shop/{$row2['filename']}" 
						);
					}
				}
			}
		}
		
		return false;
	}
}