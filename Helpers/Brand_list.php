<?php 
class Redactor_Helper_Brand_list extends Redactor_Helper {
	var $over = '';
	function __construct(){
		$words = array (
				'123',
				'A',
				'B',
				'C',
				'D',
				'E',
				'F',
				'G',
				'H',
				'I',
				'J',
				'K',
				'L',
				'M',
				'N',
				'O',
				'P',
				'Q',
				'R',
				'S',
				'T',
				'U',
				'V',
				'W',
				'X',
				'Y',
				'Z'
		);
		$this->over ='<ul>
            	
            	
            ';
		foreach ($words as $word){
			$this->over .='<li><a href="/brands/'.$word.'/">'.$word.'</a></li>';
		}
		$this->over .='</ul>';
	}
}