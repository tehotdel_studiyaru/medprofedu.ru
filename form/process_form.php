<?php
/**
 * Created with Visual Form Builder by 23rd and Walnut
 * www.visualformbuilder.com
 * www.23andwalnut.com
 */

header("Content-Type: text/html; charset=utf-8");

session_start();
$form = new ProcessForm();
$form->field_rules = array(
	'field23'=>'required', //23
	'field26'=>'required',
	'field27'=>'required',
	'field28'=>'required',
	'field30'=>'required',
	'field31'=>'required',
	'field32'=>'required',
	'field33'=>'required',
	'field34'=>'required',
	'field35'=>'required',
	'field36'=>'required',
	'field37'=>'required',
	'field38'=>'required',
	'field39'=>'required',
	'field40'=>'required',
	'field42'=>'required',
	'field43'=>'required',
	'field44'=>'required'
);
$form->validate();

 

class ProcessForm
{
    public $field_rules;
    public $error_messages;
    public $fields;
    private $error_list;
    private $is_xhr;
    public  $field_name;





    function __construct()
    {
        $this->error_messages = array(
            'required' => 'This field is required',
            'email' => 'Please enter a valid email address',
            'number' => 'Please enter a numeric value',
            'url' => 'Please enter a valid URL',
            'pattern' => 'Please correct this value',
            'min' => 'Please enter a value larger than the minimum value',
            'max' => 'Please enter a value smaller than the maximum value'
        );

        $this->field_rules = array();
        $this->error_list = '';
        $this->fields = $_POST;
        $this->is_xhr = $this->xhr();
    }





    function validate()
    {
        if (!empty($this->fields))
        {
            //Validate each of the fields
            foreach ($this->field_rules as $field => $rules)
            {
                $rules = explode('|', $rules);

                foreach ($rules as $rule)
                {
                    $result = null;

                    if (isset($this->fields[$field]))
                    {
                        $param = false;

                        if (preg_match("/(.*?)\[(.*?)\]/", $rule, $match))
                        {
                            $rule = $match[1];
                            $param = $match[2];
                        }

                        $this->fields[$field] = $this->clean($this->fields[$field]);

                        //if the field is a checkbox group create string
                        if (is_array($this->fields[$field]))
                            $this->fields[$field] = implode(', ', $this->fields[$field]);

                        // Call the function that corresponds to the rule
                        if (!empty($rule))
                            $result = $this->$rule($this->fields[$field], $param);

                        // Handle errors
                        if ($result === false)
                            $this->set_error($field, $rule);
                    }
                }
            }
            $word = mb_strtolower(iconv("utf-8", "windows-1251", $_POST['field45']), 'cp1251');
            $_SESSION['secret_code'] = mb_strtolower($_SESSION['secret_code'], 'cp1251');
            if ($word!=$_SESSION['secret_code']){
            	$this->error_list['field45'] = iconv("windows-1251","utf-8", "�� ����� ������� ����� � ��������");
            	//$this->error_list .= iconv("windows-1251","utf-8", "<div class='error'>�� ����� ������� ����� � ��������</div>");
            }
            if (empty($this->error_list))
            {
                if ($this->is_xhr)
                    echo json_encode(array('status' => 'success'));

                $this->process();
            }
            else
            {
                if ($this->is_xhr)
                    echo json_encode(array('status' => 'invalid', 'errors' => $this->error_list));
                else echo $this->error_list;
            }
        }
    }





    function process()
    {
         /**
         * SUCCESS!!
         * There were no errors in the form. Insert your processing logic here (i.e. send an email, save to a
         * database etc.
         *
         * All of the submitted fields are available in the $this->fields variable.
         *
         *
         * IMPORTANT - PLEASE READ:
         * 1. YOU MUST UNCOMMENT THE CODE FOR IT TO WORK.
         *    - This means removing the '//' in front of each line.
         *    - If you do not know what php comments are see here: http://php.net/manual/en/language.basic-syntax.comments.php
         *
         * 2. YOU CAN ENTER ANY EMAIL ADDRESS IN THE $from VARIABLE.
         *    - This is the address that will show in the From column in your mail application.
         *    - If your form contains an email field, and you want to use that value as the $from variable, you can set $from = $this->fields['name of your email field'];
         *
         * 3. FILE ATTACHMENTS
         *    - As stated in the description on codecanyon, this code does not mail attachments. Google 'php html email attachments' for information on how to do this
         *
         *  4. REDIRECTING TO ANOTHER PAGE AFTER SUBMISSION
         *    - This is an ajax enabled form, so you need to perform the redirection in main.js AND this php file.
         *      a. Please see instructions in main.js for redirection. This is for users without JS.
         *      b. Replace the relevant code below with the page you would like to redirect to. REMEMBER TO UNCOMMENT THE LINE FOR IT TO WORK.
         */



         /*********************************************/
         /*             SAMPLE MAIL CODE              */
         /*********************************************/
    	$field_name = array(
    			'field23'=>'������ �� �������� ', 
    			'field26'=>'������������ ���������� (��� ����������)',
    			'field27'=>'������� ��������� �� ���� ��������',
    			'field28'=>'�������� �����',
    			'field30'=>'�� �������',
    			'field31'=>'����� ���������� ����� ��������',
    			'field32'=>'���������� �����',
    			'field33'=>'���������� ������������, ���������� ��������',
    			'field34'=>'���������� ���������',
    			'field35'=>'�. �. �. ����������� (��� ���������� � ������ ���������)',
    			'field36'=>'����� � �������� ����������� (��� ����������) ',
    			'field37'=>'����� � �������� ����������� / �������� (��� ���������� ���)',
    			'field38'=>'���/��� (��� ����������)',
    			'field39'=>'���������� ������ (��� ���������� ���): �����, �����, ��� � ����� ����� ',
    			'field40'=>'�. �. �. ����������� ����',
    			'field42'=>'���������� �������/����',
    			'field43'=>'����������� ����� ',
    			'field44'=>'���� ����������'
    	);

         // $msg = "����� ������ �� ��������:";
         $header="Content-type: text/html; charset=cp1251";
         $txt='';
         $msg = '';
          foreach($this->fields as $key => $field){ 
          	if ($key=='field45'){
          		continue;
          	}
          	$txt=$field_name[$key];
          	$msg .= "<b>$txt:</b> ".iconv('utf-8', 'cp1251',$field)."<br />";
          }
       
          $to = 'fojia@mail.ru';
          $subject = '����� ������ �� ��������';
          $from = 'medprofedu.ru';
          
         

         /* mail($to, $subject, $msg, "From: $from\r\nReply-To: $from\r\nReturn-Path: $from\r\n Content-Type: text/plain; charset=windows-1251");*/

          mail($to, $subject, $msg , $header); //Content-Type: text/plain; charset=utf-8

        /************************************************************************************/
        /*                                REDIRECTION CODE                                  */
        /*         Only uncomment the line below if you want to redirect to another page    */
        /*                          when the form has been submitted                        */
        /************************************************************************************/

        if (!$this->is_xhr)
            header('Location: http://medprofedu.x7s.ru/form/');


    }



    function set_error($field, $rule)
    {
        if ($this->is_xhr)
        {
            $this->error_list[$field] = $this->error_messages[$rule];
        }
        else $this->error_list .= "<div class='error'>$field: " . $this->error_messages[$rule] . "</div>";
    }





    function xhr()
    {
        return (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') ? true : false;
    }





    /** Validation Functions */
    function required($str, $val = false)
    {

        if (!is_array($str))
        {
            $str = trim($str);
            return ($str == '') ? false : true;
        }
        else
        {
            return (!empty($str));
        }
    }





    function email($str)
    {
        return (!preg_match("/^(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){255,})(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){65,}@)(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22))(?:\\.(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-[a-z0-9]+)*\\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-[a-z0-9]+)*)|(?:\\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\\]))$/iD", $str)) ? false : true;
    }





    function number($str)
    {
        return (!is_numeric($str)) ? false : true;
    }





    function min($str, $val)
    {
        return ($str >= $val) ? true : false;
    }





    function max($str, $val)
    {
        return ($str <= $val) ? true : false;
    }





    function pattern($str, $pattern)
    {
        return (!preg_match($pattern, $str)) ? false : true;
    }





    function clean($str)
    {
        $str = is_array($str) ? array_map(array("ProcessForm", 'clean'), $str) : str_replace('\\', '\\\\', strip_tags(trim(htmlspecialchars((get_magic_quotes_gpc() ? stripslashes($str) : $str), ENT_QUOTES))));
        return $str;
    }
}


?>