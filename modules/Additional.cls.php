<?php
class additional_admin extends Redactor_Admin {
	function __construct() {
	}
	function Load_Site_Settings() {
		$sql = $this->query ( "select * from site_setting" );
		$tmp = array ();
		$data = array ();
		foreach ( $sql->fetchAll () as $row ) {
			$tmp [$row->option] = $this->win2utf ( $row ->value);
		
		}
		echo "{success:true, data:{$this->JEncode($tmp)}}";
	}
	function Save_Site_Settings() {
		foreach ( $_POST as $int => $value ) {
			$value = addslashes ( $this->utf2win ( $value ) );
			if (in_array ( strtolower ( $int ), array (
					"module",
					"task",
					"xaction" 
			) )) {
				continue;
			}
			$test = $this->prepare ( "select COUNT(*) from site_setting where `option`=?" );
			$test->execute ( array (
					$int 
			) );
			$test = $test->fetchColumn ();
			if ($test == 0) {
				
				$sth = $this->prepare ( "insert into site_setting values (?, ?)" );
				$sth->execute ( array (
						$int,
						$value 
				) );
			} else {
				$sth = $this->prepare ( "update site_setting set `value`=? where `option`=?" );
				$sth->execute ( array (
						$value,
						$int 
				) );
			}
		}
		echo "{success:true}";
	}
}