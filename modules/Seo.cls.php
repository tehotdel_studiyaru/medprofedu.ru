<?php
class seo_admin extends Redactor_Admin {
	function __construct(){
		
	}
	function loadGeneralData() {
		$stm = $this->Query ( "select `Name`, `Value` from `Seo` where `Name` LIKE 'General_%'" );
		$data = array ();
		if ($stm != false && $stm->rowCount () > 0) {
			foreach ( $stm->fetchAll () as $row ) {
				$data [$row->Name] = $this->win2utf ( $row->Value );
			}
		}
		$robots = getenv ( 'DOCUMENT_ROOT' ) . '/robots.txt';
		if (file_exists ( $robots ) && is_readable ( $robots ) && is_writable ( $robots )) {
			$data ['General_Robots'] = $this->win2utf ( file_get_contents ( $robots ) );
		} else {
			$data ['General_Robots'] = $this->win2utf ( '���� ������ ������������, ���������� ����� ��� ������ � �����' );
		}
		echo json_encode ( array (
				'success' => true,
				"data" => $data 
		) );
	}
	function save() {
		if (count ( $_POST ) > 0) {
			$insert = $this->Stm ( "insert into `Seo` (`Name`, `Value`) values (?, ?)" );
			$update = $this->Stm ( "update `Seo` set `Value`=? where `Name`=?" );
			$count = $this->Stm ( "select count(1) from `Seo` where `Name`=?" );
			
			foreach ( $_POST as $name => $value ) {
				if ($name == 'robots') {
					continue;
				}
				$count->execute ( array (
						$name 
				) );
				if ($count != false && $count->rowCount () > 0) {
					$total = $count->fetchColumn ();
				} else {
					$total = 0;
				}
				if ($total > 0) {
					$update->execute ( array (
							$this->utf2win ( $value ),
							$name 
					) );
				} else {
					$insert->execute ( array (
							$name,
							$this->utf2win ( $value ) 
					) );
				}
			}
			if (isset ( $_POST ['General_Robots'] )) {
				$robots = getenv ( 'DOCUMENT_ROOT' ) . '/robots.txt';
				$_POST ['General_Robots'] = $this->utf2win ( $_POST ['General_Robots'] );
				if (file_exists ( $robots ) && is_writeable ( $robots )) {
					$file = fopen ( $robots, 'a+' );
					if (is_resource ( $file )) {
						ftruncate ( $file, 0 );
						fwrite($file, $_POST['General_Robots']);
						fclose($file);
					}
				}
			}
		}
	}
}