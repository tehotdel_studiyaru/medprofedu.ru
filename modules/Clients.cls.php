<?php
class Redactor_Module_Clients extends Redactor_Action {
	var $over = "";
	var $View = 'list';
	var $limit = 12;
	var $currentId = 0;
	/**
	 * ������������� ����� ������ �������� �� ��������
	 *
	 * @param number $limit        	
	 */
	public function setLimit($limit = 12) {
		$this->limit = $limit;
	}
	public function getLimit() {
		return $this->limit;
	}
	function getView() {
		$this->currentId = isset ( $_GET ['clients'] ) && is_numeric ( $_GET ['clients'] ) ? ( int ) $_GET ['clients'] : 0;
		ob_start ();
		if ($this->currentId != 0) {
			$this->View = 'card';
		}
		Breadcrumbs::add ( '<a href="' . $this->getUrl ( array (
				'module' => 'clients' 
		) ) . '">�������</a>' );
		BreadcrumbsTitle::add ( '�������' );
		include ('Views/clients/' . $this->View . '.phtml');
		$this->over = ob_get_clean ();
	}
	function getClientsItem($Fields = "*") {
		if ($this->currentId == 0) {
			
			return false;
		}
		if (is_string ( $Fields )) {
			$Fields = trim ( $Fields );
			if ($Fields != "*") {
				return false;
			}
		} elseif (is_array ( $Fields ) && count ( $Fields ) > 0) {
			$Fields = implode ( ",", array_map ( 'self::changeFields', $Fields ) );
		} else {
			return false;
		}
		
		$sql = $this->query ( "select  {$Fields} from `clients` where `id`='{$this->currentId}' and `active`='1' limit 1" );
		
		if ($sql != false && $sql->rowCount () > 0) {
			$row = $this->rowCount ( $sql );
			Breadcrumbs::add ( '<a href="?clients=' . $row->id . '">' . $row->name . '</a>' );
			BreadcrumbsTitle::add ( $row->name );
			
			if (! empty ( $row->TitlePage )) {
				BreadcrumbsTitle::set ( $row->TitlePage );
			}
			if (! empty ( $row->DescPage )) {
				Metas::setDescription ( $row->DescPage );
			}
			if (! empty ( $row->KeysPage )) {
				Metas::setKeywords ( $row->KeysPage );
			}
			return $row;
		}
		return false;
	}
	function getItemImage($id) {
		$sql = $this->query ( "select * from `clients_img` where `iditem`=? order by `osn` desc, `pos` asc limit 1" );
		if ($sql != false && $sql->rowCount () > 0) {
			
			$row = $this->rowCount ( $sql );
			$image = ( object ) array (
					"path" => "files/clients",
					"file" => "o_{$row['id']}.{$row['ext']}" 
			);
			
			return $image;
		}
		
		return false;
	}
	function getItemImages($id, $limit = 1) {
		if (is_string ( $limit )) {
			$limit = '';
		} elseif (is_numeric ( $limit ) && $limit > 0) {
			$limit = ' limit ' . $limit;
		} else {
			return false;
		}
		
		$sql = $this->query ( "select * from `clients_img` where `iditem`=? order by `osn` desc, `pos` asc {$limit}" );
		if ($sql != false && $sql->rowCount () > 0) {
			$images = array ();
			foreach ( $sql->fetchAll ( PDO::FETCH_ASSOC ) as $row ) {
				$images [] = ( object ) array (
						"path" => "files/clients",
						"file" => "o_{$row['id']}.{$row['ext']}" 
				);
			}
			return $images;
		}
		
		return false;
	}
	function getClients($Fields = array('id', 'name', 'notice')) {
		if (! is_array ( $Fields ) or is_array ( $Fields ) && count ( $Fields ) == 0) {
			return false;
		}
		
		$limit = '';
		$Fields = array_map ( 'self::changeFields', $Fields );
		if (is_numeric ( $this->limit )) {
			$nowPage = isset ( $_GET ['page'] ) ? ( int ) $_GET ['page'] : 1;
			if ($nowPage < 1) {
				$nowPage = 1;
			}
			$page = $nowPage - 1;
			if ($page < 0) {
				$page = 0;
			}
			$start = abs ( $this->limit * $page );
			$limit = "limit $start, {$this->limit}";
		}
		
		$sql = $this->query ( "select " . implode ( ", ", $Fields ) . " from `clients` where `active`='1' order by  `pos` asc {$limit}" );
		
		if ($sql != false && $sql->rowCount () > 0) {
			$items = array ();
			foreach ( $sql->fetchAll ( PDO::FETCH_ASSOC ) as $row ) {
				$items [] = $row;
			}
			return $items;
		}
		
		return false;
	}
	function countClients() {
		$sql = $this->query ( "select count(1) from `clients` where `active`='1'" );
		if ($stm->rowCount () > 0) {
			return $stm->fetchColumn ();
		}
		return 0;
	}
}
class clients_admin extends Redactor_Admin {
	var $over = "";
	function __construct() {
		ini_set ( "memory_limit", "78M" );
		if (! isset ( $_SESSION ['admin'] )) {
			exit ();
		}
	}
	function Update() {
		if (isset ( $_POST ['pos'] )) {
			$pos = $_POST ['pos'];
		} else {
			$pos = "";
		}
		if (isset ( $_POST ['active'] )) {
			$Active = $_POST ['active'];
		} else {
			$Active = "";
		}
		$this->exec ( "update clients set pos='$pos', `UpdatedDate`=CURRENT_TIMESTAMP, active='$Active' where id='$_POST[id]'" );
		echo "33";
	}
	function UpdateImagePos() {
		$id = isset ( $_POST ['id'] ) ? ( int ) $_POST ['id'] : 0;
		$pos = isset ( $_POST ['pos'] ) ? ( int ) $_POST ['pos'] : 0;
		$this->exec ( "update `clients_img` set `pos`='{$pos}' where `id`='{$id}' limit 1" );
		echo "33";
	}
	function UploadPhoto() {
		if (isset ( $_POST ['id'] ) && ! empty ( $_POST ['id'] )) {
			$this->exec ( "insert into `clients_img` values ('', '$_POST[id]','', '', '0')" );
			$id = $this->getAdapter ()->lastInsertId ();
			$uploaddir = $_SERVER ['DOCUMENT_ROOT'] . "/files/clients/";
			$p = pathinfo ( basename ( $_FILES ['photo-path'] ['name'] ) );
			if (isset ( $p ['extension'] )) {
				$ext = strtolower ( $p ['extension'] );
				
				if (in_array ( $ext, array (
						"jpg",
						"jpeg",
						"png",
						"gif" 
				) )) {
					
					$uploadfile3 = $uploaddir . "o_$id.$ext";
					if (move_uploaded_file ( $_FILES ['photo-path'] ['tmp_name'], $uploadfile3 )) {
						
						$this->exec ( "update `clients_img` set `ext`='$ext' where `id`='$id' limit 1" );
					} else {
						$this->exec ( "delete from `clients_img` where `id`='$id' limit 1" );
						echo "{failure:true}";
						exit ();
					}
				} else {
					$this->exec ( "delete from `clients_img` where `id`='$id' limit 1" );
					echo "{failure:true}";
					exit ();
				}
			} else {
				$this->exec ( "delete from `clients_img` where `id`='$id' limit 1" );
				echo "{failure:true}";
				exit ();
			}
		}
		echo "{success:true}";
	}
	function A2Up($fields, $values) {
		$string = "";
		$i = 0;
		foreach ( $fields as $name => $value ) {
			$i ++;
			if ($i > 1) {
				$string .= ",";
			}
			
			$value = addslashes ( $value );
			$vv = isset ( $values [$name] ) ? $values [$name] : '';
			$string .= "`{$value}`='$vv'";
		}
		return $string;
	}
	function A2S($Array, $Sep = ",", $Closer = "", $Slashes = false) {
		if (is_array ( $Array )) {
			$string = "";
			$i = 0;
			foreach ( $Array as $name => $value ) {
				$i ++;
				if ($i > 1) {
					$string .= "{$Sep}";
				}
				if (is_array ( $value )) {
					$this->A2S ( $value, $Sep, $Closer, $Slashes );
				} else {
					if ($Slashes == true) {
						$value = addslashes ( $value );
					}
					$string .= "{$Closer}{$value}{$Closer}";
				}
			}
			return $string;
		}
		return $Array;
	}
	function getColumns() {
		$sql = $this->query ( "SHOW COLUMNS FROM `clients`" );
		$cols = array ();
		if ($sql != false && $sql->rowCount () > 0) {
			foreach ( $sql->fetchAll ( PDO::FETCH_ASSOC ) as $row ) {
				$cols [] = strtolower($row ['Field']);
			}
		}
		return $cols;
	}
	function save() {
		$id = isset ( $_POST ['id'] ) ? ( int ) $_POST ['id'] : 0;
		if (isset ( $_POST ['id'] )) {
			unset ( $_POST ['id'] );
		}
		$fields = array ();
		$values = array ();
		
		$allow = $this->getColumns ();
		foreach ( $_POST as $field => $value ) {
			$test = strtolower ( $field );
			if (in_array ( $test, $allow )) {
				$fields [$field] = $field;
				$values [$field] = addslashes ( $this->utf2win ( $value ) );
			}
		}
		if (isset ( $_POST ['url'] ) && ! empty ( $_POST ['url'] )) {
			rewriteUrls::saveUrl ( array (
					'module' => 'clients',
					'clients' => $id,
					'name' => $_POST ['url'] 
			) );
		}
		$fields ['active'] = 'active';
		$values ['active'] = 1;
	
		$this->exec ( "update `clients` set {$this->A2Up($fields, $values)} where `id`='$id'" );
		echo "{success:true}";
	}
	function Listing() {
		$orderBy = 'order by ';
		if (isset ( $_POST ['dir'] ) && isset ( $_POST ['sort'] )) {
			$orderBy .= "`{$_POST['sort']}` {$_POST['dir']}";
		} else {
			$orderBy .= '`pos` asc';
		}
		$_POST ['start'] = isset ( $_POST ['start'] ) ? ( int ) $_POST ['start'] : 0;
		$_POST ['limit'] = isset ( $_POST ['limit'] ) ? ( int ) $_POST ['limit'] : 25;
		$sql_count = "SELECT * FROM `clients` {$orderBy}";
		$rs_count = $this->query ( $sql_count );
		if ($rs_count != false && $rs_count->rowCount () > 0) {
			$rows = $rs_count->rowCount ();
		}
		$stm = $this->query ( $sql_count . " LIMIT " . ( int ) $_POST ['start'] . ", " . ( int ) $_POST ['limit'] );
		$arr = array ();
		$arr2 = array ();
		if ($rows > 0) {
			foreach ( $stm->fetchAll ( PDO::FETCH_ASSOC ) as $obj ) {
				$record = $obj;
				$record ['url'] = rewriteUrls::getSingleUrl ( array (
						'module' => 'clients',
						'clients' => $record ['id'] 
				) );
				$record ['UpdatedDate'] =  ( $this->formatDate ( "d micromonth Y", $obj ['UpdatedDate'] ) );
				$record ['CreatedDate'] =  ( $this->formatDate ( "d micromonth Y", $obj ['CreatedDate'] ) );
				
				$record ['link'] = "?clients=$obj[id]";
				$arr [] = $this->winDecode ( $record );
			}
			$jsonresult = $this->JEncode ( $arr );
			echo '({"total":"' . $rows . '","results":' . $jsonresult . '})';
		} else {
			echo '({"total":"0", "results":""})';
		}
	}
	function deleteItem() {
		$sql = $this->exec ( "select * from `clients_img` where `iditem`='$_POST[id]'" );
		if ($sql != false && $sql->rowCount () > 0) {
			$row = $sql->fetch ( PDO::FETCH_ASSOC );
			$dir = $_SERVER ['DOCUMENT_ROOT'] . "/files/clients/";
			$file = "o_{$row['id']}.{$row['ext']}";
			
			if (file_exists ( $dir . $file )) {
				$dd = $dir . $file;
				@unlink ( $dd );
			}
			
			$this->exec ( "delete from `clients_img` where `id`='$row[id]' limit 1" );
		}
		$this->exec ( "delete from `clients` where `id`='$_POST[id]'" );
		echo "33";
	}
	function deleteImage() {
		$id = ( int ) $_POST ['id'];
		$sql = $this->query ( "select * from `clients_img` where `id`='{$id}' limit 1" );
		
		if ($sql != false && $sql->rowCount () > 0) {
			$row = $sql->fetch ( PDO::FETCH_ASSOC );
			
			$dir = $_SERVER ['DOCUMENT_ROOT'] . "/files/clients/";
			$file = "o_{$row['id']}.{$row['ext']}";
			
			if (file_exists ( $dir . $file )) {
				$dd = $dir . $file;
				@unlink ( $dd );
			}
			
			$this->exec ( "delete from `clients_img` where `id`='$row[id]' limit 1" );
		}
		echo "33";
	}
	function setOsnImage() {
		$id = ( int ) $_POST ['id'];
		$sql = $this->query ( "select `id`,`iditem` from `clients_img` where `id`='{$id}' limit 1" );
		if ($sql != false && $sql->rowCount () > 0) {
			$row = $sql->fetch ( PDO::FETCH_ASSOC );
			
			$this->exec ( "update `clients_img` set `osn`='0' where `iditem`='$row[iditem]'" );
			$this->exec ( "update `clients_img` set `osn`='1' where `id`='$row[id]'" );
		}
	}
	function Listing_Images() {
		$id = ( int ) $_POST ['dd'];
		$_POST ['start'] = isset ( $_POST ['start'] ) ? ( int ) $_POST ['start'] : 0;
		$_POST ['limit'] = isset ( $_POST ['limit'] ) ? ( int ) $_POST ['limit'] : 25;
		$rs_count = $this->query ( "SELECT count(1) FROM `clients_img` where `iditem`='$id'" );
		$rows = 0;
		if ($rs_count != false && $rs_count->rowCount () > 0) {
			$rows = $rs_count->fetchColumn ();
		}
		$sql_count = "SELECT * FROM `clients_img` where `iditem`='$id'";
		$stm = $this->query ( "$sql_count  LIMIT " . ( int ) $_POST ['start'] . ", " . ( int ) $_POST ['limit'] );
		$arr = array ();
		$arr2 = array ();
		if ($rows > 0) {
			foreach ( $stm->fetchAll ( PDO::FETCH_ASSOC ) as $obj ) {
				
				$arr2 ['id'] = $this->win2utf ( $obj ['id'] );
				$arr2 ['image'] = "o_{$obj['id']}.{$obj['ext']}";
				$arr2 ['file'] = "o_{$obj['id']}.{$obj['ext']}";
				$arr2 ['osn'] = $obj ['osn'];
				$arr2 ['pos'] = $obj ['pos'];
				$arr [] = $arr2;
			}
			$jsonresult = $this->JEncode ( $arr );
			echo '({"total":"' . $rows . '","results":' . $jsonresult . '})';
		} else {
			echo '({"total":"0", "results":""})';
		}
	}
	function NewItem() {
		$this->exec ( "insert into `clients` (`id`, `active`) value ('', '0')" );
		$id = $this->getAdapter ()->lastInsertId ();
		echo $id;
	}
	function winDecode($string) {
		if (is_array ( $string )) {
			$newArray = array ();
			foreach ( $string as $name => $value ) {
				if (is_array ( $value )) {
					$newArray [$name] = $this->winDecode ( $value );
				} else {
					if (is_string ( $value )) {
						$newArray [$name] = iconv ( "windows-1251", "utf-8", $value );
					} else {
						$newArray [$name] = $value;
					}
				}
			}
			return $newArray;
		} else {
			if (is_string ( $string )) {
				return iconv ( "windows-1251", "utf-8", $string );
			}
		}
		return $string;
	}
	function ResizeImage($image_from, $image_to, $fitwidth = 450, $fitheight = 450, $quality = 100) {
		global $php_inc;
		
		$os = $originalsize = getimagesize ( $image_from );
		
		if ($originalsize [2] != 2 && $originalsize [2] != 3 && $originalsize [2] != 1 && $originalsize [2] != 6 && ($originalsize [2] < 9 or $originalsize [2] > 12)) {
			return false;
		}
		
		if ($originalsize [0] > $fitwidth or $originalsize [1] > $fitheight) {
			$h = getimagesize ( $image_from );
			if (($h [0] / $fitwidth) > ($h [1] / $fitheight)) {
				$fitheight = $h [1] * $fitwidth / $h [0];
			} else {
				$fitwidth = $h [0] * $fitheight / $h [1];
			}
			
			if ($os [2] == 1) {
				$i = @imagecreatefromgif ( $image_from );
				if (! $i) {
					return false;
				}
				$o = ImageCreateTrueColor ( $fitwidth, $fitheight );
				
				$trans_color = imagecolortransparent ( $i );
				$trans_index = imagecolorallocate ( $i, $trans_color ['red'], $trans_color ['green'], $trans_color ['blue'] );
				imagecolortransparent ( $i, $trans_index );
				
				imagesavealpha ( $i, true );
				imagesavealpha ( $o, true );
				imagecopyresampled ( $o, $i, 0, 0, 0, 0, $fitwidth, $fitheight, $h [0], $h [1] );
				imagegif ( $o, $image_to );
				chmod ( $image_to, 0777 );
				imagedestroy ( $o );
				imagedestroy ( $i );
			} 

			elseif ($os [2] == 2 or ($os [2] >= 9 && $os [2] <= 12)) {
				$i = @ImageCreateFromJPEG ( $image_from );
				if (! $i) {
					return false;
				}
				$o = ImageCreateTrueColor ( $fitwidth, $fitheight );
				imagecopyresampled ( $o, $i, 0, 0, 0, 0, $fitwidth, $fitheight, $h [0], $h [1] );
				imagejpeg ( $o, $image_to, $quality );
				chmod ( $image_to, 0777 );
				imagedestroy ( $o );
				imagedestroy ( $i );
			} elseif ($os [2] == 3) {
				$i = @ImageCreateFromPng ( $image_from );
				if (! $i) {
					return false;
				}
				$o = ImageCreateTrueColor ( $fitwidth, $fitheight );
				imagesavealpha ( $i, true );
				
				imagesavealpha ( $i, true );
				imagealphablending ( $o, false );
				
				imagesavealpha ( $o, true );
				imagecopyresampled ( $o, $i, 0, 0, 0, 0, $fitwidth, $fitheight, $h [0], $h [1] );
				
				imagesavealpha ( $o, true );
				imagepng ( $o, $image_to );
				chmod ( $image_to, 0777 );
				imagedestroy ( $o );
				imagedestroy ( $i );
			}
			
			return 2;
		}
		if ($originalsize [0] <= $fitwidth && $originalsize [1] <= $fitheight) {
			if ($os [2] == 1) {
				$i = @imagecreatefromgif ( $image_from );
				if (! $i) {
					return false;
				}
				imagesavealpha ( $i, true );
				imagegif ( $i, $image_to );
			} elseif ($os [2] == 3) {
				$i = @ImageCreateFromPng ( $image_from );
				if (! $i) {
					return false;
				}
				imagesavealpha ( $i, true );
				imagepng ( $i, $image_to );
			} else {
				$i = @ImageCreateFromJPEG ( $image_from );
				if (! $i) {
					return false;
				}
				imagejpeg ( $i, $image_to, $quality );
			}
			imagedestroy ( $i );
			chmod ( $image_to, 0777 );
			return 1;
		}
	}
}

?>