<?php
class Redactor_Module_Brands extends Redactor_Action{
	
}
class Brands_admin extends Redactor_Admin {
	var $ItemsTable = "Brands_Items";
	var $CategoriesTable = "Brands_Categories";
	var $ImagesTable = "Brands_Items_Files";
	var $LimitRecords = 25;
	var $ImagesPath = "files/brands";
	function __construct() {
	}
	function ChangeCategoryIDItems() {
		$items = json_decode ( $_POST ['items'] );
		$new = array ();
		$CategoryID = ( int ) $_POST ['CategoryID'];
		if (is_array ( $items ) && count ( $items ) > 0) {
			$new = array ();
			foreach ( $items as $id ) {
				$new [] = ( int ) $id;
			}
			unset ( $items );
			$this->exec ( "UPDATE `{$this->ItemsTable}` SET `CategoryID`='{$CategoryID}' WHERE `Id` IN (" . implode ( ", ", $new ) . ")" );
		}
	}
	function DeleteImage($Id) {
		if ($Id == null) {
			$Id = isset ( $_POST ['Id'] ) ? ( int ) $_POST ['Id'] : 0;
		}
		$sth = $this->Stm ( "SELECT * FROM `{$this->ImagesTable}` WHERE `Id`=?" );
		if ($sth != false && ($sth->execute ( array (
				$Id 
		) )) != false && $sth->rowCount () > 0) {
			$row = $sth->fetch ();
			$this->exec ( "DELETE FROM `{$this->ImagesTable}` WHERE `Id`='{$row->Id}'" );
			$file = $this->ImagesPath . "/o_{$row->Id}.{$row->Extension}";
			if (file_exists ( $file )) {
				unlink ( $file );
			}
		}
	}
	function DeleteRecord($Id = null) {
		if ($Id == null) {
			$Id = isset ( $_POST ['Id'] ) ? ( int ) $_POST ['Id'] : 0;
		}
		$this->DeleteImages ( $Id );
		$this->exec ( "DELETE FROM `{$this->ItemsTable}` WHERE `Id`='{$Id}'" );
	}
	function DeleteImages($Id) {
		if ($Id != 0) {
			$sth = $this->Stm ( "SELECT `Id` FROM `{$this->ImagesTable}` WHERE `ItemID`=?" );
			if ($sth != false && ($sth->execute ( array (
					$Id 
			) )) != false && $sth->rowCount () > 0) {
				foreach ( $sth->fetchAll () as $row ) {
					$this->DeleteImage ( $row->Id );
				}
			}
		}
	}
	function DeleteRecords() {
		$Id = isset ( $_POST ['Id'] ) ? ( int ) $_POST ['Id'] : 0;
		if ($Id != 0) {
			$sth = $this->Stm ( "SELECT `Id` FROM `{$this->ItemsTable}` WHERE `CategoryID`=?" );
			if ($sth != false && ($sth->execute ( array (
					$Id 
			) )) != false && $sth->rowCount () > 0) {
				foreach ( $sth->fetchAll () as $row ) {
					$this->DeleteRecord ( $row->Id );
				}
			}
		}
	}
	function LoadRecord() {
		$to = isset ( $_POST ['to'] ) ? $_POST ['to'] : '';
		if ($to == 'Category' || $to == 'Record' || $to == "File") {
			$Data = array ();
			$Id = isset ( $_POST ['Id'] ) ? ( int ) $_POST ['Id'] : 0;
			
			if ($to == 'Category') {
				$table = $this->CategoriesTable;
				$fieldCategory = 'parentId';
			} elseif ($to == 'File') {
				$table = $this->ImagesTable;
			} else {
				$table = $this->ItemsTable;
				$fieldCategory = 'CategoryID';
				if ($Id == 0) {
					$sth = $this->query ( "INSERT INTO `{$table}` (`Id`,`UpdatedDate`, `Active`) VALUES (NULL, CURRENT_TIMESTAMP,1)" );
					if ($sth != false) {
						$Id = $this->getAdapter ()->lastInsertId ();
					} else {
						echo json_encode ( array (
								'failure' => true,
								'data' => array () 
						) );
						return false;
					}
				}
			}
			$sth = $this->Stm ( "SELECT * FROM `{$table}` WHERE `Id`=? LIMIT 1" );
			if ($sth != false && ($sth->execute ( array (
					$Id 
			) )) != false && $sth->rowCount () > 0) {
				$row = $sth->fetch ( PDO::FETCH_ASSOC );
				foreach ( $row as $name => $value ) {
					$Data [$name] = $this->win2utf ( $value );
				}
				
				if ($to == 'Category') {
					$Data ['url'] = $this->win2utf ( rewriteUrls::getSingleCustomUrl ( array (
							'module' => 'brands',
							'catid' => $row ['Id'] 
					) ) );
				} elseif ($to == 'Record') {
					$Data ['url'] = $this->win2utf ( rewriteUrls::getSingleCustomUrl ( array (
							'module' => 'brands',
							'brands' => $row ['Id'] 
					) ) );
				}
				$Data ["UpdatedDate2"] = $this->win2utf ( $this->formatDate ( "d micromonth Y H:i:s", $row ['UpdatedDate'] ) );
				$Data ["CreatedDate2"] = $this->win2utf ( $this->formatDate ( "d micromonth Y H:i:s", $row ['CreatedDate'] ) );
			}
			echo json_encode ( array (
					"success" => true,
					"data" => $Data 
			) );
		} else {
			echo json_encode ( array (
					'failure' => true,
					'data' => array () 
			) );
		}
	}
	function getColumns($table) {
		$cols = array ();
		$sth = $this->Query ( "SHOW COLUMNS FROM `{$table}`" );
		if ($sth != false && $sth->rowCount () > 0) {
			foreach ( $sth->fetchAll () as $row ) {
				$cols [] = $row->Field;
			}
		}
		
		return $cols;
	}
	function countFiles($Id) {
		$sth = $this->Stm ( "SELECT COUNT(1) FROM `Catalog_Items_Files` WHERE `ItemID`=?" );
		if ($sth != false && ($sth->execute ( array (
				$Id 
		) )) != false) {
			return $sth->fetchColumn ();
		}
		return 0;
	}
	function LoadRecords() {
		$columns = $this->getColumns ( $this->ItemsTable );
		$orderBy = "ORDER BY ";
		if (isset ( $_POST ['sort'] ) && in_array ( $_POST ['sort'], $columns )) {
			$orderBy .= "`{$_POST['sort']}`";
			if (isset ( $_POST ['dir'] ) && in_array ( $_POST ['dir'], array (
					"ASC",
					"DESC" 
			) )) {
				$orderBy .= ' ' . $_POST ['dir'];
			}
		} else {
			$orderBy .= '`Sort` ASC';
		}
		$results = array ();
		$rows = 0;
		
		$count = $this->Stm ( "SELECT COUNT(1) FROM `{$this->ItemsTable}`" );
		$start = isset ( $_POST ['start'] ) ? ( int ) $_POST ['start'] : 0;
		$limit = isset ( $_POST ['limit'] ) ? ( int ) $_POST ['limit'] : $this->LimitRecords;
		if ($count != false && ($count->execute ()) != false && $count->rowCount () > 0) {
			$rows = $count->fetchColumn ();
		}
		if ($rows > 0) {
			$sth = $this->Stm ( "SELECT `Id`,`Title`, `Sort`, `Active`, `UpdatedDate`, `CreatedDate` FROM `{$this->ItemsTable}`  {$orderBy} LIMIT {$start}, {$limit}" );
			if ($sth != false && ($sth->execute ()) != false && $sth->rowCount () > 0) {
				foreach ( $sth->fetchAll () as $row ) {
					$results [] = array (
							"Id" => $row->Id,
							'Link' => rewriteUrls::getUrl ( array (
									'module' => 'brands',
									'id' => $row->Id 
							) ),
							'CountImages' => $this->countFiles ( $row->Id ),
							"Sort" => $row->Sort,
							'Link' => rewriteUrls::getUrl ( array (
									'module' => 'catalog',
									'catalog' => $row->Id 
							) ),
							"Active" => $row->Active,
							"UpdatedDate" => $this->win2utf ( $this->formatDate ( "d micromonth Y", $row->UpdatedDate ) ),
							"CreatedDate" => $this->win2utf ( $this->formatDate ( "d micromonth Y", $row->CreatedDate ) ),
							"Title" => $this->win2utf ( $row->Title ) 
					);
				}
			}
		}
		
		echo json_encode ( array (
				"total" => $rows,
				"results" => $results 
		) );
	}
	function LoadImages() {
		$columns = $this->getColumns ( $this->ItemsTable );
		$orderBy = "ORDER BY ";
		if (isset ( $_POST ['sort'] ) && in_array ( $_POST ['sort'], $columns )) {
			$orderBy .= "`{$_POST['sort']}`";
			if (isset ( $_POST ['dir'] ) && in_array ( $_POST ['dir'], array (
					"ASC",
					"DESC" 
			) )) {
				$orderBy .= ' ' . $_POST ['dir'];
			}
		} else {
			$orderBy .= '`Sort` ASC';
		}
		$results = array ();
		$rows = 0;
		$CategoryID = isset ( $_POST ['CategoryID'] ) ? ( int ) $_POST ['CategoryID'] : 0;
		$count = $this->Stm ( "SELECT COUNT(1) FROM `{$this->ImagesTable}` WHERE `isImage`=1 AND `ItemID`=?" );
		$start = isset ( $_POST ['start'] ) ? ( int ) $_POST ['start'] : 0;
		$limit = isset ( $_POST ['limit'] ) ? ( int ) $_POST ['limit'] : $this->LimitRecords;
		if ($count != false && ($count->execute ( array (
				( int ) $_POST ['ItemID'] 
		) )) != false && $count->rowCount () > 0) {
			$rows = $count->fetchColumn ();
		}
		if ($rows > 0) {
			$sth = $this->Stm ( "SELECT `Id`,`Extension`, `Title`, `Sort`, `MainImage`, `ItemID` FROM `{$this->ImagesTable}` WHERE `isImage`=1 AND `ItemID`=? {$orderBy} LIMIT {$start}, {$limit}" );
			if ($sth != false && ($sth->execute ( array (
					( int ) $_POST ['ItemID'] 
			) )) != false && $sth->rowCount () > 0) {
				foreach ( $sth->fetchAll () as $row ) {
					$results [] = array (
							"Id" => $row->Id,
							'ItemID' => $row->ItemID,
							"Sort" => $row->Sort,
							'MainImage' => $row->MainImage,
							"Title" => $this->win2utf ( $row->Title ),
							"image" => 'o_' . $row->Id . '.' . $row->Extension 
					);
				}
			}
		}
		
		echo json_encode ( array (
				"total" => $rows,
				"results" => $results 
		) );
	}
	function SaveFile() {
		$Id = 0;
		if (isset ( $_POST ['Id'] )) {
			$Id = ( int ) $_POST ['Id'];
			unset ( $_POST ['Id'] );
		}
		
		$fields = array ();
		$params = array ();
		$table = '';
		$cols = array ();
		
		$table = $this->ImagesTable;
		$cols = $this->getColumns ( $table );
		
		foreach ( $_POST as $name => $value ) {
			if (in_array ( $name, $cols )) {
				$fields [] = '`' . $name . '`=?';
				$params [] = $value;
			}
		}
		
		if (isset ( $_FILES ['photo-path'] ) && isset ( $_FILES ['photo-path'] ['name'] ) && ! empty ( $_FILES ['photo-path'] ['name'] )) {
			$ext = strtolower ( pathinfo ( $_FILES ['photo-path'] ['name'], PATHINFO_EXTENSION ) );
			if (in_array ( $ext, array (
					'jpg',
					'jpeg',
					'png',
					'gif' 
			) )) {
				
				$sth = $this->Stm ( "INSERT INTO `{$table}` (`ItemID`, `Extension`, `isImage`) VALUES (?,?,?)" );
				if ($sth != false && ($sth->execute ( array (
						$_POST ['ItemID'],
						$ext,
						1 
				) )) != false) {
					$Id = $this->getAdapter ()->lastInsertId ();
					if ($Id) {
						if (is_writeable ( $this->ImagesPath )) {
							if (move_uploaded_file ( $_FILES ['photo-path'] ['tmp_name'], $this->ImagesPath . "/o_{$Id}.{$ext}" )) {
								chmod ( $this->ImagesPath . "/o_{$Id}.{$ext}", 0666 );
							} else {
								$this->exec ( "DELETE FROM {$this->ImagesTable} WHERE `Id`='{$Id}' LIMIT 1" );
								echo json_encode ( array (
										"failure" => true,
										'error' => 'error move file' 
								) );
								return false;
							}
						} else {
							$this->exec ( "DELETE FROM {$this->ImagesTable} WHERE `Id`='{$Id}' LIMIT 1" );
							echo json_encode ( array (
									"failure" => true,
									'error' => 'path not writable' 
							) );
							return false;
						}
					} else {
						echo json_encode ( array (
								"failure" => true,
								'error' => 'no ID' 
						) );
						return false;
					}
				} else {
					echo json_encode ( array (
							"failure" => true,
							'error' => 'insert image error',
							'pdo' => $this->getAdapter ()->errorInfo () 
					) );
					return false;
				}
			}
		}
		
		if ($Id == 0) {
			echo json_encode ( array (
					"failure" => true,
					'pdo_error' => true 
			) );
		}
		
		if (isset ( $_POST ['MainImage'] ) && isset ( $_POST ['ItemID'] )) {
			$this->exec ( "UPDATE `{$table}` set `MainImage`='0' where `ItemID`='" . ( int ) $_POST ['ItemID'] . "'" );
		}
		$fields [] = '`UpdatedDate`=CURRENT_TIMESTAMP';
		
		$sth = $this->Stm ( "UPDATE `{$table}` SET " . implode ( ", ", $fields ) . " WHERE `Id`=?" );
		$params [] = $Id;
		
		if ($sth != false && ($sth->execute ( $params )) != false) {
			echo json_encode ( array (
					"success" => true 
			) );
		} else {
			echo json_encode ( array (
					"failure" => true,
					'pdo_error' => true 
			) );
		}
	}
	function LoadBrandsForCombo(){
		$result = array (
				"brands" => array ()
		);
		
		$row ['Title'] = $this->win2utf ( '�� ������' );
		$row ['Id'] = 0;
		$result ['brands'] [] = $row;
		
		$sth = $this->query ( "select `Id`, `Title` from `{$this->ItemsTable}` where `Active`='1' order by `Title` asc" );
		if ($sth!=false && $sth->rowCount()>0) {
			foreach ($sth->fetchAll(PDO::FETCH_ASSOC) as $row){
				$row ['Title'] = $this->win2utf ( $row ['Title'] );
				$result ['brands'] [] = $row;
			}
		}
		echo json_encode ( $result );
	}
	function SaveData() {
		$to = isset ( $_POST ['to'] ) ? $_POST ['to'] : false;
		if ($to == false) {
			echo json_encode ( array (
					"failure" => true 
			) );
			return false;
		}
		
		$Id = 0;
		if (isset ( $_POST ['Id'] )) {
			$Id = ( int ) $_POST ['Id'];
			unset ( $_POST ['Id'] );
		}
		$cols2 = array ();
		$fields = array ();
		$params = array ();
		$table = '';
		$cols = array ();
		if ($to == "Category") {
			$table = $this->CategoriesTable;
			$cols = $this->getColumns ( $table );
		} elseif ($to == "Item") {
			$table = $this->ItemsTable;
			$cols = $this->getColumns ( $table );
		}
		
		foreach ( $_POST as $name => $value ) {
			
			if (in_array ( $name, $cols )) {
				
				if ($Id == 0) {
					$fields [] = '`' . $name . '`';
					$cols2 [] = "?";
				} else {
					$fields [] = '`' . $name . '`=?';
				}
				$params [] = $this->utf2win ( $value );
			}
		}
		
		if ($Id == 0) {
			$cols2 [] = 'CURRENT_TIMESTAMP';
			$fields [] = '`UpdatedDate`';
		} else {
			$fields [] = '`UpdatedDate`=CURRENT_TIMESTAMP';
		}
		if ($Id == 0) {
			$sth = $this->Stm ( "INSERT INTO `{$table}` (" . implode ( ", ", $fields ) . ") VALUES (" . implode ( ", ", $cols2 ) . ")" );
		} else {
			$sth = $this->Stm ( "UPDATE `{$table}` SET " . implode ( ", ", $fields ) . " WHERE `Id`=?" );
			$params [] = $Id;
		}
		
		if ($sth != false && ($sth->execute ( $params )) != false) {
			if ($Id==0){
				$Id = $this->getAdapter()->lastInsertId();
			}
			$sql = $this->query ( "select  `Title` from `{$table}` where `Id`='{$Id}' limit 1" );
			if ($sql != false && $sql->rowCount () > 0) {
				$row = $sql->fetch ( PDO::FETCH_ASSOC );
				if (isset ( $_POST ['url'] ) && ! empty ( $_POST ['url'] )) {
					
					rewriteUrls::saveUrl ( array (
							'module' => 'brands',
							'brands' => $Id,
							'parentId' => $row ['CategoryID'],
							'custom' => $_POST ['url'] 
					) );
					$url = rewriteUrls::getSingleUrl ( array (
							'module' => 'catalog',
							'catalog' => $Id 
					) );
					
					if ($this->get ( 'Brands_TypeURL' ) == 2) {
						rewriteUrls::saveUrl ( array (
								'module' => 'brands',
								'brands' => $Id,
								
								'name' => $this->translit ( $row ['Title'] ) 
						) );
					} else {
						rewriteUrls::saveUrl ( array (
								'module' => 'brands',
								'brands' => $Id,
								
								'name' => '' 
						) );
					}
				} elseif (isset ( $_POST ['url'] ) && empty ( $_POST ['url'] )) {
					
					rewriteUrls::saveUrl ( array (
							'module' => 'brands',
							'brands' => $Id,
							'parentId' => $row ['CategoryID'],
							'custom' => '' 
					) );
					
					$url = rewriteUrls::getSingleUrl ( array (
							'module' => 'catalog',
							'catalog' => $Id 
					) );
					if ($this->get ( 'Brands_TypeURL' ) == 2) {
						rewriteUrls::saveUrl ( array (
								'module' => 'brands',
								'brands' => $Id,
								
								'name' => $this->translit ( $row ['Title'] ) 
						) );
					} else {
						rewriteUrls::saveUrl ( array (
								'module' => 'brands',
								'brands' => $Id,
								
								'name' => '' 
						) );
					}
				} else {
					$url = rewriteUrls::getSingleUrl ( array (
							'module' => 'catalog',
							'catalog' => $Id 
					) );
					
					if ($this->get ( 'Brands_TypeURL' ) == 2) {
						rewriteUrls::saveUrl ( array (
								'module' => 'brands',
								'brands' => $Id,
								
								'name' => $this->translit ( $row ['Title'] ) 
						) );
					} else {
						rewriteUrls::saveUrl ( array (
								'module' => 'brands',
								'brands' => $Id,
								
								'name' => '' 
						) );
					}
				}
			}
			echo json_encode ( array (
					"success" => true 
			) );
		} else {
			
			echo json_encode ( array (
					"failure" => true 
			) );
		}
		return false;
	}
}