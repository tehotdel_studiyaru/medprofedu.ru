<?php
class Redactor_Module_Pages extends Redactor_Action {
	public $over = "";
	var $View = 'index';
	var $Row = null;
	/**
	 * ���������� � ���������� $over ������ ������
	 *
	 * @return void
	 */
	function getView() {
		$id = '';
		if (getenv ( 'REQUEST_URI' ) == '/') {
			$this->setFileTemplate ( 'main' );
		} elseif (isset ( $_GET ['pages'] )) {
			$id = ( int ) $_GET ['pages'];
			if ($id == 1||$id == 316) {
				$this->setFileTemplate ( 'index2' );
			}elseif($id == 339){
				$this->setFileTemplate ( 'index' );
			}

			 else {
				$stm = $this->Stm ( "select `Id`, `parentId`, `Title` from `pages` where `Id`=?" );
				$stm->execute ( array (
						$id 
				) );
				if ($stm != false && $stm->rowCount ( $stm ) > 0) {
					$row = $stm->fetch ( PDO::FETCH_ASSOC );
					if ($row ['parentId'] != 0) {
						$this->setFileTemplate ( 'index2' );
					}
				}
			}
		}
		
		ob_start ();
		
		include ('Views/pages/' . $this->View . '.phtml');
		$this->over = ob_get_clean ();
	}
	function getFullTitle($Id, &$cats=array()){
		$sth = $this->prepare("select `parentId`, `Title` from `pages` where `Id`=?");
		$sth->execute(array($Id));
		if ($sth->rowCount()>0){
			$row  = $sth->fetch();
			
			if ($row->parentId!=0){
				$this->getFullTitle($row->parentId, $cats);
			}
			if ($row->parentId!=0){
				$cats[] = $row->Title;
			}
		}
		return implode(" / ", $cats);
	}
	
	/**
	 * ���������� ������� ������
	 *
	 * @return void
	 */
	function getRoad($id) {
		if ($id == 0) {
			return '';
		}
		$d = '';
		
		$stm = $this->Stm ( "select `Id`, `parentId`, `Title` from `pages` where `Id`=?" );
		$stm->execute ( array (
				$id 
		) );
		if ($stm != false && $stm->rowCount ( $stm ) > 0) {
			$row = $stm->fetch ( PDO::FETCH_ASSOC );
			if ($row ['parentId'] != 0) {
				$this->getRoad ( $row ['parentId'] );
			}
			Breadcrumbs::add ( "<a href='" . $this->getUrl ( array (
					"module" => 'pages',
					'pages' => $row ['Id'] 
			) ) . "'>$row[Title]</a>" );
			BreadcrumbsTitle::add ( "$row[Title]" );
			return $d;
		}
	}
	
	/**
	 * ���������� ������� ��������
	 *
	 * @return Object || false
	 */
	function getPage() {
		if (isset ( $_GET ['isIndex'] )) {
			$stm = $this->getAdapter ()->query ( "SELECT * FROM `pages` where `OnIndex`='1' limit 1" );
		} else {
			$id = isset ( $_GET ['pages'] ) ? ( int ) $_GET ['pages'] : 0;
			if ($id != 0) {
				
				$stm = $this->Stm ( "SELECT * FROM `pages` where `Id`=? limit 1" );
				$stm->execute ( array (
						$id 
				) );
			} else {
				
				return false;
			}
		}
		
		if (isset ( $stm ) && is_object ( $stm ) && $stm->rowCount () == 0) {
			
			return false;
		}
		if ($stm == false) {
			
			return false;
		}
		$row = $stm->fetch ();
		
		if (isset ( $row->Active ) && $row->Active == 2) {
			
			return false;
		} else {
			$this->Row = $row;
			if (! isset ( $_GET ['isIndex'] ) && $row->OnIndex == 1) {
				header ( "Location: /", true, 301 );
				exit ();
			}
			if (empty ( $row->h1 )) {
				$row->h1 = $row->Title;
			}
			$this->getRoad ( $row->Id );
			if (! empty ( $row->Title_Page )) {
				BreadcrumbsTitle::set ( $row->Title_Page );
			}
			if (! empty ( $row->Desc_Page )) {
				Metas::setDescription ( $row->Desc_Page );
			}
			if (! empty ( $row->Keys_Page )) {
				Metas::setKeywords ( $row->Keys_Page );
			}
			return $row;
		}
		return false;
	}
	
	/**
	 * ���������� ������ � ��������� � ����������� ����������� �������� � ���������� $id
	 * �������� $Fields �������� �� �������������� ����, ����������� ���������� ��� ���� Id � Title
	 *
	 * @param int $id        	
	 * @param array $Fields        	
	 * @return multitype:object |boolean
	 */
	function getChildrens($id, $Fields = array('Id', 'Title')) {
		if (is_array ( $Fields )) {
			$Fields = array_map ( 'self::changeFields', $Fields );
			$items = array ();
			$stm = $this->Stm ( "select " . implode ( ", ", $Fields ) . " from `pages` where `Active`='1' and `parentId`=?" );
			$stm->execute ( array (
					$id 
			) );
			
			if ($stm != false && $stm->rowCount () > 0) {
				return $stm->fetchAll ();
			}
		}
		return false;
	}
	function hasChildren($id) {
		$stm = $this->Stm ( "select count(1) from `pages` where `parentId`=? and `Active`='1' " );
		$stm->execute ( array (
				$id 
		) );
		if ($stm != false && $stm->rowCount () > 0 && $stm->fetchColumn () > 0) {
			return true;
		}
		return false;
	}
}
/**
 * ����� ���������������� ����� ������ PAGES
 */
class pages_admin extends Redactor_Admin {
	var $Version = "1.2";
	var $Pos = 0;
	var $ModuleName = "�������� �������";
	var $PathModule = "������� �����";
	var $ModuleID = "pages";
	var $PathPos = "0";
	var $File = "pages.js";
	var $Global = 1;
	var $Icon = "pages";
	function __construct() {
		if (! isset ( $_SESSION ['admin'] )) {
			exit ( 'CheckLogin' );
		}
	}
	function LoadSetting() {
		$stm = $this->getAdapter ()->query ( "select * from `site_setting` where `option` LIKE 'pages_%'" );
		
		$tmp = array ();
		if ($stm != false && $stm->rowCount () > 0) {
			
			foreach ( $stm->fetchAll ( PDO::FETCH_ASSOC ) as $row ) {
				$tmp [$row ['option']] = $this->win2utf ( $row ['value'] );
			}
		}
		$tmp ['Pages_URL'] = $this->win2utf ( rewriteUrls::getModuleName ( 'pages' ) );
		echo "{success:true, data:{$this->JEncode($tmp)}}";
	}
	function Copy() {
		$id = ( int ) trim ( $_POST ['id'] );
		$stm = $this->getAdapter ()->query ( "select * from `pages` where `Id`='{$id}' limit 1" );
		
		if ($stm != false && $stm->rowCount () > 0) {
			$row = $stm->fetch ( PDO::FETCH_ASSOC );
			unset ( $row ['Id'] );
			$fields = array ();
			$values = array ();
			
			foreach ( $row as $name => $value ) {
				if ($name == 'UpdatedDate') {
					$fields [] = "`$name`";
					$values [] = "CURRENT_TIMESTAMP";
				} elseif ($name == 'CreatedDate') {
					$fields [] = "`$name`";
					$values [] = "CURRENT_TIMESTAMP";
				} else {
					$fields [] = "`$name`";
					$values [] = "'" . addslashes ( $value ) . "'";
				}
			}
			
			$this->getAdapter ()->exec ( "insert into `pages` (" . implode ( ",", $fields ) . ") values (" . implode ( ",", $values ) . ")" );
			$new = $this->getAdapter ()->lastInsertId ();
			echo $new;
			if ($this->get ( 'Pages_TypeURL' ) == 2) {
				rewriteUrls::saveUrl ( array (
						'module' => 'pages',
						'pages' => $new,
						'parentId' => $row ['parentId'],
						'name' => $this->translit ( $row ['Title'] ) 
				) );
			} else {
				rewriteUrls::saveUrl ( array (
						'module' => 'pages',
						'pages' => $new,
						'parentId' => $row ['parentId'],
						'name' => '' 
				) );
			}
		}
		
		exit ();
	}
	function loadRecord() {
		$id = ( int ) trim ( $_POST ['id'] );
		$stm = $this->query ( "select * from `pages` where `Id`='{$id}' limit 1" );
		
		$arr2 = array ();
		
		if ($stm != false && $stm->rowCount () > 0) {
			
			foreach ( $stm->fetchAll ( PDO::FETCH_ASSOC ) as $obj ) {
				$arr2 ['id'] = $obj ['Id'];
				$arr2 ['title'] = $this->win2utf ( $obj ['Title'] );
				$arr2 ['title2'] = "<img src='/core/icons/folder_page.png' style='margin-top:-4px'  align='left'/> <b>" . $this->win2utf ( $obj ['Title'] ) . "</b>";
				$arr2 ['parentId'] = $obj ['parentId'];
				
				if ($obj ['parentId'] == 0) {
					$arr2 ['parentName'] = $this->win2utf ( "�������� ������" );
				} else {
					$parentName = $this->getParentName ( $obj ['parentId'] );
					$arr2 ['parentName'] = $this->win2utf ( $parentName );
				}
				$arr2 ['UpdatedDate'] = $this->win2utf ( $this->formatDate ( "d micromonth Y", $obj ['UpdatedDate'] ) );
				$arr2 ['CreatedDate'] = $this->win2utf ( $this->formatDate ( "d micromonth Y", $obj ['CreatedDate'] ) );
				
				$arr2 ['text'] = $this->win2utf ( $obj ['Text'] );
				$arr2 ['secondTitle'] = $this->win2utf ( $obj ['secondTitle'] );
				$arr2 ['textblock'] = $this->win2utf ( $obj ['textblock'] );
				$arr2 ['inMenu'] = $obj ['inMenu'];
				$arr2 ['h1'] = $this->win2utf ( $obj ['h1'] );
				$arr2 ['copy'] = $this->win2utf ( $obj ['copy'] );
				$arr2 ['isService'] = $obj ['isService'];
				$arr2 ['desc'] = $this->win2utf ( $obj ['Desc_Page'] );
				$arr2 ['tags'] = $this->win2utf ( $obj ['tags'] );
				$arr2 ['link'] = rewriteUrls::getUrl ( array (
						'module' => 'pages',
						'pages' => $obj ['Id'] 
				) );
				
				$arr2 ['url'] = $this->win2utf ( rewriteUrls::getSingleCustomUrl ( array (
						'module' => 'pages',
						'pages' => $obj ['Id'] 
				) ) );
				$arr2['gallery_name'] = $this->win2utf('�� �������');
				$sth = $this->prepare("select `Title` from `Catalog_Categories` WHERE `Id`=? LIMIT 1");
				$sth->execute(array($obj['gallery']));
				if ($sth->rowCount()>0){
					$arr2['gallery_name'] = $this->win2utf($sth->fetchColumn());
				}
				$arr2 ['keys'] = $this->win2utf ( $obj ['Keys_Page'] );
				$arr2 ['pos'] = $this->win2utf ( $obj ['module'] );
				$arr2 ['title_page'] = $this->win2utf ( $obj ['Title_Page'] );
				if ($obj ['OnIndex'] == 1) {
					$arr2 ['index'] = $this->win2utf ( '�������' );
				} else {
					$arr2 ['index'] = "";
				}
				$arr2 ['active'] = $this->win2utf ( $obj ['Active'] );
			}
		}
		echo "{success:true, data:{$this->JEncode($arr2)}}";
	}
	function SaveSetting() {
		$url = isset ( $_POST ['Pages_URL'] ) ? trim ( $this->utf2win ( $_POST ['Pages_URL'] ) ) : null;
		if ($url != null) {
			unset ( $_POST ['Pages_URL'] );
		}
		if (empty ( $url )) {
			$this->getAdapter ()->exec ( "delete from `rewriteUrls` where `module`='pages' and `parentId`=0 and `objId`='0' and `module`='pages' and `isCat`='0' limit 1" );
		} else {
			$count = 0;
			$stm = $this->getAdapter ()->query ( "select count(1) from `rewriteUrls` where `module`='pages' and `parentId`='0' and `objId`='0' and `isCat`='0'" );
			if ($stm != false && $stm->rowCount () > 0) {
				$count = $stm->fetchColumn ();
			}
			if ($count > 0) {
				$stm = $this->Stm ( "update `rewriteUrls` set `name`='?' where `module`='pages' and `parentId`='0' and `objId`='0'  and `isCat`='0' limit 1" );
				$stm->execute ( array (
						$url 
				) );
			} else {
				$stm = $this->Stm ( "insert into `rewriteUrls` (`module`, `name`, `parentId`, `objId`, `isCat`) values ('pages', '?', 0,0,0)" );
				$stm->execute ( array (
						$url 
				) );
			}
		}
		
		$old = $this->get ( 'Pages_TypeURL' );
		
		$insert = $this->Stm ( "insert into `site_setting` values (?, ?)" );
		$update = $this->Stm ( "update `site_setting` set `value`=? where `option`=?" );
		foreach ( $_POST as $int => $value ) {
			$value = addslashes ( $this->utf2win ( $value ) );
			if (in_array ( strtolower ( $int ), array (
					"module",
					"task",
					"xaction" 
			) )) {
				continue;
			}
			$test = 0;
			$stm = $this->getAdapter ()->query ( "select COUNT(*) from `site_setting` where `option`='$int'" );
			if ($stm != false && $stm->rowCount () > 0) {
				$test = $stm->fetchColumn ();
			}
			
			if ($test == 0) {
				
				$insert->execute ( array (
						$int,
						$value 
				) );
			} else {
				$update->execute ( array (
						$value,
						$int 
				) );
			}
		}
		if (($new = $this->get ( 'Pages_TypeURL' )) == 2 && $new != $old) {
			$stm = $this->getAdapter ()->query ( "select `Id`,`Title`, `parentId` from `pages`" );
			if ($stm != false && $stm->rowCount () > 0) {
				foreach ( $stm->fetchAll ( PDO::FETCH_ASSOC ) as $row ) {
					rewriteUrls::saveUrl ( array (
							'module' => 'pages',
							'pages' => $row ['Id'],
							'parentId' => $row ['parentId'],
							'name' => $this->translit ( $row ['Title'] ) 
					) );
				}
			}
		} elseif (($new = $this->get ( 'Pages_TypeURL' )) == 1 && $new != $old) {
			$stm = $this->getAdapter ()->query ( "select `Id`,`Title`, `parentId` from `pages`" );
			if ($stm != false && $stm->rowCount () > 0) {
				foreach ( $stm->fetchAll ( PDO::FETCH_ASSOC ) as $row ) {
					rewriteUrls::saveUrl ( array (
							'module' => 'pages',
							'pages' => $row ['Id'],
							'parentId' => $row ['parentId'],
							'name' => $row ['Id'] 
					) );
				}
			}
		}
		echo "{success:true}";
	}
	function getParentName($Id) {
		$stm = $this->Stm ( "select `Title` from `pages` where `Id`=?  limit 1" );
		$stm->execute ( array (
				$Id 
		) );
		if ($stm != false && $stm->rowCount () > 0) {
			return $stm->fetchColumn ();
		}
		return '';
	}
	function listing_parents_auxpages($parentId, $br = "--") {
		global $arr;
		$orderBy = 'order by ';
		if (isset ( $_POST ['sort'] )) {
			switch ($_POST ['sort']) {
				case "Id" :
				case "id" :
					$orderBy .= "`Id`";
					break;
				case "title" :
				case "title2" :
					$orderBy .= "`Title`";
					break;
				case "inMenu" :
					$orderBy .= " `inMenu`";
					break;
				case "module" :
				case "pos" :
					$orderBy .= "`module`";
					break;
				case "active" :
					$orderBy .= "`Active`";
					break;
				case "isService" :
					$orderBy .= "`isService`";
					break;
				default :
					$orderBy .= "`module`";
			}
		} else {
			$orderBy .= "`module`";
		}
		if (isset ( $_POST ['dir'] )) {
			$orderBy .= " {$_POST['dir']}";
		} else {
			$orderBy .= " asc";
		}
		
		$count = "SELECT count(1) FROM `pages` where `parentId`=$parentId {$orderBy}";
		$sql_count = "SELECT * FROM `pages` where `parentId`=$parentId {$orderBy}";
		$sql = $sql_count ;
		$rs_count = $this->getAdapter ()->query ( $count );
		if ($rs_count != false && $rs_count->rowCount () > 0) {
			$rows = $rs_count->fetchColumn ();
			$rs = $this->getAdapter ()->query ( $sql );
			if ($rs != false && $rs->rowCount () > 0) {
				$dd = $br;
				foreach ( $rs->fetchAll ( PDO::FETCH_ASSOC ) as $obj ) {
					$br = $dd;
					
					$arr2 ['id'] = $obj ['Id'];
					$arr2 ['title'] = $this->win2utf ( $obj ['Title'] );
					$arr2 ['title2'] = "<div style='padding-left:{$br}px'>" . $this->win2utf ( $obj ['Title'] ) . "</div>";
					$arr2 ['parentId'] = $obj ['parentId'];
					if ($obj ['parentId'] == 0) {
						$arr2 ['parentName'] = $this->win2utf ( "�������� ������" );
					} else {
						$parentName = $this->getParentName ( $obj ['parentId'] );
						$arr2 ['parentName'] = $this->win2utf ( $parentName );
					}
					$arr2 ['text'] = $this->win2utf ( $obj ['Text'] );
					
					$arr2 ['textblock'] = $this->win2utf ( $obj ['textblock'] );
					$arr2 ['desc'] = $this->win2utf ( $obj ['Desc_Page'] );
					$arr2 ['url'] = $this->win2utf ( rewriteUrls::getSingleCustomUrl ( array (
							'module' => 'pages',
							'pages' => $obj ['Id'] 
					) ) );
					$arr2 ['tags'] = $this->win2utf ( $obj ['tags'] );
					$arr2 ['copy'] = $this->win2utf ( $obj ['copy'] );
					$arr2 ['h1'] = $this->win2utf ( $obj ['h1'] );
					$arr2 ['inMenu'] = $obj ['inMenu'];
					$arr2 ['isService'] = $obj ['isService'];
					$arr2 ['UpdatedDate'] = $this->win2utf ( $this->formatDate ( "d micromonth Y", $obj ['UpdatedDate'] ) );
					$arr2 ['CreatedDate'] = $this->win2utf ( $this->formatDate ( "d micromonth Y", $obj ['CreatedDate'] ) );
					$arr2 ['UpdatedDate2'] = $this->win2utf ( $this->formatDate ( "d micromonth Y H:i:s", $obj ['UpdatedDate'] ) );
					$arr2 ['CreatedDate2'] = $this->win2utf ( $this->formatDate ( "d micromonth Y H:i:s", $obj ['CreatedDate'] ) );
					
					$arr2 ['keys'] = $this->win2utf ( $obj ['Keys_Page'] );
					$arr2 ['pos'] = $this->win2utf ( $obj ['module'] );
					$arr2 ['link'] = rewriteUrls::getUrl ( array (
							'module' => 'pages',
							'pages' => $obj ['Id'] 
					) );
					$arr2['gallery_name'] = $this->win2utf('�� �������');
					$sth = $this->prepare("select `Title` from `Catalog_Categories` WHERE `Id`=? LIMIT 1");
					$sth->execute(array($obj['gallery']));
					if ($sth->rowCount()>0){
						$arr2['gallery_name'] = $this->win2utf($sth->fetchColumn());
					}
					$arr2 ['title_page'] = $this->win2utf ( $obj ['Title_Page'] );
					if ($obj ['OnIndex'] == 1) {
						$arr2 ['index'] = $this->win2utf ( '�������� ��������' );
					} else {
						$arr2 ['index'] = "";
					}
					$arr2 ['active'] = $this->win2utf ( $obj ['Active'] );
					$arr [] = $arr2;
					unset ( $arr2 );
					$ss3 = $this->query ( "select count(1) from `pages` where `parentId`='$obj[Id]'" );
					
					if ($ss3 != false && $ss3->rowCount () > 0 && $ss3->fetchColumn () > 0) {
						$br += 8;
						
						$this->listing_parents_auxpages ( $obj ['Id'], $br );
					}
				}
			}
		}
	}
	protected function read() {
		$br = "";
		if (isset ( $_POST ['start'] ) && $_POST ['start'] == "") {
			$_POST ['start'] == "0";
		}
		global $arr;
		
		$orderBy = 'order by ';
		if (isset ( $_POST ['sort'] )) {
			switch ($_POST ['sort']) {
				case "Id" :
				case "id" :
					$orderBy .= "`Id`";
					break;
				case "active" :
					$orderBy .= "`Active`";
					break;
				case "title" :
				case "title2" :
					$orderBy .= "`Title`";
					break;
				case "inMenu" :
					$orderBy .= " `inMenu`";
					break;
				case "module" :
				case "pos" :
					$orderBy .= "`module`";
					break;
				case "isService" :
					$orderBy .= "`isService`";
					break;
				default :
					$orderBy .= "`module`";
			}
		} else {
			$orderBy .= "`module`";
		}
		if (isset ( $_POST ['dir'] )) {
			$orderBy .= " {$_POST['dir']}";
		} else {
			$orderBy .= " asc";
		}
		
		$count = "SELECT count(1) FROM `pages` where `parentId`='0' ";
		$sql_count = "SELECT * FROM `pages` where `parentId`='0'";
		$sql = $sql_count . "  LIMIT " . ( int ) $_POST ['start'] . ", " . ( int ) $_POST ['limit'];
		$rs_count = $this->getAdapter ()->query ( $count );
		if ($rs_count != false && $rs_count->rowCount () > 0) {
			$rows = $rs_count->fetchColumn ();
			
			$rs = $this->query ( $sql );
			
			if ($rs != false && $rs->rowCount () > 0) {
				
				foreach ( $rs->fetchAll ( PDO::FETCH_ASSOC ) as $obj ) {
					$arr2 ['id'] = $obj ['Id'];
					$arr2 ['title'] = $this->win2utf ( $obj ['Title'] );
					$arr2 ['title2'] = "<img src='/core/icons/folder_page.png' style='margin-top:-4px'  align='left'/> <b>" . $this->win2utf ( $obj ['Title'] ) . "</b>";
					$arr2 ['parentId'] = $obj ['parentId'];
					
					if ($obj ['parentId'] == 0) {
						$arr2 ['parentName'] = $this->win2utf ( "�������� ������" );
					} else {
						$parentName = $this->getParentName ( $obj ['parentId'] );
						$arr2 ['parentName'] = $this->win2utf ( $parentName );
					}
					$arr2 ['UpdatedDate'] = $this->win2utf ( $this->formatDate ( "d micromonth Y", $obj ['UpdatedDate'] ) );
					$arr2 ['CreatedDate'] = $this->win2utf ( $this->formatDate ( "d micromonth Y", $obj ['CreatedDate'] ) );
					
					$arr2 ['UpdatedDate2'] = $this->win2utf ( $this->formatDate ( "d micromonth Y H:i:s", $obj ['UpdatedDate'] ) );
					$arr2 ['CreatedDate2'] = $this->win2utf ( $this->formatDate ( "d micromonth Y H:i:s", $obj ['CreatedDate'] ) );
					
					$arr2 ['text'] = $this->win2utf ( $obj ['Text'] );
					$arr2 ['secondTitle'] = $this->win2utf ( $obj ['secondTitle'] );
					$arr2 ['textblock'] = $this->win2utf ( $obj ['textblock'] );
					$arr2 ['inMenu'] = $obj ['inMenu'];
					$arr2 ['h1'] = $this->win2utf ( $obj ['h1'] );
					$arr2 ['copy'] = $this->win2utf ( $obj ['copy'] );
					$arr2 ['isService'] = $obj ['isService'];
					$arr2 ['desc'] = $this->win2utf ( $obj ['Desc_Page'] );
					$arr2 ['tags'] = $this->win2utf ( $obj ['tags'] );
					$arr2 ['link'] = rewriteUrls::getUrl ( array (
							'module' => 'pages',
							'pages' => $obj ['Id'] 
					) );
					
					$arr2 ['url'] = $this->win2utf ( rewriteUrls::getSingleCustomUrl ( array (
							'module' => 'pages',
							'pages' => $obj ['Id'] 
					) ) );
					$arr2 ['keys'] = $this->win2utf ( $obj ['Keys_Page'] );
					$arr2 ['pos'] = $this->win2utf ( $obj ['module'] );
					$arr2 ['title_page'] = $this->win2utf ( $obj ['Title_Page'] );
					if ($obj ['OnIndex'] == 1) {
						$arr2 ['index'] = $this->win2utf ( '�������' );
					} else {
						$arr2 ['index'] = "";
					}
					$arr2['gallery_name'] = $this->win2utf('�� �������');
					$sth = $this->prepare("select `Title` from `Catalog_Categories` WHERE `Id`=? LIMIT 1");
					$sth->execute(array($obj['gallery']));
					if ($sth->rowCount()>0){
						$arr2['gallery_name'] = $this->win2utf($sth->fetchColumn());
					}
					$arr2 ['active'] = $this->win2utf ( $obj ['Active'] );
					$arr [] = $arr2;
					unset ( $arr2 );
					$ss3 = $this->query ( "select count(1) from `pages` where `parentId`='$obj[Id]'" );
					
					if ($ss3 != false && $ss3->rowCount () > 0 && $ss3->fetchColumn () > 0) {
						$br = 20;
						
						$this->listing_parents_auxpages ( $obj ['Id'], $br );
					}
				}
				$jsonresult = $this->JEncode ( $arr );
				echo '({"total":"' . $rows . '","results":' . $jsonresult . '})';
			} else {
				echo $this->JEncode ( array (
						"total" => 0,
						"results" => "",
						"sql" => $sql_count 
				) );
			}
		} else {
			echo $this->JEncode ( array (
					"total" => 0,
					"results" => "",
					"sql" => $sql_count 
			) );
		}
	}
	function replaceNames() {
		unset ( $_POST ['module'] );
		foreach ( $_POST as $name => $val ) {
			switch ($name) {
				case "text" :
					unset ( $_POST [$name] );
					$name = "Text";
					break;
				case "keys" :
					unset ( $_POST [$name] );
					$name = "Keys_Page";
					break;
				case "title" :
					unset ( $_POST [$name] );
					$name = "Title";
					break;
				case "desc" :
					unset ( $_POST [$name] );
					$name = "Desc_Page";
					break;
				case "title_page" :
					unset ( $_POST [$name] );
					$name = "Title_Page";
					break;
				case "active" :
					unset ( $_POST [$name] );
					$name = "Active";
					break;
				case "pos" :
					unset ( $_POST [$name] );
					
					$name = "module";
					break;
			}
			$_POST [$name] = $val;
		}
		return $_POST;
	}
	protected function homepage() {
		$_POST ['id'] = ( int ) $_POST ['id'];
		$this->exec ( "update pages set `OnIndex`=0, `UpdatedDate`=CURRENT_TIMESTAMP where `OnIndex`=1" );
		$this->exec ( "update pages set `OnIndex`='1', `UpdatedDate`=CURRENT_TIMESTAMP where id='$_POST[id]'" );
		echo "{success:true}";
	}
	protected function destroy() {
		$id = isset ( $_POST ['id'] ) ? ( int ) $_POST ['id'] : 0;
		
		$action = $this->win2utf ( "�������� ��������" );
		$err = $this->win2utf ( "���������� ��������� ������, ���������� ���� �������" );
		$succ = $this->win2utf ( "�������� �������" );
		$this->exec("delete from `rewriteUrls` WHERE `Module`='pages' and `objId`='{$id}'");
		$this->exec ( "DELETE FROM `pages` WHERE `Id`='$id'" );
		$this->exec("update `pages` set `parentId`='0' where `parentId`='{$id}'");
		echo "{success: true, msg:'$succ', action:'$action'}";
	}
	function update() {
		$update = array ();
		$id = isset ( $_POST ['id'] ) ? ( int ) $_POST ['id'] : 0;
		unset ( $_POST ['id'] );
		$_POST = $this->replaceNames ();
		
		if ($id == 0 or empty ( $id )) {
			$fields = array ();
			$values = array ();
			
			foreach ( $_POST as $name => $value ) {
				// ! preg_match ( "/(parentName|task|xaction)/is", $name )
				if (! in_array ( strtolower ( $name ), array (
						"parentname",
						"gallery_name",
						"task",
						"xaction" 
				) )) {
					$fields [] = "`$name`";
					$values [] = "'" .  $value  . "'";
				}
			}
			$fields [] = "`UpdatedDate`";
			$values [] = "CURRENT_TIMESTAMP";
			$this->exec ( "insert into `pages` (" . implode ( ",", $fields ) . ") values (" . implode ( ",", $values ) . ")" ) or die ( "{failure:true}" );
			$newId = $this->getAdapter ()->lastInsertId ();
			$id = $newId;
		} else {
			$values = array ();
			foreach ( $_POST as $name => $value ) {
				if (! in_array ( strtolower ( $name ), array (
						"parentname",
						"gallery_name",
						"task",
						"xaction" 
				) )) {
					
					$values [] = "`$name`='" .  $value  . "'";
				}
			}
			$values [] = "`UpdatedDate`=CURRENT_TIMESTAMP";
			if (count ( $values ) > 0) {
				
				$this->exec ( "update `pages` set " . implode ( ",", $values ) . " where `Id`='$id'" ) or die ( "{failure:true}\n" );
			}
		}
		$sql = $this->query ( "select  `Title` from `pages` where `Id`='{$id}' limit 1" );
		if ($sql != false && $sql->rowCount () > 0) {
			$row = $sql->fetch ( PDO::FETCH_ASSOC );
			if (isset ( $_POST ['url'] ) && ! empty ( $_POST ['url'] )) {
				rewriteUrls::saveUrl ( array (
						'module' => 'pages',
						'pages' => $id,
						'parentId' => $_POST ['parentId'],
						'custom' => $_POST ['url'] 
				) );
				$url = rewriteUrls::getSingleUrl ( array (
						'module' => 'pages',
						'pages' => $id 
				) );
				
				if (empty ( $url )) {
					if ($this->get ( 'Pages_TypeURL' ) == 2) {
						rewriteUrls::saveUrl ( array (
								'module' => 'pages',
								'pages' => $id,
								'parentId' => $_POST ['parentId'],
								'name' => $this->translit ( $row ['Title'] ) 
						) );
					} else {
						rewriteUrls::saveUrl ( array (
								'module' => 'pages',
								'pages' => $id,
								'parentId' => $_POST ['parentId'],
								'name' => '' 
						) );
					}
				}
			} elseif (isset ( $_POST ['url'] ) && empty ( $_POST ['url'] )) {
				
				rewriteUrls::saveUrl ( array (
						'module' => 'pages',
						'pages' => $id,
						'parentId' => $_POST ['parentId'],
						'custom' => '' 
				) );
				$url = rewriteUrls::getSingleUrl ( array (
						'module' => 'pages',
						'pages' => $id 
				) );
				
				if (empty ( $url )) {
					if ($this->get ( 'Pages_TypeURL' ) == 2) {
						rewriteUrls::saveUrl ( array (
								'module' => 'pages',
								'pages' => $id,
								'parentId' => $_POST ['parentId'],
								'name' => $this->translit ( $row ['Title'] ) 
						) );
					} else {
						rewriteUrls::saveUrl ( array (
								'module' => 'pages',
								'pages' => $id,
								'parentId' => $_POST ['parentId'],
								'name' => '' 
						) );
					}
				}
			} else {
				$url = rewriteUrls::getSingleUrl ( array (
						'module' => 'pages',
						'pages' => $id 
				) );
				
				if (empty ( $url )) {
					if ($this->get ( 'Pages_TypeURL' ) == 2) {
						rewriteUrls::saveUrl ( array (
								'module' => 'pages',
								'pages' => $id,
								'parentId' => $_POST ['parentId'],
								'name' => $this->translit ( $row ['Title'] ) 
						) );
					} else {
						rewriteUrls::saveUrl ( array (
								'module' => 'pages',
								'pages' => $id,
								'parentId' => $_POST ['parentId'],
								'name' => '' 
						) );
					}
				}
			}
			$url = rewriteUrls::getSingleUrl ( array (
					'module' => 'pages',
					'pages' => $id 
			) );
			$url2 = rewriteUrls::getSingleCustomUrl ( array (
					'module' => 'pages',
					'pages' => $id 
			) );
			if (empty ( $url ) && empty ( $url2 )) {
				$this->exec ( "DELETE FROM `rewriteUrls` where `module`='pages' and `objId`='{$id}' and `parentId`='" . ( int ) $_POST ['parentId'] . "'" );
			}
		}
		echo "{success:true}";
	}
	function A2S($Array, $Sep = ",", $Closer = "", $Slashes = false) {
		if (is_array ( $Array )) {
			$string = "";
			$i = 0;
			foreach ( $Array as $name => $value ) {
				$i ++;
				if ($i > 1) {
					$string .= "{$Sep}";
				}
				if (is_array ( $value )) {
					$this->A2S ( $value, $Sep, $Closer, $Slashes );
				} else {
					if ($Slashes == true) {
						$value = addslashes ( $value );
					}
					$string .= "{$Closer}{$value}{$Closer}";
				}
			}
			return $string;
		}
		return $Array;
	}
	protected function Load_Tree_Pages() {
		echo $this->JEncode ( $this->getRoot_Aux () );
	}
	protected function getChild_Aux($id) {
		$sql = $this->query ( "select * from `pages` where parentId='$id'" );
		
		if ($sql != false && $sql->rowCount () > 0) {
			$children = array ();
			foreach ( $sql->fetchAll ( PDO::FETCH_ASSOC ) as $row ) {
				$child = $this->getChild_Aux ( $row ['Id'] );
				$tmp ['text'] = $this->win2utf ( $row ['Title'] );
				$tmp ['id'] = $row ['Id'];
				if (is_array ( $child )) {
					$tmp ['leaf'] = false;
				} else {
					$tmp ['leaf'] = true;
				}
				$tmp ['children'] = $child;
				$children [] = $tmp;
				unset ( $tmp );
			}
			
			return $children;
		}
		return '';
	}
	protected function getRoot_Aux() {
		$sql = $this->query ( "select * from `pages` where parentId=0" );
		if ($sql != false && ($num = $sql->rowCount ()) > 0) {
			
			$nodes = array ();
			foreach ( $sql->fetchAll ( PDO::FETCH_ASSOC ) as $row ) {
				$child = $this->getChild_Aux ( $row ['Id'] );
				
				$tmp ['text'] = $this->win2utf ( $row ['Title'] );
				$tmp ['id'] = $row ['Id'];
				if (is_array ( $child )) {
					$tmp ['leaf'] = false;
				} else {
					$tmp ['leaf'] = true;
				}
				$tmp ['children'] = $child;
				$nodes [] = $tmp;
				unset ( $tmp );
			}
			
			return $nodes;
		}
		return '';
	}
	function winDecode($string) {
		if (is_array ( $string )) {
			$newArray = array ();
			foreach ( $string as $name => $value ) {
				if (is_array ( $value )) {
					$newArray [$name] = $this->winDecode ( $value );
				} else {
					if (is_string ( $value )) {
						$newArray [$name] = iconv ( "windows-1251", "utf-8", $value );
					} else {
						$newArray [$name] = $value;
					}
				}
			}
			return $newArray;
		} else {
			if (is_string ( $string )) {
				return iconv ( "windows-1251", "utf-8", $string );
			}
		}
		return $string;
	}
}

