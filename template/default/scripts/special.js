    function set_font_size(fs) {
        save_user_settings('fs', fs);
        $('body').removeClass('fontsize-small fontsize-normal fontsize-big').addClass(fs);
    }
    
    function set_colors(color) {
        save_user_settings('color', color);
        $('body').removeClass('color1 color2 color3 color4 color5').addClass(color);
    }
    
    function set_font_family(ff) {
        save_user_settings('ff', ff);
        $('body').removeClass('serif sans-serif').addClass(ff);
    }
    
    function set_letter_spacing(ls) {
        save_user_settings('ls', ls);
        $('body').removeClass('spacing-normal spacing-big spacing-small').addClass(ls);
    }
    
    function set_images(c) {
        save_user_settings('imgs', c);
        $('body').removeClass('imagesoff imageson').addClass(c);
        $('.a-images a').attr('rel', (c=='imagesoff')?'imageson':'imagesoff');
    }
    
    function set_default_settings() {
        set_colors(def_settings.color);
        set_font_family(def_settings.ff);
        set_font_size(def_settings.fs);
        set_letter_spacing(def_settings.ls);
        set_images(def_settings.imgs);
    }
    
    function set_user_settings() {
        var sets = { // iieo?aai ec eoe iieuciaaoaeuneea iano?ieee
            color:  $.cookie('color') || window.def_settings.color,
            ff:     $.cookie('ff') || window.def_settings.ff,
            fs:     $.cookie('fs') || window.def_settings.fs,
            ls:     $.cookie('ls') || window.def_settings.ls,
            imgs:   $.cookie('imgs') || window.def_settings.imgs
        };
        set_colors(sets.color);
        set_font_family(sets.ff);
        set_font_size(sets.fs);
        set_letter_spacing(sets.ls);
        set_images(sets.imgs);
    }
    
    function save_user_settings(name, value) {
        $.cookie(name, value, { expires: 2*365, path: '/', domain: '.' + document.domain, secure: false });
    }
    
    
$(document).ready(function(){
    window.def_settings = {
            color:  'color1',
            ff:     'sans-serif',
            fs:     'fontsize-small',
            ls:     'spacing-small',
            imgs:   'imageson'
        };
    set_user_settings();
    
    $('.choose-font-family a').click(function(){
        set_font_family($(this).attr('rel'));
        return false;
    });
    $('.choose-letter-spacing a').click(function(){
        set_letter_spacing($(this).attr('rel'));
        return false;
    });
    $('.a-images a').click(function(){
        set_images($(this).attr('rel'));
        return false;
    });

  $('.a-fontsize a').click(function(){
    set_font_size($(this).attr('rel'));
    return false;
  });
  $('a.a-colors, .choose-colors a').click(function(){	
    set_colors($(this).attr('rel'));
    return false;
  });
  
  $('.a-settings a,.saveit .closepopped').click(function(){
    $('.popped').slideToggle('fast');
    return false;
  });
  
  $('.saveit .default').click(function(){
    set_default_settings();
    return false;
  });
  
});
        
            
