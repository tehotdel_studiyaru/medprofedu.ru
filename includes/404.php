<?php 
class not_found {
	var $over = "";
	function getView() {
		if (getenv ( 'REQUEST_URI' ) == '/') {
			$this->setFileTemplate ( 'index' );
		}
		ob_start ();
		include ('Views/404/index.phtml');
		$this->over = ob_get_clean ();
	}
	function __construct() {
		header ( "HTTP/1.x 404 Not Found" );
	}
}
?>