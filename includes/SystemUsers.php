<?php 

class SystemUsers extends Redactor_Ini {
	protected static $params = array ();
	protected static $acl = array (
			"pages" => array (
					"module" => 'pages'
			),
			"blocks" => array (
					"module" => 'blocks'
			),
			"news" => array (
					"module" => 'news'
			),
			"articles" => array (
					"module" => 'articles'
			),
			"price" => array (
					"module" => 'price'
			),
			"faq" => array (
					"module" => 'faq'
			),
			"files" => array (
					"module" => 'files'
			),
			"banners" => array (
					"module" => 'banners'
			),
			"slider" => array (
					"module" => 'slider'
			),
			"pricelistfiles" => array (
					"module" => 'pricelistfiles'
			),
			"shop-users" => array (
					"module" => 'shop'
			),
			"shop-newsletter" => array (
					"module" => 'shop'
			),
			"site_settings" => array (
					"module" => 'additional'
			),
			"UploadFiles" => array (
					"module" => 'additional'
			),
			"SystemUsers" => array (
					"module" => 'systemusers'
			)
	);
	static function iUser($login = null, $password = null) {
		if ($login == null && $password == null) {
			if (isset ( $_COOKIE ['user'] ) && isset ( $_COOKIE ['passwd'] )) {

				$user = $_COOKIE ['user'] ;
				$passwd =  $_COOKIE ['passwd'] ;
				$sql =self::query ( "select * from `users` where `login`='{$user}' and `password`='{$passwd}' limit 1" );
				if ($sql!=false && $sql->rowCount()>0) {
					self::$params = $sql->fetch ( PDO::FETCH_ASSOC );
					$_SESSION ['admin'] = $_COOKIE ['user'];
					return true;
				}
			}
		} else {
			$user =  $login;
			$passwd =  md5 ( $password ) ;
				
			$sql = self::query ( "select * from `users` where `login`='{$user}' and `password`='{$passwd}' limit 1" );
				if ($sql!=false && $sql->rowCount()>0) {

				self::$params =$sql->fetch ( PDO::FETCH_ASSOC );
				$_SESSION ['admin'] = $login;
				setcookie ( 'user', $login, time () + 60 * 60 * 24 * 7, '/', preg_replace ( "/www./", "", "." . getenv ( 'HTTP_HOST' ) ) );
				setcookie ( 'passwd', md5 ( $password ), time () + 60 * 60 * 24 * 7, '/', preg_replace ( "/www./", "", "." . getenv ( 'HTTP_HOST' ) ) );

				return true;
			}
		}
		return false;
	}
	static function isAllowed($resource) {
		if (isset ( self::$params [$resource] ) && self::$params [$resource] == 1) {
			return true;
		}
		return false;
	}
}
?>