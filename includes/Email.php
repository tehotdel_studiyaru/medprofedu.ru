<?php 
class Email {
	protected $attache = array ();
	protected $error = array ();
	protected $hash = null;
	protected $headers = "";
	protected $message = "";
	protected $from = "";
	protected $charset = "windows-1251";
	protected $TransferEncoding = "Quot-Printed";
	function setFrom($from) {
		if (is_string ( $from ) && ! empty ( $from )) {
			$this->from = $from;
		}
	}
	function __construct() {
		$this->hash = "==x" . md5 ( time () ) . "x";
	}

	// modified: attachments possible
	function EmailHTML($to, $subject = "", $html = "", $path = false, $name = false) {
		if ($path) {
			$fp = fopen ( $path, "rb" );
			if (! $fp) {
				print "�������� � ��������� ����� ����� ���������";
				exit ();
			}
			$file = fread ( $fp, filesize ( $path ) );
			fclose ( $fp );
		}
		$this->headers .= "MIME-Version: 1.0\n";
		if (! empty ( $this->from )) {
			$this->headers .= "From:$this->from\n";
		} else {
			$this->headers .= "From: info@" . str_replace ( "www.", "", getenv ( 'HTTP_HOST' ) ) . "\n";
		}
		$this->headers .= "X-Sender: < " . getenv ( 'HTTP_HOST' ) . " >\n";
		$this->headers .= "Content-Type: multipart/mixed;";
		$this->headers .= " boundary=\"{$this->hash}\"\n\n";

		$this->message .= "--{$this->hash}\n";
		$this->message .= "Content-Type:text/html; charset=\"{$this->charset}\"\n";
		$this->message .= "Content-Transfer-Encoding: {$this->TransferEncoding}\n\n";
		$this->message .= $html . "\n\n";

		if (count ( $this->attache ) > 0) {
			$this->message .= $this->addFiles ();
		}
		mail ( $to, $subject, $this->message, $this->headers );
	}
	function EmailTXT($to, $subject, $html) {
		$this->headers .= "MIME-Version: 1.0\n";
		if (! empty ( $this->from )) {
			$this->headers .= "From:$this->from\n";
		}
		$this->headers .= "X-Sender: < " . getenv ( 'HTTP_HOST' ) . " >\n";
		$this->headers .= "Content-Type: multipart/mixed;";

		$this->headers .= " boundary=\"{$this->hash}\"\n\n";
		$this->message .= "--{$this->hash}\n";
		$this->message .= "Content-Type:text/plain; charset=\"{$this->charset}\"\n";
		$this->message .= "Content-Transfer-Encoding: {$this->TransferEncoding}\n\n";

		$this->message .= $html . "\n\n";

		if (count ( $this->attache ) > 0) {
			$this->message .= $this->addFiles ();
		}
		mail ( $to, $subject, $this->message, $this->headers );
	}
	function addFiles() {
		$a = "";
		foreach ( $this->attache as $file ) {
			$a .= "--{$this->hash}\n";
			$a .= "Content-Type: {$file[2]}; name=\"{$file[1]}\"\n";
			$a .= "Content-Transfer-Encoding: base64\n";
			$a .= "Content-Disposition: attachment\n\n";
			$a .= $file [0];
			$a .= "\n";
		}
		var_dump ( $a );
		die ();
		return $a;
	}
	function addFile($file, $filename = "file") {
		if (empty ( $filename )) {
			$filename = "file";
		}
		$info = $this->fileInfo ( $file );
		$type = $this->getType ( $info ['extReal'] );
		$data = file_get_contents ( $file );
		$data = chunk_split ( base64_encode ( $data ) );
		$this->attache [] = array (
				$data,
				$filename . $info ['ext'],
				$type
		);
	}
	function addFileUploaded($tmp, $realFile, $filename = "file") {
		if (empty ( $filename )) {
			$filename = "file";
		}
		$info = $this->fileInfo ( $realFile );
		$type = $this->getType ( $info ['extReal'] );
		$data = file_get_contents ( $tmp );
		$data = chunk_split ( base64_encode ( $data ) );
		$this->attache [] = array (
				$data,
				$filename . $info ['ext'],
				$type
		);
	}
	function getType($ext) {
		$type = "application/octet-stream";
		switch ($ext) {
			case "oda" :
				$type = "application/oda";
				break;
			case "pdf" :
				$type = "application/pdf";
				break;
			case "eps" :
			case "ps" :
			case "ai" :
				$type = "application/postscript";
			case "rtf" :
				$type = "application/rtf";
				break;
			case "bcpio" :
				$type = "application/x-bcpio";
				break;
			case "cpio" :
				$type = "application/x-cpio";
				break;
			case "csh" :
				$type = "application/x-csh";
				break;
			case "dvi" :
				$type = "application/x-dvi";
				break;
			case "gtar" :
				$type = "application/x-gtar";
				break;
			case "hdf" :
				$type = "application/x-hdf";
				break;
			case "latex" :
				$type = "application/x-latex";
				break;
			case "mif" :
				$type = "applicatlon/x-mif";
				break;
			case "cdf" :
			case "nc" :
				$type = "application/x-netcdf";
					
			case "sh" :
				$type = "application/x-sh";
				break;
			case "shar" :
				$type = "application/x-shar";
				break;
			case "sv4cpio" :
				$type = "application/x-sv4cpio";
				break;
			case "sv4crc" :
				$type = "application/x-sv4crc";
				break;
			case "tar" :
				$type = "application/x-tar";
				break;
			case "tcl" :
				$type = "application/x-tcl";
				break;
			case "tex" :
				$type = "application/x-tex";
				break;
			case "texi" :
			case "texinfo" :
				$type = "application/x-texinfo";
				break;
			case "man" :
				$type = "application/x-troff-man";
				break;
			case "me" :
				$type = "application/x-troff-me";
				break;
			case "ms" :
				$type = "application/x-troff-ms";
				break;
			case "tr" :
			case "roff" :
			case "t" :
				$type = "applicatlon/x-troff";
					
			case "ustar" :
				$type = "application/x-ustar";
				break;
			case "src" :
				$type = "application/x-wais-source";
				break;
			case "zip" :
				$type = "application/zip";
				break;
			case "snd" :
			case "au" :
				$type = "application/basic";
				break;
			case "aif" :
			case "aiff" :
			case "aifc" :
				$type = "application/x-aiff";
			case "wav" :
				$type = "application/x-wav";
				break;
			case "gif" :
				$type = "image/gif";
				break;
			case "ief" :
				$type = "image/ief";
				break;
			case "jpeg" :
			case "jpg" :
			case "jpe" :
				$type = "image/jpeg";
				break;
			case "png" :
				$type = "image/png";
				break;
			case "tiff" :
			case "tif" :
				$type = "image/tiff";
				break;
			case "ras" :
				$type = "image/x-cmu-raster";
				break;
			case "rpnm" :
				$type = "image/x-portable-anymap";
				break;
			case "pbm" :
				$type = "image/x-portable-bitmap";
				break;
			case "pgm" :
				$type = "image/x-portable-graymap";
				break;
			case "ppm" :
				$type = "image/x-portable-pixmap";
				break;
			case "rgb" :
				$type = "image/x-rgb";
				break;
			case "xbm" :
				$type = "image/x-xbitmap";
				break;
			case "xpm" :
				$type = "imaqe/x-xpixrnap";
				break;
			case "xwd" :
				$type = "image/x-xwindowdump";
				break;
			case "htm" :
			case "html" :
				$type = "text/html";
				break;
			case "txt" :
				$type = "text/plain";
				break;
			case "rtx" :
				$type = "text/richtext";
				break;
			case "tsv" :
				$type = "text/tab-separated-values";
				break;
			case "etx" :
				$type = "text/x-setext";
				break;
			case "mpeg" :
			case "mpe" :
			case "mpg" :
				$type = "video/mpeg";
				break;
			case "qt" :
			case "mov" :
				$type = "video/quicktime";
				break;
			case "qvi" :
				$type = "video/x-msvideo";
				break;
			case "movie" :
				$type = "video/x-sgi-movie";
				break;
			default :
				$type = "application/octet-stream";
				break;
		}
		return $type;
	}
	function fileInfo($file) {
		$path = pathinfo ( $file );
		if (! isset ( $path ['extension'] )) {
			$path ['extension'] = "";
		}
		return array (
				"ext" => (! empty ( $path ['extension'] )) ? ".{$path['extension']}" : '',
				"extReal" => strtolower ( $path ['extension'] )
		);
	}
}
?>