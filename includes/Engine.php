<?php
putenv ( 'TMPDIR=' . dirname ( dirname ( __FILE__ ) ) . "/tmp" );
define ( 'DEBUG', true );
define ( 'APPLICATION_PATH', dirname ( dirname ( __FILE__ ) ) );
define ( 'PEARPATH', dirname ( __FILE__ ) . '/extra' );
define ( 'Redactor_Engine', true );
$ModulesPaths = array ();

$ModulesPaths [] = realpath ( APPLICATION_PATH . '/includes' );
$ModulesPaths [] = realpath ( APPLICATION_PATH . '/includes/extra' );
$ModulesPaths [] = realpath ( APPLICATION_PATH );
$ModulesPaths [] = get_include_path ();

set_include_path ( implode ( PATH_SEPARATOR, $ModulesPaths ) );
require_once 'Headers.php';
require_once 'Breadcrumbs.php';
require_once 'Email.php';
require_once 'Metas.php';
require_once '404.php';
require_once 'SystemUsers.php';
class rewriteUrls extends Redactor_Ini {
	static $cache = array ();
	static $item = false;
	static $cat = false;
	static $noIsModule = array (
			'articles',
			'shop',
			'news',
			'clients',
			'partners',
			'gallery',
			'faq',
			'pages',
			'catalog',
			'brands' 
	);
	static $isBe = array ();
	private static $Instance = null;
	static function getInstance() {
		if (self::$Instance == null) {
			self::$Instance = new self ();
		}
		
		return self::$Instance;
	}
	// ���������� ����� ������� ������ � ����������� URL
	static function tryIt($array) {
		$match = array ();
		$module = isset ( $array ['module'] ) ? $array ['module'] : (isset ( $array [0] ) ? $array [0] : '');
		unset ( $array [0] );
		if (count ( $array ) == 1 && is_numeric ( $array [1] )) {
			$match ['module'] = $module;
			$match [$module] = $array [1];
		} elseif (count ( $array ) == 2 && ! is_numeric ( $array [1] ) && $array [1] == 'catid' && is_numeric ( $array [2] )) {
			$match ['module'] = $module;
			$match ['catid'] = $array [2];
		} elseif (count ( $array ) == 0) {
			$match ['module'] = $module;
		}
		
		return $match;
	}
	// �������� �� ���������� �������� URL �� ������� � �������
	static function match($url) {
		$path = parse_url ( $url, PHP_URL_PATH );
		
		$path = explode ( "/", $path );
		
		$new = array ();
		$i = 0;
		$isCat = 0;
		foreach ( $path as $num => $p ) {
			$p = trim ( urldecode ( $p ) );
			if (empty ( $p )) {
				continue;
			}
			$i ++;
			
			if ($i == 2 && $p == 'category') {
				$isCat = 1;
				
				continue;
			}
			
			$new [] = $p;
		}
		
		$isNew = false;
		
		$last = 0;
		$match = array ();
		$module = (count ( $p ) == 1 && is_numeric ( $p ) ? 'pages' : false);
		foreach ( $new as $num => $p ) {
			
			if (($el = self::find ( $p, $last, $module, $isCat )) != false) {
				
				$last = $el ['objId'];
				$match = array (
						'module' => $el ['module'] 
				);
				
				$module = $el ['module'];
				
				if ($el ['objId'] == 0 && in_array ( $el ['module'], self::$noIsModule )) {
					
					continue;
				}
				
				if ($el ['isCat'] == 1) {
					$match ['catid'] = $el ['objId'];
				} else {
					$match [$el ['module']] = $el ['objId'];
				}
			} elseif (in_array ( $p, self::$noIsModule )) {
				
				$match ['module'] = $p;
				$module = $p;
			} elseif (is_numeric ( $p )) {
				if (isset ( $match ['module'] )) {
					$m = $match;
					$m [$match ['module']] = $p;
					$m2 = $m;
					$m2 ['module'] = self::getModuleName ( $m ['module'] );
				} else {
					$m = $m2 = array (
							'module' => 'pages',
							'pages' => $p 
					);
					unset ( $m2 ['module'] );
				}
				
				if (($u = self::getUrl ( $m )) != "/" . implode ( '/', $m2 ) && $u != implode ( '/', $m2 )) {
					self::getInstance ()->saveHit ( true );
					// exit("{$u} === ".implode ( '/', $m2 ));
					header ( "Location: {$u}", true, 301 );
					
					exit ();
				} else {
					
					return self::tryIt ( array_values ( $m ) );
				}
			} else {
				if (($mm = self::getModuleByName ( $new [0] ))) {
					$new [0] = self::getModuleByName ( $new [0] );
				}
				
				return self::tryIt ( $new );
			}
		}
		
		return $match;
	}
	// ����� ����� URL �� ������� � �������
	static function find($name, $parent = 0, $module = false, $isCat = 0) {
		$dop = '';
		if ($module) {
			$dop = " and `module`='" . addslashes ( $module ) . "'";
		}
		if ($isCat == 1) {
			$dop = ' and `isCat`=1';
			;
		}
		
		$sql = self::getInstance ()->query ( "select `module`, `objId`, `isCat`, `parentId`, `custom` from `rewriteUrls` where `name`='" . addslashes ( $name ) . "' and `parentId`='{$parent}' {$dop} or `custom`='" . addslashes ( $name ) . "' and `parentId`='{$parent}' {$dop} limit 1" );
		
		if ($sql != false && $sql->rowCount () > 0) {
			
			return $sql->fetch ( PDO::FETCH_ASSOC );
		}
		return false;
	}
	// static function getRow($module, $parentId, $current){
	// $sth = self::getStatement()->Stm("select `objId`, `parentId`, `isCat` from `rewriteUrls` where `module`=? and `objId`=?");
	// }
	// �������� ������������ ����� URL
	protected function getParent($id, $module, $isCat = 2, &$urls, $params = array()) {
		$dop = '';
		/*
		 * if ($isCat != 2) { if ($isCat == 1) { $dop = ' and `isCat`="1"'; } else { $dop = ' and `isCat`="0"'; } }
		 */
		
		if ($params ['isCat'] == 0 && $params ['parentId'] != 0) {
			$dop = ' and `isCat`="1"';
		} elseif ($params ['isCat'] == 1 && $params ['parentId'] != 0) {
			$dop = ' and `isCat`="1"';
		} else {
			$dop = ' and `isCat`="0"';
		}
		
		if ($module == 'pages') {
			$dop = ' and `isCat`="0"';
		}
		
		$sql = self::getInstance ()->query ( "select `parentId`, `module`,`name`, `objId`, `isCat`, `custom` from `rewriteUrls` where `module`='" . addslashes ( $module ) . "' and `objId`='{$id}' {$dop}  limit 1" );
		if ($sql != false && $sql->rowCount () > 0) {
			$row = $sql->fetch ( PDO::FETCH_ASSOC );
			if (empty ( $row ['name'] )) {
				$row ['name'] = $row ['objId'];
			}
			if (! empty ( $row ['custom'] )) {
				$row ['name'] = $row ['custom'];
			}
			if (in_array ( $row ['module'], self::$noIsModule ) && $row ['parentId'] == 0 && $row ['objId'] != 0) {
				if ($row ['isCat'] == 0) {
					$isCat = 2;
				} else {
					$isCat = 1;
				}
				self::getParent ( 0, $row ['module'], $isCat, $urls, $row );
			} elseif ($row ['parentId'] != 0) {
				if ($row ['isCat'] == 0) {
					$isCat = 2;
				} else {
					$isCat = 1;
				}
				self::getParent ( $row ['parentId'], $row ['module'], $isCat, $urls, $row );
			}
			$urls [] = urlencode ( $row ['name'] );
		} elseif (in_array ( $module, self::$noIsModule )) {
			
			$sql = self::getInstance ()->query ( "select `parentId`, `module`,`name`, `custom` from `rewriteUrls` where `module`='" . addslashes ( $module ) . "' and `objId`='0' limit 1" );
			if ($sql != false && $sql->rowCount () > 0) {
				$row = $sql->fetch ( PDO::FETCH_ASSOC );
				
				if (! empty ( $row ['custom'] )) {
					$row ['name'] = $row ['custom'];
				}
				
				$urls [] = urlencode ( $row ['name'] );
			}
		}
	}
	// �������� ������� �������� ������ �� ����� URL
	static function getModuleByName($module) {
		$objId = 0;
		$dop = '';
		
		$module = urldecode ( $module );
		$urls = array ();
		
		$sql = self::getInstance ()->query ( "select `module` from `rewriteUrls` where `name`='" . addslashes ( $module ) . "' and `parentId`='0' and `isCat`='0' and `objId`='0' limit 1" );
		if ($sql != false && $sql->rowCount () > 0) {
			$row = $sql->fetch ( PDO::FETCH_ASSOC );
			
			$urls [] = $row ['module'];
		}
		
		return implode ( '/', $urls );
	}
	// �������� ������� �������� ������
	static function getModuleName($module) {
		$objId = 0;
		$dop = '';
		
		$urls = array ();
		
		$sql = self::getInstance ()->query ( "select `name` from `rewriteUrls` where `module`='" . addslashes ( $module ) . "' and `parentId`='0' and `isCat`='0' and `objId`='0' limit 1" );
		if ($sql != false && $sql->rowCount () > 0) {
			$row = $sql->fetch ( PDO::FETCH_ASSOC );
			
			$urls [] = $row ['name'];
		}
		
		return implode ( '/', $urls );
	}
	// �������� URL ������ ������� ���� ������������ ��������������
	static function getSingleUrl($params) {
		$objId = 0;
		$dop = '';
		
		if (isset ( $params ['catid'] )) {
			$dop = " and `objId`='" . ( int ) $params ['catid'] . "' and `isCat`='1'";
			unset ( $params ['catid'] );
		} elseif (isset ( $params [$params ['module']] )) {
			$objId = $params [$params ['module']];
			$dop = " and `objId`='" . ( int ) $params [$params ['module']] . "'";
			unset ( $params [$params ['module']] );
		} else {
			$dop = " and `parentId`='0' and `objId`='0'";
		}
		
		$module = $params ['module'];
		unset ( $params ['module'] );
		$urls = array ();
		
		$sql = self::getInstance ()->query ( "select `parentId`, `module`,`name`, `objId` from `rewriteUrls` where `module`='" . addslashes ( $module ) . "' {$dop} limit 1" );
		if ($sql != false && $sql->rowCount () > 0) {
			$row = $sql->fetch ( PDO::FETCH_ASSOC );
			$urls [] = $row ['name'];
		}
		
		return implode ( '/', $urls );
	}
	// �������� URL ������ ������� ���� ������������ � ������
	static function getSingleCustomUrl($params) {
		$objId = 0;
		$dop = '';
		if (isset ( $params ['catid'] )) {
			$dop = " and `objId`='" . ( int ) $params ['catid'] . "' and `isCat`='1'";
			unset ( $params ['catid'] );
		} elseif (isset ( $params [$params ['module']] )) {
			$objId = $params [$params ['module']];
			$dop = " and `objId`='" . ( int ) $params [$params ['module']] . "'";
			unset ( $params [$params ['module']] );
		} else {
			$dop = " and `parentId`='0' and `objId`='0'";
		}
		
		$module = $params ['module'];
		unset ( $params ['module'] );
		$urls = array ();
		
		$sql = self::getInstance ()->query ( "select `parentId`, `module`,`custom`, `objId` from `rewriteUrls` where `module`='" . addslashes ( $module ) . "' {$dop} limit 1" );
		if ($sql != false && $sql->rowCount () > 0) {
			$row = $sql->fetch ( PDO::FETCH_ASSOC );
			$urls [] = $row ['custom'];
		}
		
		return implode ( '/', $urls );
	}
	// ��������� URL ������
	static function saveUrl($params) {
		$old = $params;
		$objId = 0;
		$parentId = 0;
		$isCat = 0;
		if (isset ( $params ['catid'] )) {
			$dop = " and `objId`='" . ( int ) $params ['catid'] . "' and `isCat`='1'";
			$objId = ( int ) $params ['catid'];
			
			$isCat = 1;
			
			unset ( $params ['catid'] );
		} elseif (isset ( $params [$params ['module']] )) {
			
			$dop = " and `objId`='" . ( int ) $params [$params ['module']] . "' and `isCat`='0'";
			$objId = ( int ) $params [$params ['module']];
		} else {
			$dop = " and `parentId`='0' and `objId`='0'";
		}
		if (! isset ( $params ['custom'] )) {
			$params ['custom'] = self::getSingleCustomUrl ( $old );
		}
		if (! isset ( $params ['name'] )) {
			$params ['name'] = self::getSingleUrl ( $old );
		}
		if (isset ( $params ['parentId'] )) {
			$parentId = ( int ) $params ['parentId'];
			$dop2 = " and `parentId`='{$parentId}'";
		}
		$count = 0;
		$stm = self::getInstance ()->query ( "select count(1) from `rewriteUrls` where `module`='" . addslashes ( $params ['module'] ) . "' {$dop}" );
		if ($stm != false && $stm->rowCount () > 0) {
			$count = $stm->fetchColumn ();
		}
		if ($count == 0) {
			self::getInstance ()->exec ( "insert into `rewriteUrls` (`name`, `module`, `parentId`, `objId`, `isCat`, `custom`) values ('" . addslashes ( $params ['name'] ) . "','" . addslashes ( $params ['module'] ) . "','{$parentId}','{$objId}', '{$isCat}', '" . addslashes ( $params ['custom'] ) . "')" );
		} else {
			self::getInstance ()->exec ( "update `rewriteUrls` set `name`='" . addslashes ( $params ['name'] ) . "', `custom`='" . addslashes ( $params ['custom'] ) . "', `parentId`='{$parentId}', `isCat`='{$isCat}' where `module`='" . addslashes ( $params ['module'] ) . "' {$dop}" );
		}
	}
	// �������� ������ URL ������
	static function getUrl($params, $additionalParams = array()) {
		$objId = 0;
		$dop = '';
		$catId = 0;
		$isCat = 2;
		self::$cat = false;
		self::$item = false;
		
		$ss = $params;
		if (isset ( $params [$params ['module']] ) && ! is_numeric ( $params [$params ['module']] )) {
			
			unset ( $params [$params ['module']] );
		}
		$trueCat = 0;
		
		if (isset ( $params ['catid'] )) {
			
			self::$cat = true;
			$dop = " and `objId`='" . ( int ) $params ['catid'] . "' and `isCat`='1'";
			$catId = ( int ) $params ['catid'];
			$isCat = 1;
			$trueCat = 1;
			unset ( $params ['catid'] );
		} elseif (isset ( $params [$params ['module']] ) && is_numeric ( $params [$params ['module']] )) {
			$objId = $params [$params ['module']];
			$isCat = 0;
			self::$item = true;
			
			$dop = " and `objId`='" . ( int ) $params [$params ['module']] . "' and `isCat`='0'";
			unset ( $params [$params ['module']] );
		} else {
			$dop = " and `parentId`='0' and `objId`='0'";
		}
		
		$module = $params ['module'];
		unset ( $params ['module'] );
		$urls = array ();
		
		$sql = self::getInstance ()->query ( "select `parentId`, `module`,`name`, `objId`, `isCat`, `custom` from `rewriteUrls` where `module`='" . addslashes ( $module ) . "' {$dop} limit 1" );
		
		if ($sql != false && $sql->rowCount () > 0) {
			$row = $sql->fetch ( PDO::FETCH_ASSOC );
			
			if (empty ( $row ['name'] )) {
				$row ['name'] = $row ['objId'];
			}
			if (! empty ( $row ['custom'] )) {
				$row ['name'] = $row ['custom'];
			}
			
			if (in_array ( $row ['module'], self::$noIsModule ) && $row ['parentId'] == 0 && $row ['objId'] != 0) {
				
				self::getParent ( 0, $row ['module'], $isCat, $urls, $row );
				
				$urls [] = urlencode ( $row ['name'] );
			} elseif ($row ['parentId'] == 0 && $row ['objId'] == 0) {
				$module = $row ['name'];
			} elseif ($row ['parentId'] != 0) {
				if ($row ['isCat'] == 0) {
					$isCat = 1;
				} elseif (self::$cat == true) {
					$isCat = 1;
				} else {
					$isCat = 2;
				}
				
				self::getParent ( $row ['parentId'], $row ['module'], $isCat, $urls, $row );
				$urls [] = urlencode ( $row ['name'] );
			}
		} elseif (($m = self::getModuleName ( $module ))) {
			if (! empty ( $m )) {
				$module = urlencode ( $m );
			} else {
				unset ( $module );
			}
		} else {
			unset ( $module );
		}
		
		if (count ( $urls ) == 0) {
			if (isset ( $module )) {
				$urls [] = $module;
			}
			if ($objId != 0) {
				$urls [] = $objId;
			} 

			elseif ($catId != 0) {
				$urls [] = 'catid';
				$urls [] = $catId;
			}
		}
		
		$params = array_merge ( $params, $additionalParams );
		$query = http_build_query ( $params );
		if (! empty ( $query )) {
			$query = '?' . $query;
		}
		
		return '/' . implode ( '/', $urls ) . '' . $query;
	}
}
class PDO_Ex extends PDO {
	function __construct($dsn, $username = "", $password = "", $driver_options = array()) {
		parent::__construct ( $dsn, $username, $password, $driver_options );
		// $this->setAttribute(PDO::ATTR_STATEMENT_CLASS, array('DBStatement', array($this)));
	}
	function prepare ( $statement , $driver_options = array()  ){
		$sth = parent::prepare($statement, $driver_options);
		$sth->setFetchMode ( PDO::FETCH_OBJ );
		return $sth;
	}
	function query($query) {
		$statement = parent::query ( $query );
		
		$statement->setFetchMode ( PDO::FETCH_OBJ );
		return $statement;
	}
}
class DBStatement extends PDOStatement {
	public $dbh;
	protected function __construct($dbh) {
		$this->dbh = $dbh;
	}
}
class Redactor_Ini {
	private $DBName = null;
	private $DBUser = null;
	private $DBPassword = null;
	private $DBHost = null;
	private $DBPort = 3306;
	private $DBCharset = 'cp1251';
	protected static $Stm = array ();
	protected static $Adapter = null;
	var $CurrentModule = '';
	var $CurrentObjID = 0;
	var $CurrentCatID = 0;
	
	/* ���������������� ���� */
	var $UserFieldsGroupsTable = 'UserFields_Groups';
	var $UserFieldsTable = 'UserFields';
	var $UserFieldsValues = 'UserFields_Values';
	var $UserFieldsTypes = array (
			0 => '',
			1 => '',
			2 => '',
			3 => '',
			4 => '' 
	);
	
	// TODO: ������� ������� ������� ���������� ������
	public function update($table, $fields, $where = array()) {
		$query = "UPDATE `{$table}` SET ";
		$f = array ();
		$v = array ();
		$params = array ();
		foreach ( $fields as $name => $value ) {
		}
	}
	// TODO: ������� ������� ������� ������� ������
	public function insert($table, $fields) {
	}
	function addUserField($params = array('table'=>'', 'Module'=>null, 'isCategory'=>0, 'Group'=>0), $fields = array()) {
		if (! is_array ( $params ) or ! is_array ( $fields ) or is_array ( $fields ) && count ( $fields ) == 0) {
			return false;
		}
		$params = array_merge ( array (
				'table' => '',
				'Module' => null,
				'isCategory' => 0,
				'Group' => 0 
		), $params );
		if (empty ( $params ['table'] ) or $params ['Module'] == null or ! isset ( $fields ['Type'] ) or $fields ['Type'] > 4 or $fields ['Type'] < 0) {
			return false;
		}
		$f = array ();
		$values = array ();
		foreach ( $fields as $name => $value ) {
			if ($name != 'Id') {
				$f ["`{$name}`"] = "?";
				$values [] = $value;
			}
		}
		$f ['`Group`'] = "?";
		$values [] = ( int ) $params ['Group'];
		$f ['`isCategory`'] = "?";
		$values [] = ( int ) $params ['isCategory'];
		$f ['`Module`'] = "?";
		$values [] = $params ['Module'];
		$sth = $this->prepare ( "INSERT INTO `{$this->UserFieldsTable}` (" . implode ( ", ", array_keys ( $f ) ) . ") VALUES (" . implode ( ", ", $f ) . ");" );
		if ($sth != false && ($sth->execute ( $values )) != false) {
			return $this->addFieldToTable ( array (
					'type' => $fields ['Type'],
					'table' => $params ['table'],
					'Id' => $this->lastId (),
					'default' => (isset ( $fields ['default'] ) ? $fields ['default'] : '') 
			) );
		}
		return false;
	}
	function addUserFieldGroup($params) {
		return $this->UpdateUserFieldsGroup ( $params );
	}
	function UpdateUserFieldsGroup($params = array('Module'=>null, 'isCategory'=>0, 'Title'=>'', 'Id'=>0)) {
		if (! is_array ( $params )) {
			return false;
		}
		$params = array_merge ( array (
				'Module' => null,
				'isCategory' => 0,
				'Title' => '',
				'Id' => 0 
		), $params );
		if ($params ['Module'] == null) {
			return false;
		}
		if ($params ['Id'] != 0) {
			$sth = $this->prepare ( "UPDATE `{$this->UserFieldsGroupsTable}` SET `Title`=?, `Module`=?, `isCategory`=? Where `Id`=?" );
			if ($sth != false && ($sth->execute ( array (
					$params ['Title'],
					$params ['Module'],
					$params ['isCategory'],
					$params ['Id'] 
			) )) != false) {
				return true;
			} else {
				return false;
			}
		} else {
			$sth = $this->prepare ( "INSERT INTO `{$this->UserFieldsGroupsTable}` (`Title`, `Module`, `isCategory`) VALUES (?, ?, ?)" );
			if ($sth != false && ($sth->execute ( array (
					$params ['Title'],
					$params ['Module'],
					$params ['isCategory'] 
			) )) != false) {
				return true;
			} else {
				return false;
			}
		}
		return true;
	}
	function updateUserField($params = array('table'=>'', 'Module'=>null, 'isCategory'=>0), $fields = array()) {
		if (! is_array ( $params ) or ! is_array ( $fields ) or is_array ( $fields ) && count ( $fields ) == 0) {
			return false;
		}
		$params = array_merge ( array (
				'table' => '',
				'Module' => null,
				'isCategory' => 0 
		), $params );
		if (empty ( $params ['table'] ) or $params ['Module'] == null or ! isset ( $fields ['Id'] ) or $fields ['Id'] == 0 or empty ( $fields ['Id'] ) or ! is_numeric ( $fields ['Id'] )) {
			return false;
		}
		$f = array ();
		$values = array ();
		foreach ( $fields as $name => $value ) {
			if ($name != 'Id') {
				$f [] = "`{$name}`=?";
				$values [] = $value;
			}
		}
		$values [] = ( int ) $params ['Id'];
		$values [] = $params ['Module'];
		$sth = $this->prepare ( "UPDATE `{$this->UserFieldsTable}` SET " . implode ( ", ", $f ) . " WHERE `Id`=? AND `Module`=?" );
		if ($sth != false && ($sth->execute ( $values )) != false) {
			return true;
		}
		return false;
	}
	
	/* END ���������������� ���� */
	function saveHit($r301 = false) {
		if ($r301) {
			$r301 = 1;
		} else {
			$r301 = 0;
		}
		$headers = array ();
		if (function_exists ( 'getallheaders' )) {
			$headers = getallheaders ();
		}
		$headers = array_merge ( $headers, headers_list () );
		$headers = serialize ( $headers );
		
		$stm = $this->Stm ( "insert into `Statistic` (`Module`, `Url`, `Headers`, `ObjID`, `Method`, `IP`, `USER_AGENT`, `REFERER`, `r301`, `responseCode`) VALUES (?, ?, ?, ?,?,?,?,?,?,?)" );
		if ($stm != false && ($stm->execute ( array (
				$this->CurrentModule,
				getenv ( 'REQUEST_URI' ),
				$headers,
				$this->CurrentObjID,
				getenv ( 'REQUEST_METHOD' ),
				getenv ( 'SERVER_ADDR' ),
				getenv ( 'HTTP_USER_AGENT' ),
				getenv ( 'HTTP_REFERER' ),
				$r301,
				http_response_code () 
		) ))) {
			return true;
		}
		return false;
	}
	
	// ���������� ���� �� �������������� �������
	// TODO: �������� ����������� �������� �������� ����, �������
	function formatDate($format, $date = null) {
		if ($date != null) {
			$date = strtotime ( $date );
		}
		$monthsFull = array (
				"01" => "������",
				"02" => "�������",
				"03" => "�����",
				"04" => "������",
				"05" => "���",
				"06" => "����",
				"07" => "����",
				"08" => "�������",
				"09" => "��������",
				"10" => "�������",
				"11" => "������",
				"12" => "�������" 
		);
		$monthsShort = array (
				"01" => "������",
				"02" => "�������",
				"03" => "����",
				"04" => "������",
				"05" => "���",
				"06" => "����",
				"07" => "����",
				"08" => "������",
				"09" => "��������",
				"10" => "�������",
				"11" => "������",
				"12" => "�������" 
		);
		$monthsMicro = array (
				"01" => "���",
				"02" => "���",
				"03" => "���",
				"04" => "���",
				"05" => "���",
				"06" => "���",
				"07" => "���",
				"08" => "���",
				"09" => "���",
				"10" => "���",
				"11" => "���",
				"12" => "���" 
		);
		
		if (preg_match ( "#FullMonth#is", $format )) {
			$format = str_replace ( "FullMonth", $this->ucfirst ( $monthsFull [date ( 'm', $date )] ), $format );
			$format = str_replace ( "fullmonth", $monthsFull [date ( 'm', $date )], $format );
		}
		if (preg_match ( "#ShortMonth#is", $format )) {
			$format = str_replace ( "ShortMonth", $this->ucfirst ( $monthsShort [date ( 'm', $date )] ), $format );
			$format = str_replace ( "shortmonth", $monthsShort [date ( 'm', $date )], $format );
		}
		
		if (preg_match ( "#micromonth#is", $format )) {
			$format = preg_replace ( "#micromonth#", $monthsMicro [date ( 'm', $date )], $format );
		}
		
		return date ( $format, $date );
	}
	function ucfirst($str, $enc = 'windows-1251') {
		return mb_strtoupper ( mb_substr ( $str, 0, 1, $enc ), $enc ) . mb_substr ( $str, 1, mb_strlen ( $str, $enc ), $enc );
	}
	public function getStm($Query) {
		return isset ( self::$Stm [$Query] ) ? self::$Stm [$Query] : false;
	}
	/**
	 *
	 * @param field_type $DBName        	
	 */
	private function setDBName($DBName) {
		$this->DBName = $DBName;
	}
	
	/**
	 *
	 * @param field_type $DBUser        	
	 */
	private function setDBUser($DBUser) {
		$this->DBUser = $DBUser;
	}
	
	/**
	 *
	 * @param field_type $DBPassword        	
	 */
	private function setDBPassword($DBPassword) {
		$this->DBPassword = $DBPassword;
	}
	
	/**
	 *
	 * @param field_type $DbHost        	
	 */
	private function setDBHost($DbHost) {
		$this->DBHost = $DbHost;
	}
	
	/**
	 *
	 * @param number $DbPort        	
	 */
	private function setDBPort($DbPort) {
		$this->DBPort = $DbPort;
	}
	
	/**
	 *
	 * @return the $Adapter
	 */
	public static function getAdapter() {
		return self::$Adapter;
	}
	public function Stm($Query) {
		if (isset ( self::$Stm [$Query] )) {
			return self::$Stm [$Query];
		}
		self::$Stm [$Query] = self::getAdapter ()->prepare ( $Query );
		return self::$Stm [$Query];
	}
	public function lastId($name = null) {
		return self::getAdapter ()->lastInsertId ( $name );
	}
	public function lastInsertId($name = null) {
		return self::getAdapter ()->lastInsertId ( $name );
	}
	public function prepare($Query) {
		if (isset ( self::$Stm [$Query] )) {
			return self::$Stm [$Query];
		}
		self::$Stm [$Query] = self::getAdapter ()->prepare ( $Query );
		return self::$Stm [$Query];
	}
	
	/**
	 *
	 * @param string $DbCharset        	
	 */
	private function setDBCharset($DbCharset) {
		$this->DBCharset = $DbCharset;
	}
	public function setAdapter($Adapter) {
		self::$Adapter = $Adapter;
	}
	function query($Query) {
		return self::getAdapter ()->query ( $Query );
	}
	function exec($Query) {
		return self::getAdapter ()->exec ( $Query );
	}
	function get($param) {
		$sql = $this->query ( "select `value` from `site_setting` where `option` LIKE '" . addslashes ( $param ) . "' limit 1" );
		if ($sql != false && $sql->rowCount () > 0) {
			return $sql->fetchColumn ();
		}
		return false;
	}
	// ����������� � ���� ������
	public function ConnectDB() {
		include ("includes/Config.php");
		$dsn = "mysql:dbname=" . $this->DBName . ";host=" . $this->DBHost . ";port={$this->DBPort}";
		try {
			self::setAdapter ( new PDO_Ex ( $dsn, $this->DBUser, $this->DBPassword, array (
					PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'' . $this->DBCharset . '\'' 
			// PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ
						) ) );
		} catch ( PDOException $e ) {
			echo $e->getMessage ();
		}
	}
	// ������ ������� require_once � ���������
	public function includeModule($module) {
		if (file_exists ( 'modules/' . $module . ".cls.php" )) {
			require_once ('modules/' . $module . ".cls.php");
		}
	}
	
	// ��������������� �������, ������� � Json
	function JEncode($arr, $en = 1) {
		require_once ("JSON.php"); // if php<5.2 need JSON class
		$json = new Services_JSON (); // instantiate new json object
		if ($en == 1) {
			$data = $json->encode ( $arr ); // encode the data in json format
		} else {
			$data = $json->decode ( $arr );
		}
		
		return $data;
	}
	// �� Utf-8 � Windows-1251
	public function utf2win($str) {
		$text = iconv ( "UTF-8", "windows-1251", $str );
		
		return $text;
	}
	// �� Windows-1251 � Utf-8
	public function win2utf($str) {
		$text = iconv ( "windows-1251", "UTF-8", $str );
		
		return $text;
	}
	
	// ��������
	function translit($str) {
		$str = $this->strtolower ( $str );
		$new = "~`!@#$%^&*()+=\\|�<>{}[]:;'\",.?/";
		$p = str_split ( $new );
		
		foreach ( $p as $n => $v ) {
			$str = trim ( str_replace ( $v, "", $str ) );
		}
		
		$new = array (
				"/�/" => "y",
				"/�/" => "c",
				"/�/" => "u",
				"/�/" => "k",
				"/�/" => "e",
				"/�/" => "e",
				"/�/" => "n",
				"/�/" => "g",
				"/�/" => "sh",
				"/�/" => "sh",
				"/�/" => "z",
				"/�/" => "h",
				"/�/" => "",
				"/�/" => "f",
				"/�/" => "i",
				"/�/" => "v",
				"/�/" => "a",
				"/�/" => "p",
				"/�/" => "r",
				"/�/" => "o",
				"/�/" => "l",
				"/�/" => "d",
				"/�/" => "j",
				"/�/" => "e",
				"/�/" => "ya",
				"/�/" => "ch",
				"/�/" => "s",
				"/�/" => "m",
				"/�/" => "i",
				"/�/" => "t",
				"/�/" => "",
				"/�/" => "b",
				"/�/" => "u" 
		);
		
		$str = preg_replace ( array_keys ( $new ), array_values ( $new ), $str );
		$str = trim ( $str );
		$str = str_replace ( "       ", "-", $str );
		$str = str_replace ( "      ", "-", $str );
		$str = str_replace ( "     ", "-", $str );
		$str = str_replace ( "    ", "-", $str );
		$str = str_replace ( "   ", "-", $str );
		$str = str_replace ( "  ", "-", $str );
		$str = str_replace ( " ", "-", $str );
		
		for($i = 15; $i >= 2; $i --) {
			$str = str_replace ( str_repeat ( '-', $i ), "-", $str );
		}
		
		return $str;
	}
	// ������ ������� strtolower
	function strtolower($str) {
		$big = "��������������������������������QWERTYUIOPASDFGHJKLZXCVBNM";
		$min = "��������������������������������qwertyuiopasdfghjklzxcvbnm";
		$split = str_split ( $big );
		$split2 = str_split ( $min );
		$ar = array ();
		foreach ( $split as $n => $t ) {
			$ar ["/$t/is"] = $split2 [$n];
		}
		return preg_replace ( array_keys ( $ar ), array_values ( $ar ), $str );
	}
}
class Redactor_Admin extends Redactor_Ini {
	var $login = false;
	
	// �������� ������ ���������� Ajax
	function __construct($exec = true) {
		if (isset ( $_GET ['xaction'] ) && $_GET ['xaction'] == 'exit') {
			unset ( $_SESSION ['admin'] );
			session_unset ();
			
			setcookie ( 'user', '', time (), '/', preg_replace ( "/www./", "", "." . getenv ( 'HTTP_HOST' ) ) );
			setcookie ( 'passwd', '', time (), '/', preg_replace ( "/www./", "", "." . getenv ( 'HTTP_HOST' ) ) );
			
			header ( "Location: /admincp.php" );
			exit ();
		}
		
		if ($exec == true) {
			
			$this->ConnectDB ();
			if (! SystemUsers::iUser ()) {
				$this->login = false;
				return false;
			}
			if (isset ( $_GET ['help'] )) {
				
				exit ();
			}
			if (isset ( $_POST ['xaction'] )) {
				$task = $_POST ['xaction'];
			} elseif (isset ( $_POST ['task'] )) {
				$task = $_POST ['task'];
			} elseif (isset ( $_GET ['xaction'] )) {
				$task = $_GET ['xaction'];
			} elseif (isset ( $_GET ['task'] )) {
				$task = $_GET ['task'];
			}
			if (isset ( $_POST ['module'] )) {
				$module = $_POST ['module'];
				$extclass = $module . "_admin";
			} elseif (isset ( $_GET ['module'] )) {
				$module = $_GET ['module'];
				$extclass = $module . "_admin";
			}
			
			if (! isset ( $module )) {
				$module = "";
			}
			if (! isset ( $task )) {
				$task = "";
			}
			if (! isset ( $extclass )) {
				$extclass = "";
			}
			
			if (isset ( $task ) && $task != "") {
				if (! class_exists ( $extclass )) {
					$this->includeModule ( ucfirst ( $module ) );
					if (class_exists ( $extclass )) {
						
						$mod = new $extclass ();
						$mod->$task ();
					}
				} else {
					$mod = new $extclass ();
					$mod->$task ();
				}
				exit ();
			}
		} else {
			$this->ConnectDB ();
			if (! SystemUsers::iUser ()) {
				$this->login = false;
				return false;
			}
		}
	}
	var $UserFieldsGroupsTable = 'UserFields_Groups';
	var $UserFieldsTable = 'UserFields';
	var $UserFieldsValuesTable = 'UserFields_Values';
	var $UserFieldsTypes = array (
			0 => '',
			1 => '',
			2 => '',
			3 => '',
			4 => '' 
	);
	public function getColumns($table) {
		$cols = array ();
		$sth = $this->Query ( "SHOW COLUMNS FROM `{$table}`" );
		if ($sth != false && $sth->rowCount () > 0) {
			foreach ( $sth->fetchAll () as $row ) {
				$cols [] = $row->Field;
			}
		}
		
		return $cols;
	}
	public function LoadUFGroup() {
		$Data = array ();
		$Id = isset ( $_POST ['Id'] ) ? ( int ) $_POST ['Id'] : 0;
		
		$table = $this->UserFieldsGroupsTable;
		
		$sth = $this->Stm ( "SELECT * FROM `{$table}` WHERE `Id`=? LIMIT 1" );
		if ($sth != false && ($sth->execute ( array (
				$Id 
		) )) != false && $sth->rowCount () > 0) {
			$row = $sth->fetch ( PDO::FETCH_ASSOC );
			foreach ( $row as $name => $value ) {
				$Data [$name] = $this->win2utf ( $value );
			}
		}
		echo json_encode ( array (
				"success" => true,
				"data" => $Data 
		) );
	}
	function addFieldToTable($params = array('table'=>null, 'Type'=>0, 'default'=>'', 'Id'=>0)) {
		if (! is_array ( $params )) {
			return false;
		}
		$params = array_merge ( array (
				'table' => '',
				'Type' => 0,
				'default' => '',
				'Id' => 0 
		), $params );
		if (empty ( $params ['table'] ) or empty ( $params ['Id'] ) or ! is_numeric ( $params ['Id'] )) {
			return false;
		}
		$params ['Id'] = ( int ) $params ['Id'];
		if ($params ['Type'] == 1 or $params ['Type'] == 2) {
			$this->exec ( "ALTER TABLE  `{$params['table']}` ADD  `uf_{$params['Id']}` TEXT NOT NULL ;" );
		} else {
			$this->exec ( "ALTER TABLE  `{$params['table']}` ADD  `uf_{$params['Id']}` VARCHAR( 1000 ) NOT NULL ;" );
		}
	}
	public function addUFField() {
		$params ['Title'] = isset ( $_POST ['Title'] ) ? $this->utf2win ( $_POST ['Title'] ) : '��� ��������';
		$params ['Type'] = isset ( $_POST ['Type'] ) ? ( int ) $_POST ['Type'] : 0;
		$params ['Group'] = isset ( $_POST ['Group'] ) ? ( int ) $_POST ['Group'] : 0;
		$params ['Module'] = isset ( $_POST ['module'] ) ? strtolower ( $_POST ['module'] ) : '';
		$params ['isCategory'] = isset ( $_POST ['isCategory'] ) ? strtolower ( $_POST ['isCategory'] ) : '';
		$sth = $this->prepare ( "INSERT INTO `UserFields` (`Module`,`Group`, `Type`, `isCategory`, `Title`) VALUES (?, ?,?,?,?)" );
		if ($sth != false && ($sth->execute ( array (
				$params ['Module'],
				$params ['Group'],
				$params ['Type'],
				$params ['isCategory'],
				$params ['Title'] 
		) ))) {
			if ($params ['isCategory'] == 1) {
				$params ['table'] = $this->CategoriesTable;
			} else {
				$params ['table'] = $this->ItemsTable;
			}
			$params ['Id'] = $this->lastId ();
			$this->addFieldToTable ( $params );
			echo json_encode ( array (
					"success" => true,
					'Id' => $params ['Id'] 
			) );
			return true;
		}
		echo json_encode ( array (
				"failure" => true 
		) );
	}
	// TODO: �������� � ������� ������� ����� �� ��������� � ����� ����
	public function DeleteUFCategory() {
		$Id = isset ( $_POST ['Id'] ) ? ( int ) $_POST ['Id'] : 0;
		if ($Id != 0) {
			$this->exec ( "DELETE FROM {$this->UserFieldsGroupsTable} WHERE `Id`='{$Id}' LIMIT 1" );
		}
		echo json_encode ( array (
				"success" => true 
		) );
	}
	public function SaveUFGroup() {
		$params ['Module'] = isset ( $_POST ['module'] ) ? strtolower ( $_POST ['module'] ) : '';
		$params ['Id'] = isset ( $_POST ['Id'] ) ? ( int ) $_POST ['Id'] : 0;
		$params ['Title'] = isset ( $_POST ['Title'] ) ? $this->utf2win ( $_POST ['Title'] ) : '';
		$params ['isCategory'] = isset ( $_POST ['isCategory'] ) ? ( int ) $_POST ['isCategory'] : 0;
		if ($params ['isCategory'] != 0 && $params ['isCategory'] != 1) {
			$params ['isCategory'] = 0;
		}
		if ($this->UpdateUserFieldsGroup ( $params )) {
			echo json_encode ( array (
					'success' => true 
			) );
		} else {
			echo json_encode ( array (
					'failure' => true,
					'error' => $this->getAdapter ()->errorInfo () 
			) );
		}
	}
	public function DeleteUF() {
		$Id = isset ( $_POST ['Id'] ) ? ( int ) $_POST ['Id'] : 0;
		$Module = isset ( $_POST ['module'] ) ? strtolower ( $_POST ['module'] ) : '';
		$this->exec ( "ALTER TABLE  `{$this->ItemsTable}` DROP  `uf_{$Id}`" );
		$this->exec ( "DELETE FROM `{$this->UserFieldsTable}` WHERE `Id`='{$Id}'" );
		$this->exec ( "DELETE FROM `{$this->UserFieldsValuesTable}` WHERE `UFId`='{$Id}'" );
		echo json_encode ( array (
				'success' => true 
		) );
	}
	public function SaveUFField() {
		$Id = isset ( $_POST ['Id'] ) ? ( int ) $_POST ['Id'] : 0;
		if (isset ( $_POST ['Id'] )) {
			unset ( $_POST ['Id'] );
		}
		$columns = $this->getColumns ( $this->UserFieldsTable );
		$fields = array ();
		$cols = array ();
		foreach ( $_POST as $name => $value ) {
			if (in_array ( $name, $columns )) {
				$fields [] = '`' . $name . '`=?';
				$cols [] = $this->utf2win ( $value );
			}
		}
		$cols [] = $Id;
		
		$sth = $this->prepare ( "UPDATE {$this->UserFieldsTable} SET " . implode ( ", ", $fields ) . " WHERE `Id`=?" );
		if ($sth != false && ($sth->execute ( $cols )) != false) {
			echo json_encode ( array (
					'success' => true 
			) );
			return true;
		}
		echo json_encode ( array (
				'failure' => true,
				'error' => $this->getAdapter ()->errorInfo () 
		) );
	}
	public function LoadUFRecords() {
		$module = isset ( $_POST ['module'] ) ? strtolower ( $_POST ['module'] ) : '';
		
		$results = array ();
		$rows = 0;
		$Group = isset ( $_POST ['Group'] ) ? ( int ) $_POST ['Group'] : 0;
		$count = $this->Stm ( "SELECT COUNT(1) FROM `{$this->UserFieldsTable}` WHERE `{$this->UserFieldsTable}`.`Group`=?" );
		
		$start = isset ( $_POST ['start'] ) ? ( int ) $_POST ['start'] : 0;
		$limit = isset ( $_POST ['limit'] ) ? ( int ) $_POST ['limit'] : 25;
		if ($count != false && ($count->execute ( array (
				$Group 
		) )) != false && $count->rowCount () > 0) {
			$rows = $count->fetchColumn ();
		}
		
		if ($rows > 0) {
			$sth = $this->Stm ( "SELECT `{$this->UserFieldsTable}`.`Id`, `{$this->UserFieldsTable}`.`Title`, `{$this->UserFieldsTable}`.`Sort`, `{$this->UserFieldsTable}`.`Type`
		FROM `{$this->UserFieldsTable}` WHERE `{$this->UserFieldsTable}`.`Group`=? AND `Module`=? ORDER BY `Sort` LIMIT {$start}, {$limit}" );
			
			if ($sth != false && ($sth->execute ( array (
					$Group,
					strtolower ( $_POST ['module'] ) 
			) )) != false && $sth->rowCount () > 0) {
				
				foreach ( $sth->fetchAll () as $row ) {
					
					$results [] = array (
							"Id" => $row->Id,
							"Sort" => $row->Sort,
							"Title" => $this->win2utf ( $row->Title ),
							"Type" => $row->Type 
					);
				}
			}
		}
		
		echo json_encode ( array (
				"total" => $rows,
				"results" => $results 
		) );
	}
	private function getField($row) {
		$field = array (
				'style' => array (
						'paddingLeft' => '0px' 
				) 
		);
		if ($row->Type == 1) {
			$field ['xtype'] = 'textarea';
			$field ['name'] = 'uf_' . $row->Id;
			$field ['id'] = 'uf_' . $row->Id;
			
			$field ['fieldLabel'] = $this->win2utf ( $row->Title );
		} elseif ($row->Type == 2) {
		} else {
			$field ['xtype'] = 'textfield';
			$field ['name'] = 'uf_' . $row->Id;
			$field ['id'] = 'uf_' . $row->Id;
			$field ['fieldLabel'] = $this->win2utf ( $row->Title );
		}
		return $field;
	}
	public function getUFGroupName($id) {
		$sth = $this->prepare ( "SELECT `Title` FROM `{$this->UserFieldsGroupsTable}` WHERE `Id`=? LIMIT 1" );
		if ($sth != false && ($sth->execute ( array (
				$id 
		) )) != false && $sth->rowCount () > 0) {
			return $sth->fetchColumn ();
		}
		return '��� ��������';
	}
	public function getUFJS($module, $Group = 0, $isCategory = 0) {
		$fields = array ();
		$fields2 = array ();
		$sth = $this->prepare ( "SELECT * FROM {$this->UserFieldsTable} WHERE `Module` LIKE ? AND `isCategory`=? AND `Group`=? ORDER BY `Sort` ASC" );
		if ($sth != false && ($sth->execute ( array (
				$module,
				$isCategory,
				0 
		) )) != false && $sth->rowCount () > 0) {
			
			foreach ( $sth->fetchAll () as $row ) {
				
				$fields2 [] = $this->getField ( $row );
			}
			$fields [] = array (
					'xtype' => 'fieldset',
					'title' => $this->win2utf ( '�����' ),
					'autoHeight' => true,
					'collapsible' => true,
					'defaults' => array (
							'xtype' => 'textfield' 
					),
					'layout' => 'form',
					'items' => $fields2 
			);
		}
		
		if ($Group != 0) {
			$fields2 = array ();
			$sth = $this->prepare ( "SELECT * FROM {$this->UserFieldsTable} WHERE `Module` LIKE ? AND `isCategory`=? AND `Group`=? ORDER BY `Sort` ASC" );
			if ($sth != false && ($sth->execute ( array (
					$module,
					$isCategory,
					$Group 
			) )) != false && $sth->rowCount () > 0) {
				
				foreach ( $sth->fetchAll () as $row ) {
					
					$fields2 [] = $this->getField ( $row );
				}
				$fields [] = array (
						'xtype' => 'fieldset',
						'defaults' => array (
								'xtype' => 'textfield' 
						),
						'title' => $this->win2utf ( $this->getUFGroupName ( $Group ) ),
						'autoHeight' => true,
						'collapsible' => true,
						'layout' => 'form',
						'items' => $fields2 
				);
			}
		}
		
		return $fields;
	}
	public function LoadUFCategories() {
		$Data = array (
				array (
						"Title" => $this->win2utf ( '�����' ),
						"Id" => 0,
						'text' => $this->win2utf ( '�����' ),
						"id" => 0,
						'icon' => '/core/icons/folder.png',
						'draggable' => false,
						"leaf" => true 
				) 
		);
		$module = isset ( $_POST ['module'] ) ? strtolower ( $_POST ['module'] ) : '';
		$sth = $this->Stm ( "Select `Id`, `Title` from `{$this->UserFieldsGroupsTable}` WHERE `Module`=? ORDER BY `Title`" );
		if ($sth != false && ($sth->execute ( array (
				$module 
		) )) != false && $sth->rowCount () > 0) {
			
			foreach ( $sth->fetchAll () as $row ) {
				$item = array (
						"Title" => $this->win2utf ( $row->Title ),
						"Id" => $row->Id,
						'text' => $this->win2utf ( $row->Title ),
						"id" => $row->Id,
						'icon' => '/core/icons/folder.png',
						'draggable' => false,
						"leaf" => true 
				);
				
				$Data [] = $item;
			}
		}
		echo json_encode ( $Data );
	}
	public function getUFGroups() {
		$Data = array (
				array (
						
						0,
						$this->win2utf ( '�� �������' ) 
				) 
		);
		$module = isset ( $_POST ['module'] ) ? strtolower ( $_POST ['module'] ) : '';
		$sth = $this->Stm ( "Select `Id`, `Title` from `{$this->UserFieldsGroupsTable}` WHERE `Module`=? ORDER BY `Title`" );
		if ($sth != false && ($sth->execute ( array (
				$module 
		) )) != false && $sth->rowCount () > 0) {
			
			foreach ( $sth->fetchAll () as $row ) {
				$Data [] = array (
						$row->Id,
						$this->win2utf ( $row->Title ) 
				);
			}
		}
		return $Data;
	}
	public function LoadUFField() {
		$Data = array ();
		$Id = isset ( $_POST ['Id'] ) ? ( int ) $_POST ['Id'] : 0;
		
		$table = $this->UserFieldsTable;
		
		$sth = $this->Stm ( "SELECT * FROM `{$table}` WHERE `Id`=? LIMIT 1" );
		if ($sth != false && ($sth->execute ( array (
				$Id 
		) )) != false && $sth->rowCount () > 0) {
			$row = $sth->fetch ( PDO::FETCH_ASSOC );
			foreach ( $row as $name => $value ) {
				$Data [$name] = $this->win2utf ( $value );
			}
		}
		echo json_encode ( array (
				"success" => true,
				"data" => $Data 
		) );
	}
}
class Redactor_Action extends Redactor_Ini {
	private $module = "pages";
	private static $FileTemplate = 'index';
	private static $Template = 'default';
	static $defaultUrls = array ();
	static $brands = array ();
	protected static $Collector = array ();
	var $content = '';
	
	/**
	 *
	 * @return the $Template
	 */
	public static function getTemplate() {
		return Redactor_Action::$Template;
	}
	
	/**
	 *
	 * @param string $Template        	
	 */
	public static function setTemplate($Template) {
		Redactor_Action::$Template = $Template;
	}
	
	// ������������� ������� ���� �������
	function setFileTemplate($filename) {
		self::$FileTemplate = $filename;
	}
	// ���������� ������� ���� �������
	function getFileTemplate() {
		return self::$FileTemplate;
	}
	// ��������� � ������ URL ������� �� ���������
	function addDefaultUrl($module, $string, $params = array()) {
		self::$defaultUrls [$module] [$string] = $params;
	}
	// ������������� ��� ������� URL �� ���������
	// TODO: ������� �������������� ��������� URL � ����������� �� ������������� �������
	function defineDefaultUrls() {
		self::addDefaultUrl ( "pages", "pages-(?P<pages>[0-9]+?).html" );
		self::addDefaultUrl ( "news", "news.html" );
		self::addDefaultUrl ( "news", "news-page-(?P<page>[0-9]+?).html" );
		self::addDefaultUrl ( "news", "news-(?P<news>[0-9]+?).html" );
		self::addDefaultUrl ( "articles", "articles.html" );
		self::addDefaultUrl ( "articles", "articles-page-(?P<page>[0-9]+?).html" );
		self::addDefaultUrl ( "articles", "articles-(?P<articles>[0-9]+?).html" );
		
		self::addDefaultUrl ( "faq", "faq.html" );
		self::addDefaultUrl ( "faq", "faq-page-(?P<page>[0-9]+?).html" );
	}
	// ������ ������� urlencode
	function urlencode($url) {
		if (substr ( $url, 0, 1 ) == '/') {
			$url = substr ( $url, 1, strlen ( $url ) );
		}
		$newUrl = $url;
		$ext = pathinfo ( $newUrl, PATHINFO_EXTENSION );
		
		if (strlen ( $ext ) > 0) {
			$ext = "." . $ext;
			$newUrl = substr ( $url, 0, strlen ( $url ) - strlen ( $ext ) );
		}
		$exp = explode ( '/', $newUrl );
		foreach ( $exp as $s => $u ) {
			$exp [$s] = urlencode ( $u );
		}
		return implode ( "/", $exp ) . $ext;
	}
	// ���������� false ��� ��������� ������ ������
	function getModule($module) {
		$file = ucfirst ( strtolower ( $module ) );
		$moduleClass = 'Redactor_Module_' . $file;
		if (isset ( self::$Collector [$moduleClass] )) {
			return self::$Collector [$moduleClass];
		}
		if (class_exists ( $moduleClass )) {
			self::$Collector [$moduleClass] = new $moduleClass ();
			return self::$Collector [$moduleClass];
		}
		$this->includeModule ( $file );
		if (class_exists ( $moduleClass )) {
			self::$Collector [$moduleClass] = new $moduleClass ();
			return self::$Collector [$moduleClass];
		}
		return false;
	}
	// ���������� ����� ����� �� ������ �����
	function getBlock($name) {
		$sql = $this->Stm ( "select `text` from `blocks` where `name` LIKE ? limit 1" );
		$sql->execute ( array (
				$name 
		) );
		if ($sql != false && $sql->rowCount () != false) {
			return $sql->fetchColumn ();
		}
		return '';
	}
	// ���������� ������ URL ��� ������
	function getUrl($params, $additionalParams = array()) {
		return rewriteUrls::getUrl ( $params, $additionalParams );
	}
	// ���������� false ��� ��������� ������ ��������� ����
	function getHelper($helperName) {
		$file = ucfirst ( strtolower ( $helperName ) );
		
		$moduleClass = 'Redactor_Helper_' . $file;
		if (isset ( self::$Collector [$moduleClass] )) {
			return self::$Collector [$moduleClass];
		}
		if (class_exists ( $moduleClass )) {
			self::$Collector [$moduleClass] = new $moduleClass ( $moduleClass );
			return self::$Collector [$moduleClass];
		}
		if (file_exists ( "Helpers/{$file}.php" )) {
			require_once "Helpers/{$file}.php";
		}
		
		if (class_exists ( $moduleClass )) {
			self::$Collector [$moduleClass] = new $moduleClass ( $moduleClass );
			return self::$Collector [$moduleClass];
		}
		return false;
	}
	
	// ���������� ��������� ������ ��� ������� � MySQL ������
	function changeFields($value) {
		return "`{$value}`";
	}
	function LayoutPath() {
		return "/template/default/";
	}
	// ���������� ������� �������� ������
	function getContent() {
		return $this->content;
	}
	function view() {
		$this->ConnectDB ();
		
		$path = parse_url ( getenv ( 'REQUEST_URI' ), PHP_URL_PATH );
		$query = parse_url ( getenv ( 'REQUEST_URI' ), PHP_URL_QUERY );
		$ext = pathinfo ( $path, PATHINFO_EXTENSION );
		if ($path != '/' && empty ( $ext ) && substr ( $path, - 1 ) == '/' && ! preg_match ( "#//#", $path )) {
			$new = substr ( $path, 0, strlen ( $path ) - 1 );
			if (! empty ( $query )) {
				$new .= '?' . $query;
			}
			$this->saveHit ( true );
			header ( "Location: {$new}", true, 301 );
			
			exit ();
		}
		
		$redirect = include ('redirect.php');
		if (isset ( $redirect [getenv ( 'REQUEST_URI' )] )) {
			$this->saveHit ( true );
			header ( "HTTP/1.1 301 Moved Permanently" );
			header ( "Location: {$redirect[getenv('REQUEST_URI')]}" );
			exit ();
		}
		
		self::defineDefaultUrls ();
		
		$this->matchUrl ();
		
		$this->initMetaTags ();
		
		$moduleClass = '';
		if (isset ( $_GET ['module'] )) {
			$moduleClass = "Redactor_Module_" . ucfirst ( $_GET ['module'] );
			if (! class_exists ( $moduleClass )) {
				$this->includeModule ( ucfirst ( $_GET ['module'] ) );
			}
			if (class_exists ( $moduleClass )) {
				
				$mod = new $moduleClass ();
				if (method_exists ( $mod, '_init' )) {
					$mod->_init ();
				}
				if (method_exists ( $mod, 'getView' )) {
					$mod->getView ();
				}
				self::$Collector [$moduleClass] = $mod;
			} else {
				header ( "HTTP/1.x 404 Not Found" );
				$mod = new not_found ();
				if (method_exists ( $mod, 'getView' )) {
					$mod->getView ();
				}
				
				self::$Collector [$moduleClass] = $mod;
			}
		} elseif (! isset ( $_GET ['module'] ) && $_SERVER ['REQUEST_URI'] != "/") {
			header ( "HTTP/1.x 404 Not Found" );
			$mod = new not_found ();
			
			if (method_exists ( $mod, 'getView' )) {
				$mod->getView ();
			}
			self::$Collector ['not_found'] = $mod;
		} else {
			$moduleClass = "Redactor_Module_" . ucfirst ( $this->module );
			if (! class_exists ( $moduleClass )) {
				$this->includeModule ( ucfirst ( $this->module ) );
			}
			if (class_exists ( $moduleClass )) {
				
				$mod = new $moduleClass ();
				if (method_exists ( $mod, '_init' )) {
					$mod->_init ();
				}
				if (method_exists ( $mod, 'getView' )) {
					$mod->getView ();
				}
				self::$Collector [$moduleClass] = $mod;
			} else {
				header ( "HTTP/1.x 404 Not Found" );
				$mod = new not_found ();
				
				if (method_exists ( $mod, 'getView' )) {
					$mod->getView ();
				}
				self::$Collector ['not_found'] = $mod;
			}
		}
		$this->saveHit ();
		
		if (isset ( $mod ) && is_object ( $mod )) {
			
			$this->content = $mod->over;
		}
		
		$this->show ();
	}
	// ������������� ��������� ������
	// TODO: �������� �������������� ������ ����� ��� 500 � �.�.
	function setError($errnum = 404) {
		switch ($errnum) {
			case 404 :
				header ( "HTTP/1.x 404 Not Found" );
				break;
		}
	}
	// ������������� �������� � Title �������� �� ���������
	function initMetaTags() {
		$rows = $this->getAdapter ()->query ( "select * from `site_setting`" );
		
		if ($rows->rowCount () > 0) {
			foreach ( $rows as $row ) {
				switch ($row->option) {
					case "title" :
						BreadcrumbsTitle::add ( $row->value );
						break;
					case "desc" :
						Metas::setDescription ( $row->value );
						break;
					case "keys" :
						Metas::setKeywords ( $row->value );
						break;
				}
			}
		}
	}
	
	// �������� �������� URL
	private function matchUrl() {
		$path = getenv ( 'REQUEST_URI' );
		$clearPath = parse_url ( $path, PHP_URL_PATH );
		if (preg_match ( "#//#", $clearPath )) {
			return false;
		}
		$params = array (
				'module' => false 
		);
		$route = false;
		if ($path == "/") {
			$_GET ['module'] = 'pages';
			$_GET ['isIndex'] = true;
			return true;
		}
		$path2 = parse_url ( $path, PHP_URL_PATH );
		
		if (($match = rewriteUrls::match ( $path )) != false) {
			$_GET = array_merge ( $_GET, $match );
			return false;
		}
		
		foreach ( self::$defaultUrls as $module => $urls ) {
			if ($route == true) {
				break;
			}
			foreach ( $urls as $url => $addparams ) {
				$params ['module'] = $module;
				$url2 = str_replace ( "/", "\/", $url );
				if (preg_match ( "/$url2/", $path, $p )) {
					
					unset ( $p [0] );
					foreach ( array_keys ( $p ) as $key ) {
						if (! is_string ( $key )) {
							unset ( $p [$key] );
						}
					}
					$totalParams = count ( $p );
					
					foreach ( $p as $pNum => $value ) {
						if (empty ( $value )) {
							unset ( $p [$pNum] );
						}
					}
					
					if ($totalParams != count ( $p )) {
						$params ['module'] = false;
						break;
					}
					$params = array_merge ( $params, $p );
					$params = array_merge ( $params, $addparams );
					foreach ( array_keys ( $params ) as $key ) {
						
						if (! is_string ( $key )) {
							
							unset ( $params [$key] );
						}
					}
					
					$route = true;
					break;
				} else {
					$params ['module'] = false;
				}
			}
		}
		if ($params ['module'] != false) {
			$_GET = array_merge ( $_GET, $params );
			return true;
		}
		
		$_GET = array_merge ( $_GET, $params );
		
		return true;
	}
	// ����� �������
	private function show() {
		$file = 'template/' . self::getTemplate () . '/' . $this->getFileTemplate () . '.phtml';
		
		if (file_exists ( $file )) {
			include ($file);
		} else {
			echo '������! ���� ������� �� ������';
		}
	}
	
	// ������ ������ �� ���������� ��� �������� ��������� ��� ������ �������
	function pagginator($config = array('rows'=>0, 'limit'=>0, 'pageParam'=>'page')) {
		if (! is_array ( $config )) {
			return false;
		}
		if (! isset ( $config ['rows'] ) or ! isset ( $config ['limit'] )) {
			return false;
		}
		$config ['limit'] = ( int ) $config ['limit'];
		$config ['rows'] = ( int ) $config ['rows'];
		if ($config ['limit'] <= 0 or $config ['rows'] <= 0) {
			return false;
		}
		if (! isset ( $config ['pageParam'] )) {
			$config ['pageParam'] = 'page';
		}
		
		$pages = array (
				'prev' => false,
				'current' => 1,
				'next' => false,
				'pages' => array (),
				'lastpage' => 0 
		);
		$adjacents = 1;
		
		$page = isset ( $_GET [$config ['pageParam']] ) ? ( int ) $_GET [$config ['pageParam']] : 1;
		
		if ($page < 1) {
			$page = 1;
		}
		
		if ($page == 0) {
			$page = 1;
		}
		$add = '';
		$prev = $page - 1;
		$next = $page + 1;
		$lastpage = ceil ( $config ['rows'] / $config ['limit'] );
		$pages ['lastpage'] = $lastpage;
		$lpm1 = $lastpage - 1;
		
		if ($lastpage > 1) {
			
			if ($prev < 1) {
			} else {
				$pages ['prev'] = $prev;
			}
			
			$now = $lastpage - (2 + ($adjacents * 2));
			if (($page - 2) >= 1 or ($page - 2) == 1) {
				$now = $page - 2;
			} elseif (($page - 2) < 1) {
				$now = 1;
			} 

			elseif ($lastpage - (2 + ($adjacents * 2)) > $page) {
				$now = $page;
			}
			$end = $lastpage;
			if (($now + 4) < $lastpage) {
				$end = $now + 4;
			}
			$tot = $end - $now + 1;
			
			if ($tot < 4 && $lastpage >= 4) {
				$now = $now - 1;
				if ($now < 1) {
					$now = 1;
				}
				
				$end = $end + 2;
				if ($end > $lastpage) {
					$end = $lastpage;
				}
			}
			for($counter = $now; $counter <= $end; $counter ++) {
				if ($counter == $page) {
					$pages ['current'] = $counter;
					$pages ['pages'] [] = $counter;
				} else {
					$pages ['pages'] [] = $counter;
				}
			}
			// next button
			if ($page < $counter - 1) {
				$pages ['next'] = $next;
			}
		}
		return $pages;
	}
}
class Redactor_Helper extends Redactor_Action {
	function __toString() {
		if (isset ( $this->over )) {
			return $this->over;
		}
	}
}
class Redactor_Helper_Title extends Redactor_Helper {
	var $over = "";
	function __construct() {
		$this->over = BreadcrumbsTitle::getClear ( ' / ' );
	}
}
class Redactor_Helper_Desc extends Redactor_Helper {
	var $over = "";
	function __construct() {
		$this->over = htmlspecialchars ( Metas::$Desc );
	}
}
class Redactor_Helper_Keys extends Redactor_Helper {
	var $over = "";
	function __construct() {
		$this->over = htmlspecialchars ( Metas::$Keys );
	}
}
class StringPlural {
	/**
	 * ���������� ���������� ����
	 */
	const PLURAL_DEFAULT_LANG = 'ru';
	
	/**
	 * �������� ���������� ����� ����� ��� �����������(!) ����� � ������������ �
	 * ������ ������������ ����������.
	 * ���������� ���� ������������ ���������� PLURAL_DEFAULT_LANG (�� �������
	 * "ru") � ������ ������.
	 *
	 * @param integer $amount
	 *        	����� ������������ ���������� "���������" �����
	 * @param mixed $_        	
	 *
	 * @example self::Plural(1, array('����', '�����', '�������')); //����
	 * @example self::Plural(2, '����', '�����', '�������'); //�����
	 * @example self::Plural(5, '����', '�����', '�������'); //�������
	 *         
	 * @return string
	 */
	static function Plural($amount, $_) {
		$argv = func_get_args ();
		$arr = array ();
		
		if (is_array ( $_ )) {
			$arr = $_;
		} else {
			for($i = 1, $x = count ( $argv ); $i < $x; $i ++)
				$arr [] = $argv [$i];
		}
		
		return self::PluralLang ( self::PLURAL_DEFAULT_LANG, $amount, $arr );
	}
	
	/**
	 * �������� ���������� ����� ����� ��� ����������� ����� � ������������ �
	 * ������ ������������ ����������
	 *
	 * @param integer $amount
	 *        	����� ������������ ���������� "���������" �����
	 * @param mixed $_        	
	 *
	 * @example self::PluralEn(1, array('window', 'windows')); //window
	 * @example self::PluralEn(2, 'window', 'windows'); //windows
	 *         
	 * @return string
	 */
	static function PluralEn($amount, $_) {
		$argv = func_get_args ();
		$arr = array ();
		
		if (is_array ( $_ )) {
			$arr = $_;
		} else {
			for($i = 1, $x = count ( $argv ); $i < $x; $i ++)
				$arr [] = $argv [$i];
		}
		
		return self::PluralLang ( 'en', $amount, $arr );
	}
	
	/**
	 * �������� ���������� ����� ����� ��� ������� ����� � ������������ � ������
	 * ������������ ����������
	 *
	 * @param string $lang
	 *        	������������� ����� ��� �������� ����� ���������� ����� �����
	 * @param integer $amount
	 *        	����� ������������ ���������� "���������" �����
	 * @param mixed $_        	
	 *
	 * @example self::PluralLang('en', 1, array('window', 'windows')); //window
	 * @example self::PluralLang('en', 2, 'window', 'windows'); //windows
	 *         
	 * @return string
	 */
	static function PluralLang($lang, $amount, $_) {
		$argv = func_get_args ();
		
		if (count ( $argv ) < 3) {
			trigger_error ( __METHOD__ . ': missing required arguments', E_USER_WARNING );
			return null;
		}
		
		$amount = ( int ) $amount;
		
		$form = self::PluralLangGetForm ( $lang, $amount );
		if (is_array ( $_ )) {
			if (array_key_exists ( $form, $_ )) {
				return $_ [$form];
			} elseif (count ( $_ > 0 )) {
				return $_ [0];
			} else {
				trigger_error ( __METHOD__ . ': missing required arguments', E_USER_WARNING );
				return null;
			}
		} else {
			if (array_key_exists ( ($form + 2), $argv )) {
				return $argv [$form + 2];
			} else {
				return $argv [2];
			}
		}
	}
	
	/**
	 * �������� ���������� ��������� �������������� ����� ��� ������� �����
	 *
	 * @param string $lang
	 *        	������������� �����
	 * @return integer ���������� ���������
	 */
	static function PluralLangGetCount($lang) {
		switch ($lang) {
			case 'ach' :
			case 'af' :
			case 'ak' :
			case 'am' :
			case 'an' :
			case 'arn' :
			case 'ast' :
			case 'az' :
			case 'bg' :
			case 'bn' :
			case 'br' :
			case 'ca' :
			case 'da' :
			case 'de' :
			case 'el' :
			case 'en' :
			case 'eo' :
			case 'es' :
			case 'et' :
			case 'eu' :
			case 'fi' :
			case 'fil' :
			case 'fo' :
			case 'fr' :
			case 'fur' :
			case 'fy' :
			case 'gl' :
			case 'gu' :
			case 'ha' :
			case 'he' :
			case 'hi' :
			case 'hu' :
			case 'ia' :
			case 'is' :
			case 'it' :
			case 'jv' :
			case 'kn' :
			case 'ku' :
			case 'lb' :
			case 'ln' :
			case 'mai' :
			case 'mfe' :
			case 'mg' :
			case 'mi' :
			case 'mk' :
			case 'ml' :
			case 'mn' :
			case 'mr' :
			case 'nah' :
			case 'nap' :
			case 'nb' :
			case 'ne' :
			case 'nl' :
			case 'se' :
			case 'nn' :
			case 'no' :
			case 'nso' :
			case 'oc' :
			case 'or' :
			case 'ps' :
			case 'pa' :
			case 'pap' :
			case 'pms' :
			case 'pt' :
			case 'rm' :
			case 'sco' :
			case 'si' :
			case 'so' :
			case 'son' :
			case 'sq' :
			case 'sw' :
			case 'sv' :
			case 'ta' :
			case 'te' :
			case 'ti' :
			case 'tk' :
			case 'tr' :
			case 'ur' :
			case 'wa' :
			case 'yo' :
				return 2;
			case 'ar' :
				return 6;
			case 'ay' :
			case 'bo' :
			case 'cgg' :
			case 'dz' :
			case 'fa' :
			case 'hy' :
			case 'id' :
			case 'ja' :
			case 'jbo' :
			case 'ka' :
			case 'kk' :
			case 'km' :
			case 'ko' :
			case 'ky' :
			case 'lo' :
			case 'ms' :
			case 'sah' :
			case 'su' :
			case 'tg' :
			case 'th' :
			case 'tt' :
			case 'ug' :
			case 'uz' :
			case 'vi' :
			case 'wo' :
			case 'zh' :
				return 1;
			case 'be' :
			case 'bs' :
			case 'cs' :
			case 'hr' :
			case 'lt' :
			case 'lv' :
			case 'mnk' :
			case 'pl' :
			case 'ro' :
			case 'ru' :
			case 'sk' :
			case 'sr' :
			case 'uk' :
				return 3;
			case 'cy' :
			case 'gd' :
			case 'kw' :
			case 'mt' :
			case 'sl' :
				return 4;
			case 'ga' :
				return 5;
			default :
				return 1;
		}
	}
	
	/**
	 * �������� ������������� ����� �������������� �����
	 *
	 * @param string $lang
	 *        	������������� �����
	 * @param integer $n
	 *        	����� �� ������� ������������ ����� �����
	 * @return integer ������������� ����� �����
	 */
	private static function PluralLangGetForm($lang, $n) {
		switch ($lang) {
			case 'ach' :
			case 'ak' :
			case 'am' :
			case 'arn' :
			case 'br' :
			case 'fil' :
			case 'fr' :
			case 'ln' :
			case 'mfe' :
			case 'mg' :
			case 'mi' :
			case 'oc' :
			case 'ti' :
			case 'tr' :
			case 'wa' :
				return ( int ) ($n > 1);
			case 'af' :
			case 'an' :
			case 'ast' :
			case 'az' :
			case 'bg' :
			case 'bn' :
			case 'ca' :
			case 'da' :
			case 'de' :
			case 'el' :
			case 'en' :
			case 'eo' :
			case 'es' :
			case 'et' :
			case 'eu' :
			case 'fi' :
			case 'fo' :
			case 'fur' :
			case 'fy' :
			case 'gl' :
			case 'gu' :
			case 'ha' :
			case 'he' :
			case 'hi' :
			case 'hu' :
			case 'ia' :
			case 'it' :
			case 'kn' :
			case 'ku' :
			case 'lb' :
			case 'mai' :
			case 'ml' :
			case 'mn' :
			case 'mr' :
			case 'nah' :
			case 'nap' :
			case 'nb' :
			case 'ne' :
			case 'nl' :
			case 'se' :
			case 'nn' :
			case 'no' :
			case 'nso' :
			case 'or' :
			case 'ps' :
			case 'pa' :
			case 'pap' :
			case 'pms' :
			case 'pt' :
			case 'rm' :
			case 'sco' :
			case 'si' :
			case 'so' :
			case 'son' :
			case 'sq' :
			case 'sw' :
			case 'sv' :
			case 'ta' :
			case 'te' :
			case 'tk' :
			case 'ur' :
			case 'yo' :
				return ( int ) ($n != 1);
			case 'jv' :
				return ( int ) ($n != 0);
			case 'ay' :
			case 'bo' :
			case 'cgg' :
			case 'dz' :
			case 'fa' :
			case 'hy' :
			case 'id' :
			case 'ja' :
			case 'jbo' :
			case 'ka' :
			case 'kk' :
			case 'km' :
			case 'ko' :
			case 'ky' :
			case 'lo' :
			case 'ms' :
			case 'sah' :
			case 'su' :
			case 'tg' :
			case 'th' :
			case 'tt' :
			case 'ug' :
			case 'uz' :
			case 'vi' :
			case 'wo' :
			case 'zh' :
				return ( int ) (0);
			
			case 'be' :
			case 'bs' :
			case 'hr' :
			case 'ru' :
			case 'sr' :
			case 'uk' :
				return ( int ) ($n % 10 == 1 && $n % 100 != 11 ? 0 : ($n % 10 >= 2 && $n % 10 <= 4 && ($n % 100 < 10 || $n % 100 >= 20) ? 1 : 2));
			case 'lt' :
				return ( int ) ($n % 10 == 1 && $n % 100 != 11 ? 0 : ($n % 10 >= 2 && ($n % 100 < 10 or $n % 100 >= 20) ? 1 : 2));
			case 'lv' :
				return ( int ) ($n % 10 == 1 && $n % 100 != 11 ? 0 : ($n != 0 ? 1 : 2));
			case 'mt' :
				return ( int ) ($n == 1 ? 0 : ($n == 0 || ($n % 100 > 1 && $n % 100 < 11) ? 1 : (($n % 100 > 10 && $n % 100 < 20) ? 2 : 3)));
			case 'pl' :
				return ( int ) ($n == 1 ? 0 : ($n % 10 >= 2 && $n % 10 <= 4 && ($n % 100 < 10 || $n % 100 >= 20) ? 1 : 2));
			case 'ar' :
				return ( int ) ($n == 0 ? 0 : ($n == 1 ? 1 : ($n == 2 ? 2 : ($n % 100 >= 3 && $n % 100 <= 10 ? 3 : ($n % 100 >= 11 ? 4 : 5)))));
			case 'gd' :
				return ( int ) (($n == 1 || $n == 11) ? 0 : (($n == 2 || $n == 12) ? 1 : (($n > 2 && $n < 20) ? 2 : 3)));
			case 'is' :
				return ( int ) ($n % 10 != 1 || $n % 100 == 11);
			
			case 'cs' :
			case 'sk' :
				return ( int ) (($n == 1) ? 0 : (($n >= 2 && $n <= 4) ? 1 : 2));
			case 'cy' :
				return ( int ) (($n == 1) ? 0 : (($n == 2) ? 1 : (($n != 8 && $n != 11) ? 2 : 3)));
			case 'ga' :
				return ( int ) (($n == 1) ? 0 : (($n == 2) ? 1 : ($n < 7 ? 2 : ($n < 11 ? 3 : 4))));
			case 'kw' :
				return ( int ) (($n == 1) ? 0 : (($n == 2) ? 1 : (($n == 3) ? 2 : 3)));
			
			case 'mk' :
				return ( int ) (($n == 1 || $n % 10 == 1) ? 0 : 1);
			case 'mnk' :
				return ( int ) ($n == 0 ? 0 : ($n == 1 ? 1 : 2));
			
			case 'ro' :
				return ( int ) ($n == 1 ? 0 : (($n == 0 || ($n % 100 > 0 && $n % 100 < 20)) ? 1 : 2));
			case 'sl' :
				return ( int ) ($n % 100 == 1 ? 1 : ($n % 100 == 2 ? 2 : ($n % 100 == 3 || $n % 100 == 4 ? 3 : 0)));
			default :
				return 0;
		}
	}
}
