<?php
class Breadcrumbs {
	protected static $lastLink = false;
	protected static $path = array ();
	protected static $closer = '';
	static function add($key) {
		self::$path [] = $key;
	}
	static function set($key) {
		self::$path = array (
				$key
		);
	}
	static function lastLink($bool) {
		self::$lastLink = $bool;
	}
	static function forward() {
		krsort ( self::$path );
	}
	function __call($name, $args) {
		echo $name;
	}
	static function getPath() {
		$path = self::$path;
		if (self::$lastLink == false) {
			if (count ( $path ) == 1) {
				$path [0] = trim ( strip_tags ( $path [count ( $path ) - 1] ) );
			} elseif (count ( $path ) > 1) {
				$path [count ( $path ) - 1] = trim ( strip_tags ( $path [count ( $path ) - 1] ) );
			}
		}
		return $path;
	}
	static function closer($tag = "li", $optionTag = "") {
		$path = self::$path;
		if (self::$lastLink == false) {
			if (count ( $path ) == 1) {
				$path [0] = trim ( strip_tags ( $path [count ( $path ) - 1] ) );
			} elseif (count ( $path ) > 1) {
				$path [count ( $path ) - 1] = trim ( strip_tags ( $path [count ( $path ) - 1] ) );
			}
		}
		$out = '';
		foreach ( $path as $url ) {
			$out .= "{$url}";
		}
		return $out;
	}
	static function get($sep = " / ") {
		$path = self::$path;
		if (self::$lastLink == false) {
			$path [count ( $path ) - 1] = trim ( strip_tags ( $path [count ( $path ) - 1] ) );
		}

		return implode ( $sep, $path );
	}
	static function getClear($sep = " / ") {
		$new = array ();
		foreach ( self::$path as $key ) {
			$key = trim ( strip_tags ( $key ) );
		}
		return implode ( $sep, $new );
	}
}
class BreadcrumbsTitle {
	protected static $path = array ();
	static function add($key) {
		self::$path [] = $key;
	}
	static function set($path) {
		self::$path = array (
				$path
		);
	}
	static function forward() {
		krsort ( self::$path );
	}
	static function getPath() {
		return self::$path;
	}
	static function getClear($sep = " / ") {
		$new = array ();
		foreach ( self::$path as $key ) {
			$key = trim ( strip_tags ( $key ) );
			if (! empty ( $key )) {

				$new [] = $key;
			}
		}
		return implode ( $sep, $new );
	}
}
?>