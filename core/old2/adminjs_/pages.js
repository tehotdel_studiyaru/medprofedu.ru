function parentAux(param) {
	var aux_loader_tree = new Ext.tree.TreeLoader({
		url : 'admincp.php',
		baseParams : {
			xaction : 'Load_Tree_Pages',
			module : 'pages'
		},
		preloadChildren : true
	});
	var Tree4CCOS = new Ext.tree.TreePanel({
		autoScroll : true,
		animate : true,
		enableDD : false,
		width : 500,
		floatable : false,
		margins : '5 0 0 0',
		cmargins : '5 5 0 0',
		split : true,
		expanded : true,
		containerScroll : true,
		lines : false,
		singleExpand : true,
		useArrows : true,

		loader : aux_loader_tree,

		root : {
			nodeType : 'async',
			text : '�������� ������',
			expanded : true,
			draggable : false,
			id : '0'
		}
	});

	var ChangeCatOfShopItem = new Ext.Window({
		layout : 'fit',
		id : 'ChangeParentOfAux',
		title : '�������� ������',
		shim : false,
		modal : true,
		width : 500,
		height : 250,
		autoScroll : true,
		closeAction : 'close',
		plain : true,
		items : Tree4CCOS,
		buttons : [ {
			text : '�������',

			handler : function() {
				var tr = Tree4CCOS.getSelectionModel().getSelectedNode();
				var id = tr.id;
				var name = tr.text;

				Ext.getCmp('AddPages').getForm().findField('parentName')
						.setValue(name);
				Ext.getCmp('AddPages').getForm().findField('parentId')
						.setValue(id);
				Ext.getCmp('ChangeParentOfAux').close();
			}
		} ]
	}).show();
}

function q_add_pages(btn) {
	if (btn == "yes") {
		Ext.getCmp('AddPages').getForm().reset();
		if (Ext.getCmp('pages').layout.activeItem.id != 'PagesIndex')

		{
			Ext.getCmp('pages').layout.setActiveItem('PagesIndex');
			if (PagesStore.data.length < 1) {
				PagesStore.load({
					method : 'post',
					params : {
						start : 0,
						limit : 25
					}
				});
			}
			;
		}
		;
	}
	;
};

function save_pages(btn) {
	var form = Ext.getCmp('AddPages').getForm();
	var id = form.findField('id').getValue();
	var title = form.findField('title').getValue();
	tinyMCE.triggerSave();
	if (form.isValid() & form.findField('text').getValue() != "") {
		form.submit({
			url : 'admincp.php',
			method : 'post',
			params : {
				task : 'update',
				module : 'pages'
			},
			success : function() {

				form.reset();
				PagesStore.reload();
				Ext.getCmp('pages').layout.setActiveItem('PagesIndex');
				App.setAlert('', '�������� ������� ���������');
			},
			failure : function() {
				PagesStore.reload();
				App.setAlert('', '�� ������� �������� ��������');
			}
		});
		/*
		 * if (id == "") { var u = new
		 * PagesGrid.store.recordType(form.getValues());
		 * PagesGrid.store.insert(0,u); PagesGrid.store.reload();
		 * Ext.getCmp('pages').layout.setActiveItem('PagesIndex'); form.reset();
		 * PagesStore.reload(); App.setAlert('', '�������� ������� ���������'); }
		 * else { var text = form.findField('text').getValue(); text =
		 * text.replace("\n", ""); form.findField('text').setValue(text); var
		 * rec = PagesGrid.getSelectionModel().getSelected();
		 * form.updateRecord(rec); form.reset(); PagesStore.reload();
		 * Ext.getCmp('pages').layout.setActiveItem('PagesIndex');
		 * App.setAlert('', '�������� ������� ���������'); };
		 */
	} else {
		var textmsg = "";
		if (form.findField('title').getValue() == "") {
			textmsg += "�� ��������� ���� '<b>���������</b>'<br>";
		}
		;
		if (form.findField('text').getValue() == "") {
			textmsg += "�� ��������� ���� '<b>����� ��������</b>'<br>";
		}
		;

		Ext.MessageBox
				.show({
					msg : '<center><b>�� ��������� ������������ ����:</b></center><br><br>'
							+ textmsg,
					width : 300,
					buttons : Ext.MessageBox.OK,
					animEl : btn
				});
	}
	;
}
var PagesFormBar = function() {
	return [
			{
				text : '<b>���������</b>',
				iconCls : 'back',
				handler : function() {
					var form = Ext.getCmp('AddPages').getForm();
					if (form.findField('title').getValue() != ""
							|| form.findField('text').getValue() != "") {
						Ext.MessageBox.confirm('',
								'�� ������� ��� ������ ����� ��� ����������?',
								q_add_pages);
					} else {
						Ext.getCmp('AddPages').getForm().reset();
						if (Ext.getCmp('pages').layout.activeItem.id != 'PagesIndex') {
							Ext.getCmp('pages').layout
									.setActiveItem('PagesIndex');
							if (PagesStore.data.length < 1) {
								PagesStore.load({
									method : 'post',
									params : {
										start : 0,
										limit : 25
									}
								});
							}
							;
						}
						;

					}

				}
			}, '-', {
				text : '<b>���������</b>',
				iconCls : 'apply',
				id : Ext.id(),
				handler : function() {
					save_pages(this.id);
				}
			} ];
};

var PagesWriter = new Ext.data.JsonWriter({
	listful : true
});
var PagesStore = new Ext.data.Store({
	proxy : new Ext.data.HttpProxy({
		url : 'admincp.php'
	}),
	baseParams : {
		module : 'pages',
		task : 'read'
	},
	remoteSort : true,
	reader : new Ext.data.JsonReader({
		root : 'results',
		totalProperty : 'total',
		id : 'id'
	}, [ {
		name : 'id',
		mapping : 'id'
	}, {
		name : 'title',
		mapping : 'title'
	}, {
		name : 'url',
		mapping : 'url'
	}, {
		name : 'title2',
		mapping : 'title2'
	}, {
		name : 'parentId',
		mapping : 'parentId'
	}, {
		name : 'parentName',
		mapping : 'parentName'
	}, {
		name : 'text',
		mapping : 'text'
	}, {
		name : 'textblock',
		mapping : 'textblock'
	}, {
		name : 'keys',
		mapping : 'keys'
	}, {
		name : 'desc',
		mapping : 'desc'
	}, {
		name : 'tags',
		mapping : 'tags'
	}, {
		name : 'url',
		mapping : 'url'
	}, {
		name : 'copy',
		mapping : 'copy'
	}, {
		name : 'link',
		mapping : 'link'
	}, {
		name : 'UpdatedDate'
	}, {
		name : 'CreatedDate'
	}, {
		name : 'UpdatedDate2'
	}, {
		name : 'CreatedDate2'
	}, {
		name : 'pos',
		mapping : 'pos'
	}, {
		name : 'h1',
		mapping : 'h1'
	}, {
		name : 'gallery',
		mapping : 'gallery'
	}, {
		name : 'gallery_name',
		mapping : 'gallery_name'
	}, {
		name : 'secondTitle',
		mapping : 'secondTitle'
	}, {
		name : 'index',
		mapping : 'index'
	}, {
		name : 'title_page',
		mapping : 'title_page'
	}, {
		name : 'inMenu',
		mapping : 'inMenu'
	}, {
		name : 'isService',
		mapping : 'isService'
	}, {
		name : 'active',
		type : 'int',
		mapping : 'active'
	} ])
});
PagesStore.on({
	beforeload : function() {
		Ext.MessageBox.wait('���������� ���������...', '��������');
	},
	load : function() {
		Ext.MessageBox.updateProgress(1);
		Ext.MessageBox.hide();
	},
	loadexception : function() {
		Ext.MessageBox.updateProgress(1);
		Ext.MessageBox.hide();
	}
});
var PagesPagingBar = new Ext.PagingToolbar({
	pageSize : 25,
	store : PagesStore,
	displayInfo : true
});
var PagesRowActions = new Ext.ux.grid.RowActions({
	actions : [ {
		iconCls : 'house',
		qtip : '������� �������� ���������'
	}, '-',
	
	{
		iconCls : 'delete',
		qtip : '�������'
	}, {
		iconCls : 'copy',
		qtip : '����������'
	}, {
		iconCls : 'edit',
		qtip : '�������������'

	} ],
	widthIntercept : Ext.isSafari ? 4 : 2,
	header : '<center><b>��������</b></center>',
	id : 'actions'
});
PagesRowActions.on({
	action : function(grid, record, action, row, col) {
		switch (action) {
		case "delete":
			Ext.MessageBox.confirm('�������������',
					'�� ������� ��� ������ ������� ��� ��������',
					function(btn) {
						if (btn == 'yes') {
							var rec = record;
							Ext.Ajax.request({
								url : 'admincp.php',
								method : 'post',
								params : {
									id : rec.id,
									module : 'pages',
									task : 'destroy'
								}
							});
							PagesGrid.store.remove(rec);
						}
						;
					});
			break;
		case 'house':
			Ext.MessageBox.confirm('�������������',
					'�� ������� ��� ������ ������� �������� ��� ��������',
					function(btn) {
						if (btn == 'yes') {
							var rec = record;
							Ext.Ajax.request({
								url : 'admincp.php',
								params : {
									xaction : 'homepage',
									module : 'pages',
									id : rec.id
								},
								method : 'post',
								success : function() {
									App.setAlert('',
											'�������� ������� ���������');
									PagesStore.reload();
								}
							});

						}
						;
					});
			break;
		case "copy":
			Ext.Ajax.request({
				url:'/admincp.php',
				params:{module:'pages', task:'Copy', id:record.data.id},
				success:function(o){
					PagesStore.load({method: 'post', params:{start:0, limit:25}});
					Ext.getCmp('pages').layout.setActiveItem('AddPages');
				    Ext.getCmp('AddPages').load({
				    	url:'/admincp.php',
				    	method:'post',
				    	waitMsg:'��������� ��� �������� ������',
				    	params:{module:'pages',task:'loadRecord', id:o.responseText}
				    });
				}
			});
		break;
		case "edit":
			Ext.getCmp('pages').layout.setActiveItem('AddPages');
			Ext.getCmp('AddPages').getForm().loadRecord(record);
			break;
		case "apply":
			addAdv(record.data.id);
			break;

		}
		;
	}
});

function EditSettingModule_Page() {

	var form = new Ext.FormPanel({
		autoScroll : true,
		frame : true,
		labelAlign : 'top',
		items : [ {
			layout : 'table',
			layoutConfig : {
				columns : 2
			},
			items : [ {
				width : 450,
				layout : 'form',
				items : [ {
					xtype : 'textfield',
					fieldLabel : 'URL (���)',
					name : 'Pages_URL',

					anchor : '90%'
				} ]
			}, {
				layout : 'form',
				width : 400,
				items : [ {
					xtype : 'radiogroup',
					fieldLabel : '��� ���',
					name : 'Pages_TypeURL',
					items : [ {
						boxLabel : '����������� (�� ID)',
						name : 'Pages_TypeURL',
						value : '1',
						inputValue : '1'
					}, {
						boxLabel : '�������������� (��������)',
						name : 'Pages_TypeURL',
						value : '2',
						inputValue : '2'
					} ]
				} ]
			} ]
		}, {
			xtype : 'textfield',
			fieldLabel : 'H1',
			name : 'Pages_H1',

			anchor : '95%'
		}, {
			xtype : 'textfield',
			fieldLabel : 'Title',
			name : 'Pages_TitlePage',

			anchor : '95%'
		}, {
			xtype : 'textarea',
			fieldLabel : 'Description',
			name : 'Pages_DescPage',

			anchor : '95%'
		}, {
			xtype : 'textarea',
			fieldLabel : 'Keywords',
			name : 'Pages_KeysPage',

			anchor : '95%'
		}, htmled({
			name : 'Pages_TopDesc',
			label : '�������� ������',
			height : 150
		}), htmled({
			name : 'Pages_BottomDesc',
			label : '�������� �����',
			height : 150
		}), {
			xtype : 'textarea',
			fieldLabel : '���� ��� ����������� ������������',
			name : 'Pages_Tags',

			anchor : '95%'
		}, {
			xtype : 'textarea',
			fieldLabel : '�������������� ����',
			name : 'Pages_s2',

			anchor : '95%'
		}, {
			xtype : 'textarea',
			fieldLabel : '�������������� ����',
			name : 'Pages_s3',

			anchor : '95%'
		} ]
	});
	var win = new Ext.Window(
			{
				shim : true,
				frame : true,

				tools : [ {
					id : 'help',
					qtip : '�������',
					handler : function() {
						new Ext.Window({
							width : 850,
							height : 540,
							frame : true,
							title : '�������',
							closeAction : 'close',
							autoLoad : {
								url : 'admincp.php?help=pages'
							}
						}).show();
					}
				} ],
				height : 550,
				width : 900,
				layout : 'fit',
				iconCls : 'setting',
				title : '��������� ������',
				buttons : [ {
					text : '���������',
					handler : function() {
						Ext.ux.TinyMCE.initTinyMCE();
						tinyMCE.triggerSave();
						form
								.getForm()
								.submit(
										{
											url : 'admincp.php',
											params : {
												task : 'SaveSetting',
												module : 'pages'
											},
											success : function() {
												win.close();
												Ext.MessageBox
														.alert('',
																'��������� ������� ��������');
											},
											failure : function() {
												Ext.MessageBox
														.alert('',
																'�� ����� ���������� ��������� ������, ���������� ���� �������');
											}
										});
					}
				} ],
				listeners : {
					"show" : function() {
						form.load({
							url : 'admincp.php',
							method : 'post',
							waitMsg : '��������� ��� �������� ������',
							params : {
								module : 'pages',
								task : 'LoadSetting'
							}
						});
					}
				},
				items : form
			}).show();
}

var PagesGrid = new Ext.grid.EditorGridPanel(
		{
			store : PagesStore,
			frame : true,
			bbar : PagesPagingBar,
			id : 'PagesGrid',
			region : 'center',
			enableColLock : false,
			plugins : [ PagesRowActions ],
			tbar : [ {
				text : '������� ����� ��������',
				iconCls : 'add',
				handler : function() {
					Ext.getCmp('pages').layout.setActiveItem('AddPages');

				}
			}, '->', {
				xtype : 'buttongroup',
				items : [ {
					text : '<b>��������� ������</b>',
					iconCls : 'setting',
					handler : EditSettingModule_Page
				} ]
			} ],
			clicksToEdit : 1,
			columns : [
					{
						id : 'id',
						header : "<b>#</b>",
						width : 40,
						sortable : true,
						dataIndex : 'id',
						renderer : function(v, tag, rec) {
							var style = "";
							if (rec.data.active == 2) {
								style = "color:#999999";
							}
							return '<p align="left" style="padding-right:16px;'
									+ style + '">' + v + '</p>';
						}
					},
					{
						id : 'module',
						header : "<center><b>���.</b></center>",
						width : 50,
						sortable : true,
						dataIndex : 'pos',
						renderer : function(v, tag, rec) {
							var style = "";
							if (rec.data.active == 2) {
								style = "color:#999999";
							}
							return '<span style="' + style + '">' + v
									+ '</span>';
						},
						editor : new Ext.form.NumberField()
					},

					{
						id : 'title',
						header : "<center><b>���������</b></center>",
						renderer : function(v, tag, rec) {
							var style = "";
							if (rec.data.active == 2) {
								style = "color:#999999";
							}
							return '<span style="' + style + '">' + v
									+ '</span>';
						},
						width : 200,
						sortable : true,
						dataIndex : 'title2'
					},
					{
						id : 'link',
						header : "<center><b>������</b></center>",
						renderer : function(v, tag, rec) {
							var style = "";
							if (rec.data.active == 2) {
								style = "color:#999999";
							}
							return '<span style="' + style + '">' + v
									+ '</span>';
						},
						width : 200,
						sortable : true,
						dataIndex : 'link'
					},

					{
						id : 'index',
						header : "<center><b>�������</b></center>",
						renderer : function(v, tag, rec) {
							var style = "";
							if (rec.data.active == 2) {
								style = "color:#999999";
							}
							return '<span style="' + style + '">' + v
									+ '</span>';
						},
						width : 80,
						sortable : true,
						dataIndex : 'index'
					},
					{
						header : "<center><b>��������</b></center>",
						renderer : function(v, tag, rec) {
							var style = "";
							if (rec.data.active == 2) {
								style = "color:#999999";
							}
							return '<span style="' + style + '">' + v
									+ '</span>';
						},
						width : 80,
						sortable : true,
						dataIndex : 'CreatedDate'
					},
					{
						header : "<center><b>����������</b></center>",
						renderer : function(v, tag, rec) {
							var style = "";
							if (rec.data.active == 2) {
								style = "color:#999999";
							}
							return '<span style="' + style + '">' + v
									+ '</span>';
						},
						width : 80,
						sortable : true,
						dataIndex : 'UpdatedDate'
					},
					{
						id : 'Active',
						header : "<center><b></b></center>",
						sortable : true,
						dataIndex : 'active',
						width : 50,
						renderer : function(v, tag, rec) {
							var style = "";
							if (rec.data.active == 2) {
								style = "color:#999999";
							}
							return '<div style="height:16px;"><form action="'+rec.get('link')+'" id="pagesLink'+rec.get('id')+'" target="_blank"></form><img style="cursor:pointer;" ext:qtip="����������" onclick="document.getElementById(\'pagesLink'+rec.get('id')+'\').submit();" src="/core/images/icons/Internet.png" /></a> <span style="'
									+ style
									+ '"><img  onclick="PagesChangeStatus('
									+ rec.data.id
									+ ', '
									+ (rec.data.active == 2 ? 1 : 2)
									+ ');" style="cursor:pointer;" ext:qtip="'
									+ (rec.data.active == 2 ? '������������'
											: '��������������')
									+ '" src="/core/images/icons/eye'
									+ (rec.data.active == 2 ? '2' : '')
									+ '.png" /></span></div>';
						}
					}, PagesRowActions ],
			listeners : {
				'afteredit' : function(oGrid_event) {
					Ext.Ajax
							.request({
								waitMsg : '���������� ���������...',
								url : 'admincp.php',
								params : {
									xaction : "update",
									id : oGrid_event.record.data.id,
									inMenu : oGrid_event.record.data.inMenu,
									isService : oGrid_event.record.data.isService,
									// active : oGrid_event.record.data.active,
									module : 'pages',
									pos : oGrid_event.record.data.pos
								},
								success : function(response) {

									PagesStore.commitChanges();

								},
								failure : function(response) {
									var result = response.responseText;
									Ext.MessageBox
											.alert('error',
													'could not connect to the database. retry later');
								}
							});
				}
			},
			sm : new Ext.grid.RowSelectionModel({
				singleSelect : true
			}),
			viewConfig : {
				forceFit : false,
				border : false
			}
		});

var PagesChangeStatus = function(Id, currentStatus) {
	Ext.Ajax.request({
		waitMsg : '���������� ���������...',
		url : 'admincp.php',
		params : {
			xaction : "update",
			id : Id,
			active : currentStatus,
			module : 'pages'
		},
		success : function(response) {
			PagesStore.getById(Id).set('active', currentStatus)
			PagesStore.commitChanges(); // changes successful, get
			
		},
		failure : function(response) {
			var result = response.responseText;
			Ext.MessageBox.alert('error',
					'could not connect to the database. retry later');
		}
	});
}
var PagesIndex = {
	layout : 'fit',
	id : 'PagesIndex',
	border : false,
	items : [ PagesGrid ]
};


var Add_Edit_Pages = new Ext.FormPanel({
	id : 'AddPages',
	iconCls : 'add',
	border : false,
	labelAlign : 'top',
	bbar : PagesFormBar(),
	monitorValid : true,
	layout : 'fit',
	fileUpload : true,
	width : 850,
	autoWidth : false,
	frame : true,
	listeners : {
		"show" : function() {
			Ext.getCmp('TabPanelPagesAddForm').activate(0);
		}
	},
	items : [ {
		xtype : 'tabpanel',
		activeTab : 0,

		id : 'TabPanelPagesAddForm',
		border : false,
		width : 850,
		autoWidth : false,
		frame : true,
		defaults : {
			autoHeight : false,
			autoWidth : false,
			width : 850,
			height : 700,
			bodyStyle : 'padding:10px;width:850px;',
			autoScroll : true,
			frame : true,
			layout : 'form',
			border : false
		},
		items : [ {
			title : '��������',

			items : [ {
				xtype : 'hidden',
				name : 'id',
				hidden : true
			}, {
				xtype : 'hidden',
				name : 'gallery',
				hidden : true
			}, {
				xtype : 'hidden',
				name : 'parentId',
				hidden : true
			}, {
				xtype : 'textfield',
				fieldLabel : '���������',
				name : 'title',
				
				dataIndex : 'title',
				allowBlank : false,
				width : 850
			}, {
				xtype : 'trigger',
				fieldLabel : '�������� ������',
				triggerClass : 'x-form-search-trigger',
				onTriggerClick : parentAux,

				editable : false,
				allowBlank : true,
				width : 150,
				name : 'parentName'

			}, htmled({
				name : 'text',
				label : '����� ��������',
				width : 850,
				height : 300
			}) ]
		}, {
			title : '��������� SEO',
			layout : 'form',
			items : [ {
				xtype : 'textfield',
				fieldLabel : 'URL �������� (���)',
				name : 'url',
				dataIndex : 'url',
				width : 850
			}, {
				xtype : 'textfield',
				fieldLabel : 'H1',
				name : 'h1',
				dataIndex : 'h1',
				width : 850
			}, {
				xtype : 'textfield',
				fieldLabel : 'Title',
				name : 'title_page',
				dataIndex : 'title_page',
				width : 850
			}, {
				xtype : 'textarea',
				fieldLabel : 'Description',
				name : 'desc',
				dataIndex : 'desc',
				width : 850
			}, {
				xtype : 'textarea',
				fieldLabel : 'Keywords',
				name : 'keys',
				dataIndex : 'keys',
				width : 850
			}, {
				xtype : 'textfield',
				fieldLabel : '���� ��� ����������� ������������',
				name : 'tags',
				dataIndex : 'tags',
				width : 850
			},{
				xtype:'textfield',
				name:'CreatedDate2',
				disabled : true,
				width:140,
				fieldLabel:'���� ��������'
			},{
				xtype:'textfield',
				name:'UpdatedDate2',
				disabled : true,
				width:140,
				fieldLabel:'���� ����������'
			} ]
		} ]
	} ]
});
var module_pages = {
	id : 'pages',
	layout : 'card',
	activeItem : 0,
	iconCls : 'pages',
	listeners : {
		'render' : function() {
			if (PagesStore.data.length < 1) {
				PagesStore.load({
					method : 'post',
					params : {
						start : 0,
						limit : 25
					}
				});
				$('a#pages').addClass('active');
			}
			;

		}
	},
	title : '��������',
	items : [ PagesIndex, Add_Edit_Pages ]
};
init_modules[init_modules.length] = module_pages;
init_nav_modules[init_nav_modules.length] = {
	text : '��������',
	iconCls : 'pages',
	handler : function() {
		Ext.getCmp('Content').layout.setActiveItem('pages');
		if (Ext.getCmp('pages').layout.activeItem.id == 'AddPages') {
			if (Ext.getCmp('AddPages')) {
				Ext.getCmp('AddPages').getForm().reset();
			}
			;
			Ext.getCmp('pages').layout.setActiveItem('PagesGrid');
			if (PagesStore.data.length < 1) {
				PagesStore.load({
					method : 'post',
					params : {
						start : 0,
						limit : 25
					}
				});
			}
			;
		} else {
			if (PagesStore.data.length < 1) {
				PagesStore.load({
					method : 'post',
					params : {
						start : 0,
						limit : 25
					}
				});
			}
			;
		}
		;
	},
	menu : [ {
		text : '������� ����� ��������',
		iconCls : 'add',
		handler : function() {
			if (Ext.getCmp('Content').layout.activeItem.id != 'pages') {
				Ext.getCmp('Content').layout.setActiveItem('pages');
				Ext.getCmp('pages').layout.setActiveItem('AddPages');
			} else {
				if (Ext.getCmp('pages').layout.activeItem.id != 'AddPages') {
					Ext.getCmp('pages').layout.setActiveItem('AddPages');
				}
				;
			}
			;
		}
	} ]

};

Ext.apply(actions, {
	'pages' : function() {

		if (Ext.getCmp('Content').layout.activeItem.id != 'pages') {
			Ext.getCmp('Content').layout.setActiveItem('pages');
			if (PagesStore.data.length < 1) {
				PagesStore.load({
					method : 'post',
					params : {
						start : 0,
						limit : 25
					}
				});
			}
			;

		} else {
			if (Ext.getCmp('pages').layout.activeItem.id == 'AddPages') {
				if (Ext.getCmp('AddPages')) {
					Ext.getCmp('AddPages').getForm().reset();
					Ext.getCmp('TabPanelPagesAddForm').activate(0);
				}
				;
				Ext.getCmp('pages').layout.setActiveItem('PagesIndex');
				if (PagesStore.data.length < 1) {
					PagesStore.load({
						method : 'post',
						params : {
							start : 0,
							limit : 25
						}
					});
				}
				;
			}
		}
		;
	}
});
ModulesRightMenu += '<li><img src="core/images/icons/page.png"/><a id="pages" href="#">��������</a></li>';