if (!Brands){
	var Brands = {};
}



// ���� ������ ��� ������� � ��������
Brands.Store = new Ext.data.Store({

	proxy : new Ext.data.HttpProxy({
		url : 'admincp.php',
		method : 'POST'
	}),
	baseParams : {
		xaction : "LoadRecords",
		module : 'Brands'
	},
	remoteSort : true,
	reader : new Ext.data.JsonReader({
		root : 'results',
		totalProperty : 'total',
		id : 'Id'
	}, [ {
		name : 'Id'
	}, {
		name : 'Title'
	}, {
		name : 'Sort'
	}, {
		name : 'UpdatedDate'
	}, {
		name : 'CreatedDate'
	}, {
		name : 'Active'
	}, {
		name : 'Link'
	}, {
		name : 'CountImages'
	}

	])

});


Brands.UploadPhoto = function(newFile) {

	var items = new Array();
	if (newFile) {
		items.push({
			xtype : 'fileuploadfield',
			emptyText : '�������� ���� ��� ��������',
			fieldLabel : '����',
			name : 'photo-path',
			width : '500',
			anchor : '95%',
			allowBlank : false,
			buttonCfg : {
				text : ' ',
				iconCls : 'upload-icon'
			}
		});
	}
	
	items.push({
		xtype : 'textfield',
		name : 'Title',
		fieldLabel : '���������',
		anchor : '90%'
	});
	items.push({
		xtype : 'hidden',
		name : 'Id'
	});
	
	items.push(htmled({
		name : 'Description',
		label : '��������',
		height : 200
	}));
	var form = new Ext.FormPanel({
		fileUpload : true,
		labelAlign : 'top',
		frame : true,
		shim : true,
		id : 'Brands.UploadFileForm',

		items : [ items ]

	});
	return new Ext.Window(
			{

				layout : 'fit',
				shim : false,
				modal : true,
				title : '�������� ����������',
				id : 'Brands.UploadImageWindow',
				width : 860,
				height : 550,
				autoScroll : true,
				closeAction : 'close',
				plain : true,
				listeners : {
					'close' : function() {
						Brands.ImagesStore.reload();
					}
				},
				items : [ form ],
				buttons : [
						{
							text : '���������',
							handler : function() {
								if (form.getForm().isValid()) {
									tinyMCE.triggerSave();
									var idd = Ext.getCmp('Brands.EditRecord')
											.getForm().findField('Id')
											.getValue();

									form
											.getForm()
											.submit(
													{
														url : 'admincp.php',
														method : 'POST',
														params : {
															ItemID : idd,
															module : 'Brands',
															task : 'SaveFile'
														},
														waitTitle : '�������� ����������',
														waitMsg : '���������� ���������, ��� �������� ����������...',
														success : function(
																fotoupload, o) {
															Brands.ImagesStore
																	.reload();
															Ext
																	.getCmp(
																			'Brands.UploadImageWindow')
																	.close();

														},
														failure : function(
																fotoupload2, o) {
															Ext.MessageBox
																	.alert(
																			'������',
																			'�� ������� ��������� ����������');
														}
													});
								}
							}
						},
						{
							text : '�������',
							handler : function() {
								Brands.ImagesStore.reload();
								Ext.getCmp('Brands.UploadImageWindow').close();
							}
						} ]
			}).show();
}

Brands.ImagesStore = new Ext.data.Store({
	proxy : new Ext.data.HttpProxy({
		url : 'admincp.php',
		method : 'POST'
	}),
	baseParams : {
		xaction : "LoadImages",
		module : 'Brands'
	},

	reader : new Ext.data.JsonReader({
		root : 'results',
		totalProperty : 'total',
		id : 'Id'
	}, [ {
		name : 'Id'

	}, {
		name : 'image'
	}, {
		name : 'Sort'
	}, {
		name : 'MainImage'
	}, {
		name : 'Title'
	}, {
		name : 'ItemID'
	} ])

});

Brands.EditRecord = function(Item) {

	var pagingBar = new Ext.PagingToolbar({
		pageSize : 25,
		store : Brands.ImagesStore,
		paramNames : {
			start : 'start',
			limit : 'limit'
		},
		displayInfo : true

	});

	var RowAction = new Ext.ux.grid.RowActions({

		actions : [ {
			iconCls : 'apply',
			qtip : '������� ��������'
		}, '-', {
			iconCls : 'delete',
			qtip : '�������'
		}, {
			iconCls : 'edit',
			qtip : '�������������'
		} ],
		widthIntercept : Ext.isSafari ? 4 : 2,
		id : 'actions'
	});
	RowAction.on({
		action : function(grid, record, action, row, col) {
			if (action == 'delete') {
				Ext.MessageBox.confirm('',
						'�� ������� ��� ������ ������� ��� ����������',
						function(btn) {
							if (btn == "yes") {
								Ext.Ajax.request({
									url : 'admincp.php',
									params : {
										module : 'Brands',
										task : 'DeleteImage',
										Id : record.data.Id
									},
									method : 'post',
									success : function() {
										Brands.ImagesStore.reload();
									}
								});
							}
						})
			}
			if (action == "apply") {
				Ext.Ajax.request({
					url : 'admincp.php',
					params : {
						module : 'Brands',
						task : 'SaveFile',
						Id : record.data.Id,
						ItemID : record.get('ItemID'),
						MainImage : 1
					},
					method : 'post',
					success : function() {
						Brands.ImagesStore.reload();
					}
				});
			}
			if (action == 'edit') {
				Brands.UploadPhoto();
				Ext.getCmp('Brands.UploadFileForm').getForm().load({
					url : '/admincp.php',
					method : 'post',
					params : {
						module : 'Brands',
						task : 'LoadRecord',
						Id : record.get('Id'),
						to : 'File'
					},
				});
			}
		}
	});

	var grid = new Ext.grid.EditorGridPanel(
			{
				store : Brands.ImagesStore,

				enableColLock : false,
				clicksToEdit : 1,
				height : 430,
				frame : true,
				id : 'BrandsGridImages',

				loadMask : true,
				autoWidth : true,
				listeners : {
					"afteredit" : function(oGrid_event) {
						Ext.Ajax
								.request({
									waitMsg : '���������� ���������...',
									url : 'admincp.php',
									params : {
										xaction : "SaveFile",
										Id : oGrid_event.record.data.Id,
										module : 'Brands',
										Sort : oGrid_event.record.data.Sort
									},
									success : function(response) {
										var result = Ext
												.decode(response.responseText);
										if (result) {
											if (result.success) {
												Brands.ImagesStore
														.commitChanges();
											}
										}

									},
									failure : function(response) {
										var result = response.responseText;
										Ext.MessageBox
												.alert('error',
														'could not connect to the database. retry later');
									}
								});
					}
				},
				columns : [
						{
							id : 'image',
							header : "",
							width : 100,
							sortable : false,
							dataIndex : 'image',
							renderer : function(value) {
								return "<center><img src='/thumbs/80x80/files/brands/"
										+ value + "' width='80'></center>";
							}
						}, {

							header : "����",
							width : 80,
							sortable : false,
							dataIndex : 'image',
							renderer : function(value) {
								return "/files/brands/" + value + "";
							}
						}, {

							header : "���������",
							width : 150,
							sortable : false,
							dataIndex : 'Title'
						}, {
							header : "���.",
							width : 50,
							sortable : true,
							dataIndex : 'Sort',
							editor : new Ext.form.NumberField()
						}, {

							header : "",
							width : 150,
							sortable : true,
							dataIndex : 'MainImage',
							renderer : function(value) {
								if (value == 1) {
									return "<b>��������</b>";
								}
								return "";
							}
						}, RowAction ],

				sm : new Ext.grid.RowSelectionModel({
					singleSelect : true
				}),
				viewConfig : {
					forceFit : false
				},

				bbar : pagingBar,
				plugins : RowAction,
				iconCls : 'icon-grid',
				split : true,
				tbar : [ {
					text : '��������� ����� ����������',
					handler : function() {
						Brands.UploadPhoto(1);

					},
					iconCls : 'add'
				}, {
					text : '��������� ����� ����������',
					handler : function() {
						alert('������� �������� �� ��������');

					},
					iconCls : 'add'
				} ]

			});

	var form = new Ext.form.FormPanel({
		id : 'Brands.EditRecord',
		frame : true,
		border : false,
		layout : 'fit',
		height : 550,
		plain : true,
		autoScroll : true,
		labelAlign : 'top',
		items : [ {
			xtype : 'hidden',
			name : 'Id',
			value : 0
		}, {
			xtype : 'hidden',
			name : 'CategoryID',
			value : 0
		}, {
			xtype : 'tabpanel',
			activeItem : 0,
			border : false,

			autoTabs : true,
			defaults : {
				frame : true,
				width : 850,
				autoHeight : true,
				border : false,
				layout : 'form'

			},
			items : [ {
				title : '��������',
				layout : 'form',
				autoScroll : true,
				iconCls : 'viewlist',
				items : [ {
					layout : 'table',
					layoutConfig : {
						columns : 2,
						tableAttrs : {
							style : {
								width : 600
							}
						}
					},

					items : [ {
						layout : 'form',
						width : 400,

						items : [ {
							xtype : 'textfield',
							fieldLabel : '������������',
							name : 'Title',
							width : 380
						} ]
					}
					 ]
				}, htmled({
					name : 'Description',
					label : '��������',
					height : 200
				}) ]
			}, {

				height : 460,
				layout : 'fit',
				title : '����������',
				items : [ grid ],
				iconCls : 'images',
				listeners : {
					activate : function() {
						Brands.ImagesStore.reload();
					}
				}
			}, {

				title : '��������� SEO',
				layout : 'form',
				iconCls : 'seo',

				items : [ {
					xtype : 'textfield',
					fieldLabel : 'URL �������� (���)',
					name : 'url',
					dataIndex : 'url',
					width : 850
				}, {
					xtype : 'textfield',
					fieldLabel : 'H1',
					name : 'H1',
					dataIndex : 'H1',
					width : 850
				}, {
					xtype : 'textfield',
					fieldLabel : 'Title',
					name : 'TitlePage',
					dataIndex : 'TitlePage',
					width : 850
				}, {
					xtype : 'textarea',
					fieldLabel : 'Description',
					name : 'DescPage',
					dataIndex : 'DescPage',
					width : 850
				}, {
					xtype : 'textarea',
					fieldLabel : 'Keywords',
					name : 'KeysPage',
					dataIndex : 'KeysPage',
					width : 850
				}, {
					xtype : 'textfield',
					fieldLabel : '���� ��� ����������� ������������',
					name : 'Tags',
					dataIndex : 'Tags',
					width : 850
				}, {
					xtype : 'combo',
					editable : false,
					typeAhead : true,
					triggerAction : 'all',
					store : new Ext.data.SimpleStore({
						fields : [ 'partyValue', 'partyName' ],
						data : [ [ '1', '��' ], [ '0', '���' ] ]
					}),
					mode : 'local',
					displayField : 'partyName',
					valueField : 'partyValue',
					lazyRender : true,
					name : 'Noindex',
					hiddenName : 'Noindex',
					fieldLabel : '������� �� ����������',
					listClass : 'x-combo-list-small'
				} ]
			},{
				title:'����������',
				layout : 'form',
				items:[{
					xtype : 'textfield',
					name : 'CreatedDate2',
					disabled : true,
					width : 140,
					fieldLabel : '���� ��������'
				}, {
					xtype : 'textfield',
					name : 'UpdatedDate2',
					disabled : true,
					width : 140,
					fieldLabel : '���� ����������'
				}]
			} ]
		} ]
	});
	return new Ext.Window(
			{
				modal : true,
				width : 920,
				border : false,

				title : '��������/�������������� ������',
				iconCls : 'add',
				id : 'Brands.EditWindowRecord',
				height : 550,
				listeners : {
					'show' : function() {
						form
								.getForm()
								.load(
										{
											url : '/admincp.php',
											params : {
												module : 'Brands',
												task : 'LoadRecord',
												to : 'Record',
												Id : Item
											},
											success : function(o, p) {
												if (p.result) {
													if (p.result.data) {
														
														if (p.result.data.Id) {
															
															Brands.ImagesStore.baseParams.ItemID = p.result.data.Id;
															Brands.ImagesStore
																	.reload();

														} else {
															Brands.ImagesStore.baseParams.ItemID = '';
														}
													} else {
														Brands.ImagesStore.baseParams.ItemID = '';
													}
												} else {
													Brands.ImagesStore.baseParams.ItemID = '';

												}
											},
											waitMsg : '���������.. ��� �������� ������'
										});
					}
				},
				layout : 'fit',
				items : [ form ],
				buttons : [ {
					text : '���������',
					iconCls : 'apply',
					handler : function() {
						if (form.getForm().isValid() != false) {
							tinyMCE.triggerSave();
							form
									.getForm()
									.submit(
											{
												url : '/admincp.php',
												method : 'post',
												waitMsg : '���������.. ��� �������� ������',
												params : {
													module : 'Brands',
													task : 'SaveData',
													to : 'Item'
												},
												success : function(form, action) {
													
													if (action.result) {
														if (action.result.success) {
															Ext
																	.getCmp(
																			'Brands.EditWindowRecord')
																	.close();
															App
																	.setAlert(
																			'',
																			'������ ������� ���������');
															Brands.Store
																	.reload();
														} else {
															App
																	.setAlert(
																			'',
																			'�� ����� ��������� ������ ��������� ������');
														}
													} else {
														App
																.setAlert('',
																		'�� ����� ��������� ������ ��������� ������');
													}
												},
												failure : function() {
													App
															.setAlert('',
																	'�������� ������� ���������');
												}
											});
						} else {
							App
									.setAlert('',
											'��������� ��������� �� �� ��������� �����');
						}

					}
				} ]
			}).show();
}

Brands.pagingBar = new Ext.PagingToolbar({
	pageSize : 25,
	store : Brands.Store,
	paramNames : {
		start : 'start',
		limit : 'limit'
	},
	displayInfo : true

});

// �������� ��� �������
Brands.RowAction = new Ext.ux.grid.RowActions({

	actions : [ {
		iconCls : 'delete',
		qtip : '�������'
	}, {
		iconCls : 'copy',
		qtip : '����������'
	}, {
		iconCls : 'edit',
		qtip : '�������������'
	} ],
	widthIntercept : Ext.isSafari ? 4 : 2,
	id : 'actions'
});
Brands.RowAction.on({
	action : function(grid, record, action, row, col) {
		// �������� ������
		if (action == 'delete') {
			Ext.MessageBox.confirm('',
					'�� ������� ��� ������ ������� ��� ������', function(btn) {
						if (btn == "yes") {
							Ext.Ajax.request({
								url : 'admincp.php',
								params : {
									module : 'Brands',
									task : 'DeleteRecord',
									Id : record.data.Id
								},
								method : 'post',
								success : function() {
									Brands.Store.reload();
								}
							});
						}
					})
		}
		// �������������� ������
		if (action == 'edit') {
			Brands.EditRecord(record.data.Id);
		}
	}
});

// ������� � �������
Brands.Grid = new Ext.grid.EditorGridPanel(
		{
			store : Brands.Store,
			
			frame : false,
			loadMask : true,
			id : 'Brands.Grid',
			layout : 'fit',
			enableColLock : false,
			clicksToEdit : 1,

			split : true,
			margins : '3 0 3 3',
			cmargins : '3 3 3 3',
			autoWidth : true,
			
			columns : [
					{
						id : 'id',
						header : "<b>Id</b>",
						width : 30,
						sortable : true,
						dataIndex : 'Id',
						renderer : function(v, tag, rec) {
							var style = "";
							if (rec.data.Active == 0) {
								style = "color:#999999";
							}
							return '<span style="' + style + '">' + v
									+ '</span>';
						}
					},
					{
						id : 'pos',
						header : "<b>���</b>",
						width : 40,
						sortable : true,
						dataIndex : 'Sort',
						editor : new Ext.form.TextField,
						renderer : function(v, tag, rec) {
							var style = "";
							if (rec.data.Active == 0) {
								style = "color:#999999";
							}
							return '<span style="' + style + '">' + v
									+ '</span>';
						}
					},
					{
						id : 'name',
						header : "<b>������������</b>",
						width : 200,
						sortable : true,
						dataIndex : 'Title',
						renderer : function(v, tag, rec) {
							var style = "";
							if (rec.data.Active == 0) {
								style = "color:#999999";
							}
							return '<span style="' + style + '">' + v
									+ '</span>';
						}
					},
					{
						id : 'link',
						header : "<b>������</b>",
						width : 100,
						sortable : false,
						dataIndex : 'Link',
						renderer : function(v, tag, rec) {
							var style = "";
							if (rec.data.Active == 0) {
								style = "color:#999999";
							}
							return '<span style="' + style + '">' + v
									+ '</span>';
						}

					},
					{
						header : "<center><b>��������</b></center>",
						width : 80,
						sortable : true,
						dataIndex : 'CreatedDate',
						renderer : function(v, tag, rec) {
							var style = "";
							if (rec.data.Active == 0) {
								style = "color:#999999";
							}
							return '<span style="' + style + '">' + v
									+ '</span>';
						}
					},
					{
						header : "<center><b>����������</b></center>",
						width : 80,
						sortable : true,
						dataIndex : 'UpdatedDate',
						renderer : function(v, tag, rec) {
							var style = "";
							if (rec.data.Active == 0) {
								style = "color:#999999";
							}
							return '<span style="' + style + '">' + v
									+ '</span>';
						}
					},
					{

						header : "<center><b></b></center>",
						sortable : true,
						dataIndex : 'active',
						width : 50,
						renderer : function(v, tag, rec) {
							var style = "";
							if (rec.data.Active == 0) {
								style = "color:#999999";
							}
							return '<div style="height:16px;"><form action="'
									+ rec.get('Link')
									+ '" id="BrandsLink'
									+ rec.get('Id')
									+ '" target="_blank"></form><img style="cursor:pointer;" ext:qtip="����������" onclick="document.getElementById(\'BrandsLink'
									+ rec.get('Id')
									+ '\').submit();" src="/core/images/icons/Internet.png" /></a> <span style="'
									+ style
									+ '"><img  onclick="Brands.UpdateStatus('
									+ rec.data.Id
									+ ', '
									+ (rec.data.Active == 0 ? 1 : 0)
									+ ');" style="cursor:pointer;" ext:qtip="'
									+ (rec.data.Active == 0 ? '������������'
											: '��������������')
									+ '" src="/core/images/icons/eye'
									+ (rec.data.Active == 0 ? '2' : '')
									+ '.png" /></span></div>';
						}
					}, Brands.RowAction ],

			sm : new Ext.grid.RowSelectionModel({
				singleSelect : false
			}),
			listeners:{
				afteredit:function(oGrid_event) {
					Ext.Ajax.request({
						waitMsg : '���������� ���������...',
						url : 'admincp.php',
						params : {
							task : "SaveData",
							Id : oGrid_event.record.data.Id,
							to : 'Item',
							module : 'Brands',
							Sort : oGrid_event.record.data.Sort
						},
						success : function(response) {
							var result = Ext.decode(response.responseText);
							if (result.success) {
								
								Brands.Store.commitChanges(); 
							}
						},
						failure : function(response) {
							var result = response.responseText;
							Ext.MessageBox.alert('error',
									'could not connect to the database. retry later');
						}
					});
				}
		
			
			},
			
			viewConfig : {
				forceFit : false
			},
			enableDragDrop:true,
			ddGroup:'Brands.Grid2Tree',
			
			bbar : Brands.pagingBar,
			plugins : Brands.RowAction,
			enableDragDrop   : true,
	        stripeRows       : true,
			split : true,

			tbar : [ {
				text : '�������� ����� ������',
				handler : function() {
					Brands.EditRecord(0);
				},
				iconCls : 'add'
			} ],
			region : 'center'

		});


Brands.UpdateStatus = function(Id, Status) {
	Ext.Ajax.request({
		waitMsg : '���������� ���������...',
		url : 'admincp.php',
		params : {
			task : 'SaveData',
			to : 'Item',
			Id : Id,
			Active : Status,
			module : 'Brands',
		},
		success : function(response) {
			var result = Ext.decode(response.responseText);
			if (result.success) {
				Brands.Store.getById(Id).set('Active', Status)
				Brands.Store.commitChanges(); // changes successful, get rid
				// of the red triangles
			}

		},
		failure : function(response) {
			var result = response.responseText;
			Ext.MessageBox.alert('error',
					'could not connect to the database. retry later');
		}
	});
}




Brands.View = {
		id:'Brands',
		title : '�������������',
		layout : 'border',

		items : [Brands.Grid ]
};



init_modules.push(Brands.View);
Ext.apply(actions, {
	'Brands' : function() {
		if (Ext.getCmp('Content').layout.activeItem.id != 'Brands') {
			Ext.getCmp('Content').layout.setActiveItem('Brands');

			Brands.Store.load({
				params : {
					start : 0,
					limit : 25
				}
			});
			

		}
	}
});
ModulesRightMenu += '<li><img src="core/icons/brend.png" /><a id="Brands" href="#">�������������</a></li>';
