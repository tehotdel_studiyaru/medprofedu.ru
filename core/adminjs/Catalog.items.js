if (!Catalog) {
	var Catalog = {};
}
Catalog.BrandsStore = new Ext.data.Store({
	proxy : new Ext.data.HttpProxy({
		url : '/admincp.php',
		method : 'POST'
	}),
	// autoLoad:true,
	baseParams : {
		module : "Brands",
		task : 'LoadBrandsForCombo'
	},
	reader : new Ext.data.JsonReader({
		id : 'Id',
		root : 'brands'
	}, [ {
		name : 'Title'
	}, {
		name : 'Id'
	}

	])
});
// ��������� ��� ���������
Catalog.TreeLoader = new Ext.tree.TreeLoader({
	url : '/admincp.php',
	baseParams : {
		xaction : 'LoadCategories',
		module : 'Catalog'
	},
	preloadChildren : true
});

// ������ ���������
Catalog.Tree = new Ext.ux.tree.TreeGrid(
		{
			title : '���������',
			width : 320,
			containerScroll : false,
			lines : false,
			singleExpand : true,
			useArrows : true,
			//autoScroll : true,
			animate : true,

			columnResize : false,
			enableSort : false,
			enableDragDrop : true,
			enableDD : true,
			ddGroup : 'Catalog.Grid2Tree',
			listeners : {
				beforenodedrop : function(e) {

					if (Ext.isArray(e.data.selections)) {
						if (e.target == this.getRootNode()) {
							return false;
						}

						var r = new Array();
						for ( var i = 0; i < e.data.selections.length; i++) {
							r.push(e.data.selections[i].data.Id);
						}
						var CategoryID = e.target.id;
						Ext.Ajax.request({
							url : '/admincp.php',
							method : 'post',
							params : {
								module : 'Catalog',
								task : 'ChangeCategoryIDItems',
								CategoryID : CategoryID,
								items : Ext.encode(r)
							},
							success : function() {
								Catalog.Store.reload();
							}
						});
						return true;
					}
				}
			},
			split : true,
			collapsible : true,
			margins : '3 0 3 3',
			cmargins : '3 3 3 3',
			tbar : [ {
				text : '�������� ���������',
				iconCls : 'add',
				handler : function() {
					Catalog.EditCategory(0);
				}
			} ],

			region : 'west',

			columns : [
					{
						header : '��������',
						dataIndex : 'Title',
						/*
						 * tpl : new Ext.XTemplate( '{Title:this.formatHours}', {
						 * formatHours : function(v) { console.log(v); return "<span
						 * style='width:170px; overflow:hidden;
						 * white-space:normal'>" + v + '</span>'; } }),
						 */
						width : 180

					},
					{
						header : '������',
						width : 80,
						dataIndex : 'Link'
					},
					{
						header : '',
						tpl : new Ext.XTemplate(
								'{Id:this.formatHours}',
								{
									formatHours : function(v) {

										return "<img src=\"/core/images/icons/delete.png\" title=\"�������\" style='cursor:pointer' onclick='Catalog.DeleteCategory("
												+ v
												+ ")'/>&nbsp;<img src=\"/core/images/icons/new-pencil.png\" title=\"�������������\" style='cursor:pointer' onclick='Catalog.EditCategory("
												+ v + ")'/>";
									}
								}),

						width : 50,
						dataIndex : 'Id',
					} ],

			loader : Catalog.TreeLoader

		});
// set the root node
var root = new Ext.tree.AsyncTreeNode({
	text : '������',
	draggable : false, // disable root node dragging
	id : 0
});
Catalog.Tree.setRootNode(root);
Catalog.Tree.on('click', function(n) {

	Catalog.Store.baseParams = {
		xaction : "LoadRecords",
		module : 'Catalog',
		CategoryID : n.id
	};
	Catalog.Store.load({
		params : {
			start : 0,
			limit : 25
		}
	});

});
Catalog.Tree.on('nodeDrop', function(n, dd, e, data) {

	var id = n.dropNode.id;
	var parentId = n.dropNode.parentNode.id;
	Ext.Ajax.request({
		url : '/admincp.php',
		method : 'post',
		params : {
			module : 'Catalog',
			task : 'SaveData',
			to : 'Category',
			Id : id,
			parentId : parentId
		}
	});
});
Catalog.Tree.on("enddrag", function(tree) {

	function simplifyNodes(node) {
		var resultNode = new Array();
		var kids = node.childNodes;
		var len = kids.length;
		for ( var i = 0; i < len; i++) {

			resultNode.push({
				Id : kids[i].id,
				Sort : (i + 1),
				childs : simplifyNodes(kids[i])
			});

		}
		return resultNode;
	}

	var encNodes = Ext.encode(simplifyNodes(Catalog.Tree.root));

	Ext.Ajax.request({
		method : 'POST',
		url : 'admincp.php',
		params : {
			module : 'Catalog',
			task : 'UpdateSortCategories',
			nodes : encNodes
		}

	});

});

// ���� ������ ��� ������� � ��������
Catalog.Store = new Ext.data.Store({

	proxy : new Ext.data.HttpProxy({
		url : 'admincp.php',
		method : 'POST'
	}),
	baseParams : {
		xaction : "LoadRecords",
		module : 'Catalog'
	},
	remoteSort : true,
	reader : new Ext.data.JsonReader({
		root : 'results',
		totalProperty : 'total',
		id : 'Id'
	}, [ {
		name : 'Id'
	}, {
		name : 'Title'
	}, {
		name : 'Sort'
	}, {
		name : 'UpdatedDate'
	}, {
		name : 'CreatedDate'
	}, {
		name : 'Active'
	}, {
		name : 'Link'
	}, {
		name : 'Price'
	}, {
		name : 'Article'
	}, {
		name : 'Available'
	}, {
		name : 'isVirtual'
	}, {
		name : 'CurrentCategoryID',
	}

	])

});

Catalog.EditCategory = function(Item) {
	var form = new Ext.form.FormPanel({
		id : 'Catalog.EditRecord',
		frame : true,
		border : false,
		layout : 'fit',
		height : 550,
		plain : true,
		autoScroll : true,
		labelAlign : 'top',
		items : [ {
			xtype : 'hidden',
			name : 'Id',
			value : 0
		}, {
			xtype : 'hidden',
			name : 'parentId',
			value : 0
		}, {
			xtype : 'tabpanel',
			activeItem : 0,
			border : false,

			autoTabs : true,
			defaults : {
				frame : true,
				width : 850,
				autoHeight : true,
				border : false,
				layout : 'form'

			},
			items : [ {
				title : '��������',
				layout : 'form',
				autoScroll : true,
				iconCls : 'viewlist',
				items : [ {
					layout : 'table',
					layoutConfig : {
						columns : 2,
						tableAttrs : {
							style : {
								width : 600
							}
						}
					},

					items : [ {
						layout : 'form',
						width : 400,

						items : [ {
							xtype : 'textfield',
							fieldLabel : '������������',
							name : 'Title',
							width : 380
						} ]
					}, {
						layout : 'form',
						width : 320,
						items : [ {

							xtype : 'trigger',
							fieldLabel : '���������',
							name : 'categoryName',
							triggerClass : 'x-form-search-trigger',
							onTriggerClick : function() {
								Catalog.ChangeCategory('Category');
							},

							editable : false,
							allowBlank : true,
							width : 300,
						} ]
					} ]
				}, htmled({
					name : 'Description_Top',
					label : '��������',
					height : 200
				})]
			}, {

				title : '��������� SEO',
				layout : 'form',
				iconCls : 'seo',

				items : [ {
					xtype : 'textfield',
					fieldLabel : 'URL �������� (���)',
					name : 'url',
					dataIndex : 'url',
					width : 850
				}, {
					xtype : 'textfield',
					fieldLabel : 'H1',
					name : 'H1',
					dataIndex : 'H1',
					width : 850
				}, {
					xtype : 'textfield',
					fieldLabel : 'Title',
					name : 'TitlePage',
					dataIndex : 'TitlePage',
					width : 850
				}, {
					xtype : 'textarea',
					fieldLabel : 'Description',
					name : 'DescPage',
					dataIndex : 'DescPage',
					width : 850
				}, {
					xtype : 'textarea',
					fieldLabel : 'Keywords',
					name : 'KeysPage',
					dataIndex : 'KeysPage',
					width : 850
				}, {
					xtype : 'textfield',
					fieldLabel : '���� ��� ����������� ������������',
					name : 'Tags',
					dataIndex : 'Tags',
					width : 850
				} ]
			} ]
		} ]
	});
	return new Ext.Window(
			{
				modal : true,
				border : false,
				width : 850,
				title : '��������/�������������� ���������',
				iconCls : 'add',
				id : 'Catalog.EditWindowRecord',
				height : 550,
				listeners : {
					'show' : function() {
						form.getForm().load({
							url : '/admincp.php',
							params : {
								module : 'Catalog',
								task : 'LoadRecord',
								to : 'Category',
								Id : Item
							},
							waitMsg : '���������.. ��� �������� ������'
						});
					}
				},
				layout : 'fit',
				items : [ form ],
				buttons : [ {
					text : '���������',
					iconCls : 'apply',
					handler : function() {
						if (form.getForm().isValid()) {
							tinyMCE.triggerSave();
							form
									.getForm()
									.submit(
											{
												url : '/admincp.php',
												method : 'post',
												params : {
													module : 'Catalog',
													task : 'SaveData',
													to : 'Category'
												},
												success : function(form, action) {
													if (action.result) {
														if (action.result.success) {
															Ext
																	.getCmp(
																			'Catalog.EditWindowRecord')
																	.close();
															App
																	.setAlert(
																			'',
																			'��������� ������� �������');
															Catalog.Tree.root
																	.reload();
														} else {
															App
																	.setAlert(
																			'',
																			'�� ����� ��������� ������ ��������� ������');
														}
													} else {
														App
																.setAlert('',
																		'�� ����� ��������� ������ ��������� ������');
													}
												},
												failure : function() {
													App
															.setAlert('',
																	'�� ����� ��������� ������ ��������� ������');
												}
											});
						} else {
							App.setAlert('',
									'��������� ������������ ���������� �����');
						}
					}
				} ]
			}).show();
}

Catalog.UploadPhoto = function(newFile) {

	var items = new Array();
	if (newFile) {
		items.push({
			xtype : 'fileuploadfield',
			emptyText : '�������� ���� ��� ��������',
			fieldLabel : '����',
			name : 'photo-path',
			width : '500',
			anchor : '95%',
			allowBlank : false,
			buttonCfg : {
				text : ' ',
				iconCls : 'upload-icon'
			}
		});
	}

	items.push({
		xtype : 'textfield',
		name : 'Title',
		fieldLabel : '���������',
		anchor : '90%'
	});
	items.push({
		xtype : 'hidden',
		name : 'Id'
	});

	items.push(htmled({
		name : 'Description',
		label : '��������',
		height : 200
	}));
	var form = new Ext.FormPanel({
		fileUpload : true,
		labelAlign : 'top',
		frame : true,
		shim : true,
		id : 'Catalog.UploadFileForm',

		items : [ items ]

	});
	return new Ext.Window(
			{

				layout : 'fit',
				shim : false,
				modal : true,
				title : '�������� ����������',
				id : 'Catalog.UploadImageWindow',
				width : 860,
				height : 550,
				autoScroll : true,
				closeAction : 'close',
				plain : true,
				listeners : {
					'close' : function() {
						Catalog.ImagesStore.reload();
					}
				},
				items : [ form ],
				buttons : [
						{
							text : '���������',
							handler : function() {
								if (form.getForm().isValid()) {
									tinyMCE.triggerSave();
									var idd = Ext.getCmp('Catalog.EditRecord')
											.getForm().findField('Id')
											.getValue();

									form
											.getForm()
											.submit(
													{
														url : 'admincp.php',
														method : 'POST',
														params : {
															ItemID : idd,
															module : 'Catalog',
															task : 'SaveFile'
														},
														waitTitle : '�������� ����������',
														waitMsg : '���������� ���������, ��� �������� ����������...',
														success : function(
																fotoupload, o) {
															Catalog.ImagesStore
																	.reload();
															Ext
																	.getCmp(
																			'Catalog.UploadImageWindow')
																	.close();

														},
														failure : function(
																fotoupload2, o) {
															Ext.MessageBox
																	.alert(
																			'������',
																			'�� ������� ��������� ����������');
														}
													});
								}
							}
						},
						{
							text : '�������',
							handler : function() {
								Catalog.ImagesStore.reload();
								Ext.getCmp('Catalog.UploadImageWindow').close();
							}
						} ]
			}).show();
}

Catalog.ImagesStore = new Ext.data.Store({
	proxy : new Ext.data.HttpProxy({
		url : 'admincp.php',
		method : 'POST'
	}),
	baseParams : {
		xaction : "LoadImages",
		module : 'Catalog'
	},

	reader : new Ext.data.JsonReader({
		root : 'results',
		totalProperty : 'total',
		id : 'Id'
	}, [ {
		name : 'Id'

	}, {
		name : 'image'
	}, {
		name : 'Sort'
	}, {
		name : 'MainImage'
	}, {
		name : 'Title'
	}, {
		name : 'ItemID'
	} ])

});

Catalog.EditRecord = function(Item) {

	var pagingBar = new Ext.PagingToolbar({
		pageSize : 25,
		store : Catalog.ImagesStore,
		paramNames : {
			start : 'start',
			limit : 'limit'
		},
		displayInfo : true

	});

	var RowAction = new Ext.ux.grid.RowActions({

		actions : [ {
			iconCls : 'apply',
			qtip : '������� ��������'
		}, '-', {
			iconCls : 'delete',
			qtip : '�������'
		}, {
			iconCls : 'edit',
			qtip : '�������������'
		} ],
		widthIntercept : Ext.isSafari ? 4 : 2,
		id : 'actions'
	});
	RowAction.on({
		action : function(grid, record, action, row, col) {
			if (action == 'delete') {
				Ext.MessageBox.confirm('',
						'�� ������� ��� ������ ������� ��� ����������',
						function(btn) {
							if (btn == "yes") {
								Ext.Ajax.request({
									url : 'admincp.php',
									params : {
										module : 'Catalog',
										task : 'DeleteImage',
										Id : record.data.Id
									},
									method : 'post',
									success : function() {
										Catalog.ImagesStore.reload();
									}
								});
							}
						})
			}
			if (action == "apply") {
				Ext.Ajax.request({
					url : 'admincp.php',
					params : {
						module : 'Catalog',
						task : 'SaveFile',
						Id : record.data.Id,
						ItemID : record.get('ItemID'),
						MainImage : 1
					},
					method : 'post',
					success : function() {
						Catalog.ImagesStore.reload();
					}
				});
			}
			if (action == 'edit') {
				Catalog.UploadPhoto();
				Ext.getCmp('Catalog.UploadFileForm').getForm().load({
					url : '/admincp.php',
					method : 'post',
					params : {
						module : 'Catalog',
						task : 'LoadRecord',
						Id : record.get('Id'),
						to : 'File'
					},
				});
			}
		}
	});

	var grid = new Ext.grid.EditorGridPanel(
			{
				store : Catalog.ImagesStore,

				enableColLock : false,
				clicksToEdit : 1,
				height : 430,
				frame : true,
				id : 'CatalogGridImages',

				loadMask : true,
				autoWidth : true,
				listeners : {
					"afteredit" : function(oGrid_event) {
						Ext.Ajax
								.request({
									waitMsg : '���������� ���������...',
									url : 'admincp.php',
									params : {
										xaction : "SaveFile",
										Id : oGrid_event.record.data.Id,
										module : 'Catalog',
										Sort : oGrid_event.record.data.Sort
									},
									success : function(response) {
										var result = Ext
												.decode(response.responseText);
										if (result) {
											if (result.success) {
												Catalog.ImagesStore
														.commitChanges();
											}
										}

									},
									failure : function(response) {
										var result = response.responseText;
										Ext.MessageBox
												.alert('error',
														'could not connect to the database. retry later');
									}
								});
					}
				},
				columns : [
						{
							id : 'image',
							header : "",
							width : 100,
							sortable : false,
							dataIndex : 'image',
							renderer : function(value) {
								return "<center><img src='/thumbs/80x80/files/catalog/"
										+ value + "' width='80'></center>";
							}
						}, {

							header : "����",
							width : 80,
							sortable : false,
							dataIndex : 'image',
							renderer : function(value) {
								return "/files/catalog/" + value + "";
							}
						}, {

							header : "���������",
							width : 150,
							sortable : false,
							dataIndex : 'Title'
						}, {
							header : "���.",
							width : 50,
							sortable : true,
							dataIndex : 'Sort',
							editor : new Ext.form.NumberField()
						}, {

							header : "",
							width : 150,
							sortable : true,
							dataIndex : 'MainImage',
							renderer : function(value) {
								if (value == 1) {
									return "<b>��������</b>";
								}
								return "";
							}
						}, RowAction ],

				sm : new Ext.grid.RowSelectionModel({
					singleSelect : true
				}),
				viewConfig : {
					forceFit : false
				},

				bbar : pagingBar,
				plugins : RowAction,
				iconCls : 'icon-grid',
				split : true,
				tbar : [ {
					text : '��������� ����� ����������',
					handler : function() {
						Catalog.UploadPhoto(1);

					},
					iconCls : 'add'
				}, {
					text : '��������� ����� ����������',
					handler : function() {
						alert('������� �������� �� ��������');

					},
					iconCls : 'add'
				} ]

			});

	var form = new Ext.form.FormPanel({
		id : 'Catalog.EditRecord',
		frame : true,
		border : false,
		layout : 'fit',
		height : 550,
		plain : true,
		TypeUF: 0,
		autoScroll : true,
		labelAlign : 'top',
		items : [ {
			xtype : 'hidden',
			name : 'Id',
			value : 0
		}, {
			xtype : 'hidden',
			name : 'CategoryID',
			value : 0
		}, {
			xtype : 'tabpanel',
			activeItem : 0,
			border : false,

			autoTabs : true,
			defaults : {
				frame : true,
				width : 850,
				autoHeight : true,
				border : false,
				layout : 'form'

			},
			items : [ {
				title : '��������',
				layout : 'form',
				autoScroll : true,
				iconCls : 'viewlist',
				items : [ {
					layout : 'table',
					layoutConfig : {
						columns : 3,
						tableAttrs : {
							style : {
								width : 600
							}
						}
					},

					items : [ {
						layout : 'form',
						width : 360,

						items : [ {
							xtype : 'textarea',
							fieldLabel : '������������',
							name : 'Title',
							
							width : 340
						} ]
					}, {
						layout : 'form',
						width : 200,
						items : [ {

							xtype : 'trigger',
							fieldLabel : '���������',
							name : 'categoryName',
							triggerClass : 'x-form-search-trigger',
							onTriggerClick : function() {
								Catalog.ChangeCategory('Item');
							},

							editable : false,
							allowBlank : true,
							width : 180
						} ]
					} ]
			},{xtype:'textarea',anchor:'90%', name:'s1', fieldLabel:'��� ����������'},
			{xtype:'textarea',anchor:'90%', name:'s2', fieldLabel:'���������� ����������'}
			,{xtype:'textarea',anchor:'90%', name:'s3', fieldLabel:'����� ����������'}
			,{xtype:'textarea',anchor:'90%', name:'s4', fieldLabel:'����� �������� �����, �'}
			,{xtype:'hidden', name:'TypeUF', value:0} ]
		} ]
	}]});
	return new Ext.Window(
			{
				modal : true,
				width : 920,
				border : false,

				title : '��������/�������������� ������',
				iconCls : 'add',
				id : 'Catalog.EditWindowRecord',
				height : 550,
				listeners : {
					'show' : function() {
						
						form
								.getForm()
								.load(
										{
											url : '/admincp.php',
											params : {
												module : 'Catalog',
												task : 'LoadRecord',
												to : 'Record',
												Id : Item
											},
											success : function(o, p) {
												if (p.result) {
													if (p.result.data) {

														if (p.result.data.Id) {
															form.getForm().loadRecord(p.result); 
														} 
													}
												} 
											},
											waitMsg : '���������.. ��� �������� ������'
										});
					}
				},
				layout : 'fit',
				items : [ form ],
				buttons : [ {
					text : '���������',
					iconCls : 'apply',
					handler : function() {
						if (form.getForm().isValid() != false) {
							tinyMCE.triggerSave();
							form
									.getForm()
									.submit(
											{
												url : '/admincp.php',
												method : 'post',
												waitMsg : '���������.. ��� �������� ������',
												params : {
													module : 'Catalog',
													task : 'SaveData',
													to : 'Item'
												},
												success : function(form, action) {
													if (action.result) {
														if (action.result.success) {
															Ext
																	.getCmp(
																			'Catalog.EditWindowRecord')
																	.close();
															App
																	.setAlert(
																			'',
																			'������ ������� ���������');
															Catalog.Store
																	.reload();
														} else {
															App
																	.setAlert(
																			'',
																			'�� ����� ��������� ������ ��������� ������');
														}
													} else {
														App
																.setAlert('',
																		'�� ����� ��������� ������ ��������� ������');
													}
												},
												failure : function() {
													App
															.setAlert('',
																	'�������� ������� ���������');
												}
											});
						} else {
							App
									.setAlert('',
											'��������� ��������� �� �� ��������� �����');
						}

					}
				} ]
			}).show();
}
Catalog.DeleteCategory = function(Id) {
	Ext.MessageBox.confirm('', '�� ������� ��� ������ ������� ��� ���������',
			function(btn) {
				if (btn == "yes") {
					Ext.Ajax.request({
						url : 'admincp.php',
						params : {
							module : 'Catalog',
							task : 'DeleteCategory',
							Id : Id
						},
						method : 'post',
						success : function() {
							Catalog.Tree.root.reload();
						}
					});
				}
			})
}
Catalog.pagingBar = new Ext.PagingToolbar({
	pageSize : 25,
	store : Catalog.Store,
	paramNames : {
		start : 'start',
		limit : 'limit'
	},
	displayInfo : true

});

// �������� ��� �������
Catalog.RowAction = new Ext.ux.grid.RowActions({

	actions : [ {
		iconCls : 'delete',
		qtip : '�������'
	}, {
		iconCls : 'copy',
		qtip : '����������'
	}, {
		iconCls : 'edit',
		qtip : '�������������'
	} ],
	header : "<center><b>��������</b></center>",
	widthIntercept : Ext.isSafari ? 4 : 2,
	id : 'actions'
});
Catalog.RowAction
		.on({
			action : function(grid, record, action, row, col) {
				// �������� ������
				if (action == 'delete') {
					if (record.get('isVirtual') == 1) {
						Ext.MessageBox
								.confirm(
										'',
										'�� ������� ��� ������ ������� ����������� ������?<br/> �������� ����� � ������ ����������� ����� ����������...',
										function(btn) {
											if (btn == "yes") {
												Ext.Ajax.request({
													url : 'admincp.php',
													params : {
														module : 'Catalog',
														task : 'DeleteRecord',
														Id : record.data.Id,
														virtual : true,
														CategoryID: record.get('CurrentCategoryID')

													},
													method : 'post',
													success : function() {
														Catalog.Store.reload();
													}
												});
											}
										});
					} else {
						Ext.MessageBox.confirm('',
								'�� ������� ��� ������ ������� ��� ������',
								function(btn) {
									if (btn == "yes") {
										Ext.Ajax.request({
											url : 'admincp.php',
											params : {
												module : 'Catalog',
												task : 'DeleteRecord',
												Id : record.data.Id
											},
											method : 'post',
											success : function() {
												Catalog.Store.reload();
											}
										});
									}
								});
					}
				}
				// �������������� ������
				if (action == 'edit') {
					Catalog.EditRecord(record.data.Id);
				}
			}
		});

// ������� � �������
Catalog.Grid = new Ext.grid.EditorGridPanel(
		{
			store : Catalog.Store,
			title : '������',
			frame : false,
			loadMask : true,
			id : 'Catalog.Grid',
			layout : 'fit',
			enableColLock : false,
			clicksToEdit : 1,

			split : true,
			margins : '3 0 3 3',
			cmargins : '3 3 3 3',
			autoWidth : true,

			columns : [
					{
						id : 'id',
						header : "<b>Id</b>",
						width : 30,
						sortable : true,
						dataIndex : 'Id',
						renderer : function(v, tag, rec) {
							var style = "";
							if (rec.data.Active == 0 && rec.data.isVirtual == 1) {
								style = "color:red";
							} else if (rec.data.isVirtual == 1) {
								style = 'color: #3764A0';
							} else if (rec.data.Active == 0) {
								style = "color:#999999";
							}
							return '<span style="' + style + '">' + v
									+ '</span>';
						}
					},
					{
						id : 'pos',
						header : "<b>���</b>",
						width : 40,
						sortable : true,
						dataIndex : 'Sort',
						editor : new Ext.form.TextField,
						renderer : function(v, tag, rec) {
							var style = "";
							if (rec.data.Active == 0 && rec.data.isVirtual == 1) {
								style = "color:red";
							} else if (rec.data.isVirtual == 1) {
								style = 'color: #3764A0';
							} else if (rec.data.Active == 0) {
								style = "color:#999999";
							}
							return '<span style="' + style + '">' + v
									+ '</span>';
						}
					},
					{
						id : 'name',
						header : "<b>������������</b>",
						width : 200,
						sortable : true,
						dataIndex : 'Title',
						renderer : function(v, tag, rec) {
							var style = "";
							if (rec.data.Active == 0 && rec.data.isVirtual == 1) {
								style = "color:red";
							} else if (rec.data.isVirtual == 1) {
								style = 'color: #3764A0';
							} else if (rec.data.Active == 0) {
								style = "color:#999999";
							}
							return '<span style="' + style + '">' + v
									+ '</span>';
						}
					},
					{
						header : "<center><b>��������</b></center>",
						width : 80,
						sortable : true,
						dataIndex : 'CreatedDate',
						renderer : function(v, tag, rec) {
							var style = "";
							if (rec.data.Active == 0 && rec.data.isVirtual == 1) {
								style = "color:red";
							} else if (rec.data.isVirtual == 1) {
								style = 'color: #3764A0';
							} else if (rec.data.Active == 0) {
								style = "color:#999999";
							}
							return '<span style="' + style + '">' + v
									+ '</span>';
						}
					},
					{
						header : "<center><b>����������</b></center>",
						width : 80,
						sortable : true,
						dataIndex : 'UpdatedDate',
						renderer : function(v, tag, rec) {
							var style = "";
							if (rec.data.Active == 0 && rec.data.isVirtual == 1) {
								style = "color:red";
							} else if (rec.data.isVirtual == 1) {
								style = 'color: #3764A0';
							} else if (rec.data.Active == 0) {
								style = "color:#999999";
							}
							return '<span style="' + style + '">' + v
									+ '</span>';
						}
					},
					{

						header : "<center><b>���</b></center>",
						sortable : true,
						dataIndex : 'active',
						width : 50,
						renderer : function(v, tag, rec) {
							var style = "";
							if (rec.data.Active == 0 && rec.data.isVirtual == 1) {
								style = "color:red";
							} else if (rec.data.isVirtual == 1) {
								style = 'color: #3764A0';
							} else if (rec.data.Active == 0) {
								style = "color:#999999";
							}
							return '<div style="height:16px;"><form action="'
									+ rec.get('Link')
									+ '" id="CatalogLink'
									+ rec.get('Id')
									+ '" target="_blank"></form><img style="cursor:pointer;" ext:qtip="����������" onclick="document.getElementById(\'CatalogLink'
									+ rec.get('Id')
									+ '\').submit();" src="/core/images/icons/Internet.png" /></a> <span style="'
									+ style
									+ '"><img  onclick="Catalog.UpdateStatus('
									+ rec.data.Id
									+ ', '
									+ (rec.data.Active == 0 ? 1 : 0)
									+ ');" style="cursor:pointer;" ext:qtip="'
									+ (rec.data.Active == 0 ? '������������'
											: '��������������')
									+ '" src="/core/images/icons/eye'
									+ (rec.data.Active == 0 ? '2' : '')
									+ '.png" /></span></div>';
						}
					}, Catalog.RowAction ],

			sm : new Ext.grid.RowSelectionModel({
				singleSelect : false
			}),

			listeners : {
				afteredit : function(oGrid_event) {
					Ext.Ajax
							.request({
								waitMsg : '���������� ���������...',
								url : 'admincp.php',
								params : {
									task : "SaveData",
									Id : oGrid_event.record.data.Id,
									to : 'Item',
									module : 'Catalog',
									Sort : oGrid_event.record.data.Sort
								},
								success : function(response) {
									var result = Ext
											.decode(response.responseText);
									if (result.success) {

										Catalog.Store.commitChanges();
									}
								},
								failure : function(response) {
									var result = response.responseText;
									Ext.MessageBox
											.alert('error',
													'could not connect to the database. retry later');
								}
							});
				},

				render : function() {
					Catalog.gridTargetEl = Catalog.Grid.getEl();

					Catalog.DropZone = new Ext.dd.DropTarget(
							Catalog.gridTargetEl, {
								ddGroup : 'Catalog.Grid2Tree',

								notifyDrop : function(ddSource, e, data) {

									return (true);
								}
							});
				}
			},

			viewConfig : {
				forceFit : false
			},
			enableDragDrop : true,
			ddGroup : 'Catalog.Grid2Tree',

			bbar : Catalog.pagingBar,
			plugins : Catalog.RowAction,
			enableDragDrop : true,
			stripeRows : true,
			split : true,

			tbar : [ {
				text : '�������� ����� ������',
				handler : function() {
					
					Catalog.EditRecord(0);

				},
				iconCls : 'add'
			} ],
			region : 'center'

		});

Catalog.UpdateStatus = function(Id, Status) {
	Ext.Ajax.request({
		waitMsg : '���������� ���������...',
		url : 'admincp.php',
		params : {
			task : 'SaveData',
			to : 'Item',
			Id : Id,
			Active : Status,
			module : 'Catalog',
		},
		success : function(response) {
			var result = Ext.decode(response.responseText);
			if (result.success) {
				Catalog.Store.getById(Id).set('Active', Status)
				Catalog.Store.commitChanges(); // changes successful, get rid
				// of the red triangles
			}

		},
		failure : function(response) {
			var result = response.responseText;
			Ext.MessageBox.alert('error',
					'could not connect to the database. retry later');
		}
	});
}

// ����� ���������
Catalog.ChangeCategory = function(To, Additional) {
	var params = {
		xaction : 'LoadCategories',
		module : 'Catalog'
	};
	if (Additional) {
		params.checked = true;
		params.Id = Ext.getCmp('Catalog.EditRecord').getForm().findField('Id')
				.getValue();
	}
	if (To == 'Category') {
		params.Id = Ext.getCmp('Catalog.EditRecord').getForm().findField('Id')
				.getValue();
	}
	var loader = new Ext.tree.TreeLoader({
		url : '/admincp.php',
		baseParams : params,
		preloadChildren : true
	});
	var Tree4CCOS = new Ext.tree.TreePanel({
		autoScroll : true,
		animate : true,
		enableDD : false,
		width : 500,
		floatable : false,
		margins : '5 0 0 0',
		cmargins : '5 5 0 0',
		split : true,
		expanded : true,
		containerScroll : true,
		lines : false,
		singleExpand : true,
		useArrows : true,

		loader : loader,

		root : {
			nodeType : 'async',
			text : '�������� ������',
			expanded : true,
			draggable : false,
			id : '0'
		}
	});

	var ChangeCatOfShopItem = new Ext.Window(
			{
				layout : 'fit',
				id : 'ChangeParentOfAux',
				title : '�������� ���������',
				shim : false,
				modal : true,
				width : 500,
				height : 250,
				autoScroll : true,
				closeAction : 'close',
				plain : true,
				items : Tree4CCOS,
				buttons : [ {
					text : '�������',
					iconCls : 'apply',
					handler : function() {
						if (Additional) {
							var result = new Array(), names = new Array(), selNodes = Tree4CCOS
									.getChecked();
							if (selNodes && selNodes.length > 0) {
								Ext.each(selNodes, function(node) {
									result.push(node.id);
									names.push(node.text);
								});

								Ext.getCmp('Catalog.EditRecord').getForm()
										.findField('VirtualCategoriesName')
										.setValue(names.join(', '));
								Ext.getCmp('Catalog.EditRecord').getForm()
										.findField('VirtualCategoriesId')
										.setValue(Ext.encode(result));
							}
							Ext.getCmp('ChangeParentOfAux').close();
						} else {
							var tr = Tree4CCOS.getSelectionModel()
									.getSelectedNode();
							if (!tr) {
								return Ext.MessageBox.alert('',
										'�������� ���������');
							}
							var id = tr.id;
							var name = tr.text;

							if (To == 'Category') {
								Ext.getCmp('Catalog.EditRecord').getForm()
										.findField('parentId').setValue(id);
							} else {
								Ext.getCmp('Catalog.EditRecord').getForm()
										.findField('CategoryID').setValue(id);
							}
							Ext.getCmp('Catalog.EditRecord').getForm()
									.findField('categoryName').setValue(name);
							Ext.getCmp('ChangeParentOfAux').close();
						}
					}
				} ]
			}).show();
}

Catalog.View = {

	title : '�������',
	layout : 'border',

	items : [ Catalog.Tree, Catalog.Grid ]
};
Catalog.Plugins.push(Catalog.View);

Catalog.functions.push({
	init : function() {
		Catalog.BrandsStore.reload();
		Catalog.Store.load({
			params : {
				start : 0,
				limit : 25
			}
		});
		Catalog.Tree.root.expand();
	}
})
