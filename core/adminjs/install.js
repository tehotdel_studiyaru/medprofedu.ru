var install = {};
install.window = function(dd) {
	var store2 = new Ext.data.Store( {
		proxy : new Ext.data.HttpProxy( {
			url : 'admincp.php',
			method : 'POST'
		}),
		baseParams : {
			module : 'admin',
			xaction : "AllModules"
		},

		reader : new Ext.data.JsonReader( {
			root : 'results',
			totalProperty : 'total',
			id : 'Id'
		}, [ {
			name : 'Id',
			
			mapping : 'Id'
		}, {
			name : 'Name',
			mapping : 'Name'
		}, {
			name : 'Global',
			mapping : 'Global'
		}, {
			name : 'ModuleID',
			mapping : 'ModuleID'
		},
		 {
			name : 'file',
			mapping : 'file'
		},
		 {
			name : 'Version',
			mapping : 'Version'
		},
		 {
			name : 'cls',
			mapping : 'cls'
		},
		{
			name : 'install',
			mapping : 'install'
		} ])
	});
	var grid2 = new Ext.grid.EditorGridPanel( {
		store : store2,
		frame : true,
		layout : 'fit',
		listeners:{'afteredit':function(oGrid_event){
		    Ext.Ajax.request({
		        waitMsg: '���������� ���������...',
		        url: 'admincp.php',
		        params: {
		            xaction: "Update",
		            id: oGrid_event.record.data.Id,
		            install: oGrid_event.record.data.install,
		            module: 'admin'
		        },
		        success: function(response){
		            var result = eval(response.responseText);
		            switch (result) {
		                case 33:
		                	store2.commitChanges(); // changes successful, get rid of the red triangles
		                	store2.reload(); // reload our datastore.
		                    break;
		                default:
		                    Ext.MessageBox.alert('������', '�� �������� ��������� ���������...');
		                    break;
		            }
		        },
		        failure: function(response){
		            var result = response.responseText;
		            Ext.MessageBox.alert('error', 'could not connect to the database. retry later');
		        }
		    });
		}},
		 
		enableColLock : false,
		clicksToEdit : 1,
		loadMask : true,
		autoWidth : true,
		columns : [{
			id : 'Name2',
			header : "#",
			width : 50,
			sortable : true,
			dataIndex : 'ModuleID'
		}, {
			id : 'Name',
			header : "��������",
			width : 100,
			sortable : true,
			dataIndex : 'Name'
		}, {
			id : 'Name3',
			header : "������",
			width : 100,
			sortable : true,
			dataIndex : 'Version'
		},{
			id : 'installed',
			header : "",
			width : 100,
			sortable : true,
			dataIndex : 'install',
			 editor: new Ext.form.ComboBox({
		            typeAhead: true,
		            triggerAction: 'all',
		            store: new Ext.data.SimpleStore({
		                fields: ['partyValue', 'partyName'],
		                data: [['1', '�����������'], ['0', '�� �����������']]
		            }),
		            mode: 'local',
		            displayField: 'partyName',
		            valueField: 'partyValue',
		            lazyRender: true,
		            listClass: 'x-combo-list-small'
}),
			renderer:function(value){
			   if (value == 1)
			   {
				   return "<b>�����������</b>";
			   }
			   else
			   {
				   return "�� �����������";
			   }
			}
		} ],

		sm : new Ext.grid.RowSelectionModel( {
			singleSelect : true
		}),
		viewConfig : {
			forceFit : true
		},
		height : 150,

		iconCls : 'icon-grid',
		split : true
	});

	new Ext.Window( {
		width : 650,
		onEsc : true,
		height : 450,
		modal : true,
		closeAction : 'close',
		layout : 'fit',
		frame : true,
		title : '���������� ��������',
		items : [ {
			xtype : 'tabpanel',
			activeItem : 0,
			items : [{
				title : '��� ������',
				layout : 'fit',
				items : [ grid2 ]
			} ]
		} ]
	}).show(dd);
	store2.load();
};
Ext.apply(actions, {
	'install' : function() {
		install.window(this);
	}
});
ModulesRightMenuS+='<li><img src="core/icons/layout.png"/><a id="install" href="#">���������� ��������</a></li>';