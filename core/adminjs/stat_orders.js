


var StatsOrders = {};

StatsOrders.base = new Ext.data.Store({
	proxy: new Ext.data.HttpProxy({
		url: 'admincp.php',
		method: 'POST'
	}),
	baseParams:{module:'shop', module:'shop',task: "LoadStatsOrder", id:3},
	autoLoad:false,
	reader: new Ext.data.JsonReader({
		root: 'results',
		totalProperty: 'total'
	}, [
	{name:'total'},
	{name:'month'},
	{name:'Y'}

	])

});
StatsOrders.base1 = new Ext.data.Store({
	proxy: new Ext.data.HttpProxy({
		url: 'admincp.php',
		method: 'POST'
	}),
	baseParams:{module:'shop', module:'shop',task: "LoadStatsOrder", id:0},
	autoLoad:false,
	reader: new Ext.data.JsonReader({
		root: 'results',
		totalProperty: 'total'
	}, [
	{name:'total'},
	{name:'month'},
	{name:'Y'}

	])

});
StatsOrders.base2 = new Ext.data.Store({
	proxy: new Ext.data.HttpProxy({
		url: 'admincp.php',
		method: 'POST'
	}),
	baseParams:{module:'shop', module:'shop',task: "LoadStatsOrder", id:1},
	autoLoad:false,
	reader: new Ext.data.JsonReader({
		root: 'results',
		totalProperty: 'total'
	}, [
	{name:'total'},
	{name:'month'},
	{name:'Y'}

	])

});
StatsOrders.base3 = new Ext.data.Store({
	proxy: new Ext.data.HttpProxy({
		url: 'admincp.php',
		method: 'POST'
	}),
	baseParams:{module:'shop', module:'shop',task: "LoadStatsOrder", id:2},
	autoLoad:false,
	reader: new Ext.data.JsonReader({
		root: 'results',
		totalProperty: 'total'
	}, [
	{name:'total'},
	{name:'month'},
	{name:'Y'}

	])

});
var df = new Date();
var year = df.format('Y');

var allYears = new Array();
for (var i = Number(year); i>=1970; i--){
	var s = new Array();
	s.push(i);
	s.push(i);
	allYears.push(s);
}
StatsOrders.date1 = {
		frame:true,
		layout:'form',
		xtype:'form',
		items:[{
			xtype:'combo',
            typeAhead: true,
            id:'StatsOrders.year',
            triggerAction: 'all',
            fieldLabel:'�������� ���',
            listeners:{'render':function(){
            	this.setValue(Number(year));
            }},
            store: new Ext.data.SimpleStore({
                fields: ['partyValue', 'partyName'],
                data: allYears
            }),
            mode: 'local',
            editable:false,
            displayField: 'partyName',
            valueField: 'partyValue',
            lazyRender: true,
            listClass: 'x-combo-list-small'
        }]
    };

StatsOrders.Chart1 = new Ext.Panel({
	title:'���������� ������',
	 width: 600,
     height: 400,
     listeners:{'renderer':function(){StatsOrders.base.load();}},
	items:{
        xtype: 'columnchart',
        store: StatsOrders.base,
        yField: 'Y',
        url: '/core/charts.swf',
        xField: 'month',
        tipRenderer : function(chart, record){
            return '����� ����� �������: '+record.data.total;
        },
        xAxis: new Ext.chart.CategoryAxis({
            title: '�����'
        }),
        yAxis: new Ext.chart.NumericAxis({
            title: ''
        }),
        extraStyle: {
           xAxis: {
                labelRotation: -90
            }
        }
    }});
StatsOrders.Chart2 = new Ext.Panel({
	title:'������ �� �������� "�����"',
	 width: 600,
     height: 400,
     listeners:{'render':function(){StatsOrders.base3.load();}},
	items:{
        xtype: 'columnchart',
        store: StatsOrders.base3,
        yField: 'Y',
        url: '/core/charts.swf',
        xField: 'month',
        tipRenderer : function(chart, record){
            return '����� ����� �������: '+record.data.total;
        },
        xAxis: new Ext.chart.CategoryAxis({
            title: '�����'
        }),
        yAxis: new Ext.chart.NumericAxis({
            title: ''
        }),
        extraStyle: {
           xAxis: {
                labelRotation: -90
            }
        }
    }});
StatsOrders.Chart3 = new Ext.Panel({
	title:'����������� ������',
	 width: 600,
     height: 400,
     listeners:{'render':function(){StatsOrders.base2.load();}},
	items:{
        xtype: 'columnchart',
        store: StatsOrders.base2,
        yField: 'Y',
        url: '/core/charts.swf',
        xField: 'month',
        tipRenderer : function(chart, record){
            return '����� ����� �������: '+record.data.total;
        },
        xAxis: new Ext.chart.CategoryAxis({
            title: '�����'
        }),
        yAxis: new Ext.chart.NumericAxis({
            title: ''
        }),
        extraStyle: {
           xAxis: {
                labelRotation: -90
            }
        }
    }});
StatsOrders.Chart4 = new Ext.Panel({
	title:'������ ��������� ��������',
	 width: 600,
     height: 400,
     listeners:{'render':function(){StatsOrders.base1.load();}},
	items:{
        xtype: 'columnchart',
        store: StatsOrders.base1,
        yField: 'Y',
        url: '/core/charts.swf',
        xField: 'month',
        tipRenderer : function(chart, record){
            return '����� ����� �������: '+record.data.total;
        },
        xAxis: new Ext.chart.CategoryAxis({
            title: '�����'
        }),
        yAxis: new Ext.chart.NumericAxis({
            title: ''
        }),
        extraStyle: {
           xAxis: {
                labelRotation: -90
            }
        }
    }});

StatsOrders.panel = new Ext.Panel({
	autoScroll:true,
	
	tbar:[StatsOrders.date1,{
		xtype:'button',
		text:'��������',
		handler:function(){
			var y = Ext.getCmp('StatsOrders.year').getValue();
			if (y==''){
				Ext.MessageBox.alert('', '�������� ���');
				return false;
			}
			StatsOrders.base.baseParams.year = y;
			StatsOrders.base1.baseParams.year = y;
			StatsOrders.base2.baseParams.year = y;
			StatsOrders.base3.baseParams.year = y;
			StatsOrders.base.load();
			StatsOrders.base1.load();
			StatsOrders.base2.load();
			StatsOrders.base3.load();
		},
		
	}],
	items:[
	       {
	    	   layout:'table',
	    	   layoutConfig:{columns:2},
	    	   items:[StatsOrders.Chart1,StatsOrders.Chart2,StatsOrders.Chart3,StatsOrders.Chart4]
	       }
	       ]
});