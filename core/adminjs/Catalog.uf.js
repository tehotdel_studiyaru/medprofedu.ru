if (!Catalog) {
	var Catalog = {};
}
Catalog.uf = {};

Catalog.uf.DeleteCategory = function(Id) {
	Ext.MessageBox.confirm('', '�� ������� ��� ������ ������� ��� ������ �������',
			function(btn) {
				if (btn == "yes") {
					Ext.Ajax.request({
						url : 'admincp.php',
						params : {
							module : 'Catalog',
							task : 'DeleteUFCategory',
							Id : Id
						},
						method : 'post',
						success : function() {
							Catalog.uf.Tree.root.reload();
						}
					});
				}
			})
}

Catalog.uf.EditCategory = function(Item, Edit) {
    if (Edit){
    	return Ext.MessageBox.alert('','������ ������ ������� ������ �������������');
    }
	var form = new Ext.form.FormPanel({
		id : 'UF.EditGroup',
		frame : true,
		border : false,
		
		height : 550,
		plain : true,
		autoScroll : true,
		labelAlign : 'top',
		items : [{
			xtype : 'hidden',
			name : 'Id',
			value : 0
		},
			 {
							xtype : 'textfield',
							fieldLabel : '������������',
							name : 'Title',
							anchor:'90%',
							allowBlank:false
		} ]
			
	});
	return new Ext.Window(
			{
				modal : true,
				border : false,
				width : 300,
				title : '��������/�������������� ������ �������',
				iconCls : 'add',
				id : 'UF.WindowGroup',
				height : 150,
				listeners : {
					'show' : function() {
						if (Item!=0){
						form.getForm().load({
							url : '/admincp.php',
							params : {
								module : 'Catalog',
								task : 'LoadUFGroup',
								to : 'Category',
								
								Id : Item
							},
							waitMsg : '���������.. ��� �������� ������'
						});
						}
					}
				},
				layout : 'fit',
				items : [ form ],
				buttons : [ {
					text : '���������',
					iconCls : 'apply',
					handler : function() {
						if (form.getForm().isValid()) {
							tinyMCE.triggerSave();
							form
									.getForm()
									.submit(
											{
												url : '/admincp.php',
												method : 'post',
												params : {
													module : 'Catalog',
													task : 'SaveUFGroup'
												},
												success : function(form, action) {
													if (action.result) {
														if (action.result.success) {
															Ext
																	.getCmp(
																			'UF.WindowGroup')
																	.close();
															App
																	.setAlert(
																			'',
																			'��������� ������� �������');
															Catalog.uf.Tree.root
																	.reload();
														} else {
															App
																	.setAlert(
																			'',
																			'�� ����� ��������� ������ ��������� ������');
														}
													} else {
														App
																.setAlert('',
																		'�� ����� ��������� ������ ��������� ������');
													}
												},
												failure : function() {
													App
															.setAlert('',
																	'�� ����� ��������� ������ ��������� ������');
												}
											});
						} else {
							App.setAlert('',
									'��������� ������������ ���������� �����');
						}
					}
				} ]
			}).show();
}

//��������� ��� ���������
Catalog.uf.TreeLoader = new Ext.tree.TreeLoader({
	url : '/admincp.php',
	baseParams : {
		xaction : 'LoadUFCategories',
		module : 'Catalog'
	},
	preloadChildren : true
});
Catalog.uf.Tree = new Ext.ux.tree.TreeGrid(
		{
			title : '������ �������',
			width :250,
			containerScroll : true,
			lines : false,
			singleExpand : true,
			useArrows : true,
			//autoScroll : true,
			animate : true,

			columnResize : false,
			enableSort : false,
			
		   
			split : true,
			collapsible : true,
			margins : '3 0 3 3',
			cmargins : '3 3 3 3',
			tbar : [ {
				text : '�������� ������ �������',
				iconCls : 'add',
				handler : function() {
					Catalog.uf.EditCategory(0);
				}
			} ],

			region : 'west',

			columns : [
					{
						header : '<b>�������� ������</b>',
						dataIndex : 'Title',
					
						width : 180

					},
					{
						header : '',
						tpl : new Ext.XTemplate(
								'{Id:this.formatHours}',
								{
									formatHours : function(v) {

										return "<img src=\"/core/images/icons/delete.png\" title=\"�������\" style='cursor:pointer' onclick='Catalog.uf.DeleteCategory("
												+ v
												+ ")'/>&nbsp;<img src=\"/core/images/icons/new-pencil.png\" title=\"�������������\" style='cursor:pointer' onclick='Catalog.uf.EditCategory("
												+ v + (v==0?', true':'')+")'/>";
									}
								}),

						width : 50,
						dataIndex : 'Id',
					} ],

			loader : Catalog.uf.TreeLoader

		});
// set the root node

Catalog.uf.Tree.setRootNode( new Ext.tree.AsyncTreeNode({
	text : '������',
	draggable : false, // disable root node dragging
	id : 0
}));
Catalog.uf.Tree.on('click', function(n) {

	Catalog.uf.Store.baseParams = {
		xaction : "LoadUFRecords",
		module : 'Catalog',
		
		Group : n.id
	};
	Catalog.uf.Store.load({
		params : {
			start : 0,
			limit : 25
		}
	});

});

// ���� ������ ��� ������� � ��������
Catalog.uf.Store = new Ext.data.Store({

	proxy : new Ext.data.HttpProxy({
		url : 'admincp.php',
		method : 'POST'
	}),
	baseParams : {
		xaction : "LoadUFRecords",
		module : 'Catalog'
	},
	remoteSort : true,
	reader : new Ext.data.JsonReader({
		root : 'results',
		totalProperty : 'total',
		id : 'Id'
	}, [ {
		name : 'Id'
	}, {
		name : 'Title'
	}, {
		name : 'Sort'
	}, {
		name : 'Type'
	} ])

});

// �������� ��� �������
Catalog.uf.RowAction = new Ext.ux.grid.RowActions({

	actions : [ {
		iconCls : 'delete',
		qtip : '�������'
	}, {
		iconCls : 'edit',
		qtip : '�������������'
	} ],
	widthIntercept : Ext.isSafari ? 4 : 2,
	id : 'actions'
});
Catalog.uf.RowAction.on({
	action : function(grid, record, action, row, col) {
		// �������� ������
		switch (action) {
		case "edit":
			if (record.data.Type==1){
				Catalog.uf.EditTextarea(record.data.Id);
			}
			else if (record.data.Type==2){
				
			} 
			else {
				Catalog.uf.EditTextfield(record.data.Id);
			}
			break;
		case "delete":
			Ext.MessageBox.confirm('', '�� ������� ��� ������ ������� ��� ����?<br/>��� ������ �� ������� �� ����� ���� ����� �������!',
					function(btn) {
						if (btn == "yes") {
							Ext.Ajax.request({
								url : 'admincp.php',
								params : {
									module : 'Catalog',
									task : 'DeleteUF',
									Id : record.data.Id
								},
								method : 'post',
								success : function() {
									Catalog.uf.Store.reload();
								}
							});
						}
					})
			break;
		}
	}
});

// ���������
Catalog.uf.pagingBar = new Ext.PagingToolbar({
	pageSize : 25,
	store : Catalog.uf.Store,
	paramNames : {
		start : 'start',
		limit : 'limit'
	},
	displayInfo : true

});

// ������� � ������
Catalog.uf.Grid = new Ext.grid.EditorGridPanel({
			store : Catalog.uf.Store,
			title : '',
			frame : false,
			loadMask : true,
			
			layout : 'fit',
			enableColLock : false,
			clicksToEdit : 1,
			title:'����',
			split : true,
			margins : '3 0 3 3',
			cmargins : '3 3 3 3',
			autoWidth : true,
			listeners : {
				afteredit : function(oGrid_event) {
					Ext.Ajax
							.request({
								waitMsg : '���������� ���������...',
								url : 'admincp.php',
								params : {
									task : "SaveUFField",
									Id : oGrid_event.record.data.Id,
									
									module : 'Catalog',
									Sort : oGrid_event.record.data.Sort
								},
								success : function(response) {
									var result = Ext
											.decode(response.responseText);
									if (result.success) {

										Catalog.uf.Store.commitChanges();
									}
								},
								failure : function(response) {
									var result = response.responseText;
									Ext.MessageBox
											.alert('error',
													'could not connect to the database. retry later');
								}
							});
				}
			},
			columns : [
					{
						id : 'id',
						header : "<b>Id</b>",
						width : 30,
						sortable : true,
						dataIndex : 'Id'
					},
					{
						
						header : "<b>���</b>",
						width : 40,
						sortable : true,
						dataIndex : 'Sort',
						editor : new Ext.form.TextField
					},
					{

						header : "<b>���</b>",
						width : 50,
						sortable : true,
						dataIndex : 'Type'
					},
					{
						id : 'name',
						header : "<b>�������� ����</b>",
						width : 200,
						sortable : true,
						dataIndex : 'Title'
					},
					{

						header : "<b>�������� ����</b>",
						width : 50,
						sortable : true,
						dataIndex : 'Id',
						renderer:function(v, tag, rec) {
							return 'uf_'+rec.data.Id;
						}
					}, Catalog.uf.RowAction ],

			sm : new Ext.grid.RowSelectionModel({
				singleSelect : true
			}),
			
			
			viewConfig : {
				forceFit : false
			},
			
			
			bbar : Catalog.uf.pagingBar,
			plugins : Catalog.uf.RowAction,
			
	        stripeRows       : true,
			split : true,
			region:'center',
			tbar : [ {
				text : '�������� ����� ������',
				handler : function() {
				    Catalog.uf.SelectType();
				},
				iconCls : 'add'
			} ]

		});

Catalog.uf.SelectGroup = function(){
	
		var params = {
			xaction : 'LoadUFCategories',
			module : 'Catalog'
		};
		
		var loader = new Ext.tree.TreeLoader({
			url : '/admincp.php',
			baseParams : params,
			preloadChildren : true
		});
		var Tree4CCOS = new Ext.tree.TreePanel({
			autoScroll : true,
			animate : true,
			enableDD : false,
			width : 500,
			floatable : false,
			margins : '5 0 0 0',
			cmargins : '5 5 0 0',
			rootVisible:false,
			split : true,
			expanded : true,
			
			containerScroll : false,
			selModel:new Ext.tree.DefaultSelectionModel({
			  listeners:{selectionchange:function(ui, n){
					Ext.getCmp('UF.FormSelectType').getForm()
					.findField('Group').setValue(n.id);

		          Ext.getCmp('UF.FormSelectType').getForm().findField('categoryName').setValue(''+n.text);
		          Ext.getCmp('UF.ChangeGroup').close();
			  }}
			}),

			loader : loader,

			root : {
				nodeType : 'async',
				text : '����� ����',
				expanded : true,
				draggable : false,
				id : '0'
			}
		});
		

		var ChangeCatOfShopItem = new Ext.Window({
					layout : 'fit',
					id : 'UF.ChangeGroup',
					title : '�������� ������ �������',
					shim : false,
					modal : true,
					
					width : 500,
					height : 250,
					autoScroll : true,
					closeAction : 'close',
					plain : true,
					items : Tree4CCOS
					
}).show();
	
}

// ����� ��� ������ ���� ������
Catalog.uf.SelectType = function() {
	var form = new Ext.form.FormPanel({
		id : 'UF.FormSelectType',
		frame : true,
		border : false,
		
		height : 550,
		plain : true,
		autoScroll : true,
		labelAlign : 'top',
		items : [{
			xtype : 'hidden',
			name : 'Group',
			value : 0
		},
		 {
			xtype : 'textfield',
			fieldLabel : '��������� ����',
			name : 'Title',
			width:150,
			allowBlank:false
},{
            typeAhead: true,
            triggerAction: 'all',
            value:'0',
            store: new Ext.data.SimpleStore({
                fields: ['partyValue', 'partyName'],
                data: [['0', '��������� ����'], ['1', '������������� ��������� ����']]
            }),
            name:'Type',
            editable:false,
            xtype:'combo',
            width:200,
            hiddenName:'Type',
            allowBlank:false,
            mode: 'local',
            fieldLabel:'��� ����',
            displayField: 'partyName',
            valueField: 'partyValue',
            lazyRender: true,
            listClass: 'x-combo-list-small'
        },{

			xtype : 'trigger',
			fieldLabel : '������ �������',
			name : 'categoryName',
			value:'',
			width:200,
		    allowBloank:false,
			triggerClass : 'x-form-search-trigger',
			onTriggerClick : function() {
				Catalog.uf.SelectGroup();
			},

			editable : false,
			allowBlank : true
			
		} ]
			
	});
	return new Ext.Window(
			{
				modal : true,
				border : false,
				width : 400,
				title : '����� ����',
				iconCls : 'add',
				id : 'UF.WindowSelectType',
				height : 250,
			
				layout : 'fit',
				items : [ form ],
				buttons : [ {
					text : '���������',
					iconCls : 'apply',
					handler : function() {
						if (form.getForm().isValid()) {
							tinyMCE.triggerSave();
							form
									.getForm()
									.submit(
											{
												url : '/admincp.php',
												method : 'post',
												params : {
													module : 'Catalog',
													task : 'addUFField'
												},
												success : function(form, action) {
													if (action.result) {
														if (action.result.success) {
														   var Id = action.result.Id;
														  var Type = Ext.getCmp('UF.FormSelectType').getForm().findField('Type').getValue();
														  if (Type==0){
															  Catalog.uf.EditTextfield(Id);
														  }
														  else if (Type==1){
															  Catalog.uf.EditTextarea(Id);
														  }
															Ext
																	.getCmp(
																			'UF.WindowSelectType')
																	.close();
															
															
														} else {
															App
																	.setAlert(
																			'',
																			'�� ����� ��������� ������ ��������� ������');
														}
													} else {
														App
																.setAlert('',
																		'�� ����� ��������� ������ ��������� ������');
													}
												},
												failure : function() {
													App
															.setAlert('',
																	'�� ����� ��������� ������ ��������� ������');
												}
											});
						} else {
							App.setAlert('',
									'��������� ������������ ���������� �����');
						}
					}
				} ]
			}).show();
}
// ����� ��� �������������� ���� textfield
Catalog.uf.EditTextfield = function(Item) {
	var form = new Ext.form.FormPanel({
		id : 'UF.EditTextfield',
		frame : true,
		border : false,
		
		height : 550,
		plain : true,
		autoScroll : true,
		labelAlign : 'top',
		items : [{
			xtype : 'hidden',
			name : 'Id',
			value : 0
		},
			 {
							xtype : 'textfield',
							fieldLabel : '�������� ����',
							name : 'Title',
							anchor:'90%',
							allowBlank:false
		} ]
			
	});
	return new Ext.Window(
			{
				modal : true,
				border : false,
				width : 300,
				title : '��������/�������������� ����',
				iconCls : 'add',
				id : 'UF.EditWindowTextfield',
				height : 150,
				listeners : {
					'show' : function() {
						if (Item!=0){
						form.getForm().load({
							url : '/admincp.php',
							params : {
								module : 'Catalog',
								task : 'LoadUFField',
								
								
								Id : Item
							},
							waitMsg : '���������.. ��� �������� ������'
						});
						}
					}
				},
				layout : 'fit',
				items : [ form ],
				buttons : [ {
					text : '���������',
					iconCls : 'apply',
					handler : function() {
						if (form.getForm().isValid()) {
							tinyMCE.triggerSave();
							form
									.getForm()
									.submit(
											{
												url : '/admincp.php',
												method : 'post',
												params : {
													module : 'Catalog',
													task : 'SaveUFField'
												},
												success : function(form, action) {
													if (action.result) {
														if (action.result.success) {
															Catalog.uf.Store.reload();
															Ext
																	.getCmp(
																			'UF.EditWindowTextfield')
																	.close();
															App
																	.setAlert(
																			'',
																			'��������� ������� �������');
															Catalog.uf.Tree.root
																	.reload();
														} else {
															App
																	.setAlert(
																			'',
																			'�� ����� ��������� ������ ��������� ������');
														}
													} else {
														App
																.setAlert('',
																		'�� ����� ��������� ������ ��������� ������');
													}
												},
												failure : function() {
													App
															.setAlert('',
																	'�� ����� ��������� ������ ��������� ������');
												}
											});
						} else {
							App.setAlert('',
									'��������� ������������ ���������� �����');
						}
					}
				} ]
			}).show();
}
// ����� ��� �������������� ���� textarea
Catalog.uf.EditTextarea = function(Item) {
	var form = new Ext.form.FormPanel({
		id : 'UF.EditTextarea',
		frame : true,
		border : false,
		
		height : 550,
		plain : true,
		autoScroll : true,
		labelAlign : 'top',
		items : [{
			xtype : 'hidden',
			name : 'Id',
			value : 0
		},
			 {
							xtype : 'textfield',
							fieldLabel : '�������� ����',
							name : 'Title',
							anchor:'90%',
							allowBlank:false
		} ]
			
	});
	return new Ext.Window(
			{
				modal : true,
				border : false,
				width : 300,
				title : '��������/�������������� ����',
				iconCls : 'add',
				id : 'UF.EditWindowTextarea',
				height : 150,
				listeners : {
					'show' : function() {
						if (Item!=0){
						form.getForm().load({
							url : '/admincp.php',
							params : {
								module : 'Catalog',
								task : 'LoadUFField',
								
								
								Id : Item
							},
							waitMsg : '���������.. ��� �������� ������'
						});
						}
					}
				},
				layout : 'fit',
				items : [ form ],
				buttons : [ {
					text : '���������',
					iconCls : 'apply',
					handler : function() {
						if (form.getForm().isValid()) {
							tinyMCE.triggerSave();
							form
									.getForm()
									.submit(
											{
												url : '/admincp.php',
												method : 'post',
												params : {
													module : 'Catalog',
													task : 'SaveUFField'
												},
												success : function(form, action) {
													if (action.result) {
														if (action.result.success) {
															Catalog.uf.Store.reload();
															Ext
																	.getCmp(
																			'UF.EditWindowTextarea')
																	.close();
															App
																	.setAlert(
																			'',
																			'��������� ������� �������');
															Catalog.uf.Tree.root
																	.reload();
														} else {
															App
																	.setAlert(
																			'',
																			'�� ����� ��������� ������ ��������� ������');
														}
													} else {
														App
																.setAlert('',
																		'�� ����� ��������� ������ ��������� ������');
													}
												},
												failure : function() {
													App
															.setAlert('',
																	'�� ����� ��������� ������ ��������� ������');
												}
											});
						} else {
							App.setAlert('',
									'��������� ������������ ���������� �����');
						}
					}
				} ]
			}).show();
}
// ����� ��� �������������� ���� select
Catalog.uf.EditSelect = function() {
}
// ����� ��� �������������� ���� checkboxgroup
Catalog.uf.EditCheckbox = function() {
}
// ����� ��� �������������� ���� radioboxgroup
Catalog.uf.EditRadiobox = function() {
}
Catalog.uf.render = false;

Catalog.Plugins.push({
	title:'��������������',
	listeners:{
		render:function(){
			Catalog.uf.Store.load();
			Catalog.uf.Tree.root.expand();
			Catalog.uf.render = true;
		}
	},
	border:false,
	layout : 'border',
	items : [ Catalog.uf.Tree, Catalog.uf.Grid ]
});
Catalog.functions.push({
	init : function() {
		if (Catalog.uf.render){
			Catalog.uf.Store.load();
			Catalog.uf.Tree.root.expand();
		}
	}
})

