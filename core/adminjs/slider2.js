var slider2 = {};

slider2.updateRecord = function(oGrid_event){
    Ext.Ajax.request({
        waitMsg: '���������� ���������...',
        url: 'admincp.php',
        params: {
            xaction: "Update",
            id: oGrid_event.record.data.id,
            active: oGrid_event.record.data.active,
            module: 'slider2',
            pos: oGrid_event.record.data.pos
        },
        success: function(response){
            var result = eval(response.responseText);
            switch (result) {
                case 33:
                    slider2.base.commitChanges(); // changes successful, get rid of the red triangles
                    slider2.base.reload(); // reload our datastore.
                    break;
                default:
                    Ext.MessageBox.alert('������', '�� �������� ��������� ���������...');
                    break;
            }
        },
        failure: function(response){
            var result = response.responseText;
            Ext.MessageBox.alert('error', 'could not connect to the database. retry later');
        }
    });
};



slider2.addedit = function(id){


    function UploadPhoto(){
        var fotoupload2 = new Ext.FormPanel({
        
            fileUpload: true,
            width: '100%',
            frame: true,
            //border:false,
            
            shim: true,
            bodyStyle: 'padding: 10px 10px 0 10px;',
            labelWidth: 50,
            items: [{
                xtype: 'fileuploadfield',
                emptyText: '�������� ���� ��� ��������',
                fieldLabel: '����',
                name: 'photo-path',
                width: '500',
                anchor: '95%',
                allowBlank: false,
                buttonCfg: {
                    text: ' ',
                    iconCls: 'upload-icon'
                }
            }],
            buttonAlign: 'center',
            buttons: [{
                text: '���������',
                handler: function(){
                    if (fotoupload2.getForm().isValid()) {
                        var idd = Ext.getCmp('slider2.form').getForm().findField('id').getValue();
                        
                        fotoupload2.getForm().submit({
                            url: 'admincp.php',
                            method: 'POST',
                            params: {
                                id: idd,
                                module: 'slider2',
                                task: 'UploadPhoto'
                            },
                            waitTitle: '�������� ����������',
                            waitMsg: '���������� ���������, ��� �������� ����������...',
                            success: function(fotoupload, o){
                                //msg('Success', 'Processed file "'+o.result.file+'" on the server');
                                //Ext.getCmp('slider2.form.tabimages.grid').store.reload();
                                Ext.getCmp('slider2.UploadWindow').close();
                                
                            },
                            failure: function(fotoupload2, o){
                                Ext.MessageBox.alert('������', '�� ������� ��������� ����������');
                            }
                        });
                    }
                }
            }]
        });
        var shopupfileswin2 = new Ext.Window({
            //applyTo     : 'hello-win',
            layout: 'fit',
            shim: false,
            modal: true,
            title: '�������� ����������',
            id: 'slider2.UploadWindow',
            width: 400,
            height: 200,
            autoScroll: true,
            closeAction: 'close',
            plain: true,
            listeners: {
                'close': function(){
                    Ext.getCmp('slider2.form.tabimages.grid').store.reload();
                }
            },
            items: [fotoupload2],
            buttons: [{
                text: '�������',
                handler: function(){
                    Ext.getCmp('slider2.form.tabimages.grid').store.reload();
                    Ext.getCmp('slider2.UploadWindow').close();
                }
            }]
        }).show();
    }
    
    var base = new Ext.data.Store({
        proxy: new Ext.data.HttpProxy({
            url: 'admincp.php',
            method: 'POST'
        }),
        baseParams: {
            xaction: "Listing_Images",
            module: 'slider2'
        },
        
        reader: new Ext.data.JsonReader({
            root: 'results',
            totalProperty: 'total',
            id: 'id'
        }, [{
            name: 'id',
            mapping: 'id'
        }, {
            name: 'image',
            mapping: 'image'
        }, {
            name: 'file',
            mapping: 'file'
        }, {
            name: 'osn',
            mapping: 'osn'
        }, {
            name: 'pos',
            mapping: 'pos'
        }])
    
    });
    // End Base for ArticleGrid
    
    // PagingBar for articlegrid
    var pagingBar = new Ext.PagingToolbar({
        pageSize: 25,
        store: base,
        paramNames: {
            start: 'start',
            limit: 'limit'
        },
        displayInfo: true
    
    
    });
    // End
    // ArtclisRowAction
    
    var RowAction = new Ext.ux.grid.RowActions({
    
        actions: [{
            iconCls: 'apply',
            qtip: '������� ��������'
        }, {
            iconCls: 'delete',
            qtip: '�������'
        }],
        widthIntercept: Ext.isSafari ? 4 : 2,
        id: 'actions'
    });
    RowAction.on({
        action: function(grid, record, action, row, col){
            if (action == 'delete') {
                Ext.MessageBox.confirm('', '�� ������� ��� ������ ������� ��� ����������', function(btn){
                    if (btn == "yes") {
                        Ext.Ajax.request({
                            url: 'admincp.php',
                            params: {
                                module: 'slider2',
                                task: 'deleteImage',
                                id: record.data.id
                            },
                            method: 'post',
                            success: function(){
                                base.reload();
                            }
                        });
                    }
                })
            }
            if (action == "apply") {
                Ext.Ajax.request({
                    url: 'admincp.php',
                    params: {
                        module: 'slider2',
                        task: 'setOsnImage',
                        id: record.data.id
                    },
                    method: 'post',
                    success: function(){
                        base.reload();
                    }
                });
            }
        }
    });
    
    var grid = new Ext.grid.EditorGridPanel({
        store: base,
        
        
        id: 'slider2.form.tabimages.grid',
        enableColLock: false,
        clicksToEdit: 1,
        height: 290,
        frame: true,
        loadMask: true,
        autoWidth: true,
        listeners: {
            "afteredit": function(oGrid_event){
                Ext.Ajax.request({
                    waitMsg: '���������� ���������...',
                    url: 'admincp.php',
                    params: {
                        xaction: "UpdateImagePos",
                        id: oGrid_event.record.data.id,
                        module: 'slider2',
                        pos: oGrid_event.record.data.pos
                    },
                    success: function(response){
                        var result = eval(response.responseText);
                        switch (result) {
                            case 33:
                                Ext.getCmp('slider2.form.tabimages.grid').store.commitChanges();
                                
                                break;
                            default:
                                Ext.MessageBox.alert('������', '�� �������� ��������� ���������...');
                                break;
                        }
                    },
                    failure: function(response){
                        var result = response.responseText;
                        Ext.MessageBox.alert('error', 'could not connect to the database. retry later');
                    }
                });
            }
        },
        columns: [{
            id: 'image',
            header: "",
            width: 200,
            sortable: true,
            dataIndex: 'image',
            renderer: function(value){
                return "<center><img src='files/slider2/" + value + "' width='80'></center>";
            }
        }, {
            id: 'file',
            header: "����",
            width: 150,
            sortable: true,
            dataIndex: 'file'
        }, {
            header: "���.",
            width: 150,
            sortable: true,
            dataIndex: 'pos',
            editor: new Ext.form.NumberField()
        }, {
            id: 'osn',
            header: "",
            width: 150,
            sortable: true,
            dataIndex: 'osn',
            renderer: function(value){
                if (value == 1) {
                    return "<b>��������</b>";
                }
                return "";
            }
        }, RowAction],
        
        sm: new Ext.grid.RowSelectionModel({
            singleSelect: true
        }),
        viewConfig: {
            forceFit: false
        },
        height: 150,
        bbar: pagingBar,
        plugins: RowAction,
        iconCls: 'icon-grid',
        split: true,
        tbar: [{
            text: '��������� ����� ����������',
            handler: function(){
                UploadPhoto();
                
            },
            iconCls: 'add'
        }]
    
    
    });
    
    
    var form = new Ext.FormPanel({
        id: 'slider2.form',
        width: 890,
        height:290,
        frame: true,
        labelAlign: 'top',
        items: [{
            xtype: 'tabpanel',
            activeItem: 0,
            defaults: {
                frame: true,
                width: 800,
                height: 290,
                bodyStyle: 'padding:10px;'
            },
            items: [{
                title: '��������',
                layout: 'form',
                autoScroll: true,
                iconCls: 'viewlist',
                items: [{
                    layout: 'table',
                    layoutConfig: {
                        columns: 4,
                        tableAttrs: {
                            style: {
                                width: 880
                            }
                        }
                    },
                    defaults: {
                        width: 250
                    },
                    items: [{
                        layout: 'form',
                        width: 400,
                        colspan: 2,
                        items: [{
                        
                            xtype: 'textfield',
                            fieldLabel: '����� ���������� �����',
                            name: 'name',
                            anchor: '95%'
                        }]
                    }]
                }, {
                    xtype: 'hidden',
                    name: 'id'
                }, {
					xtype:'numberfield',
					name:'desc',
					fieldLabel:'������',
					anchor:'95%'
				}]
            }]
        }]
    });
    new Ext.Window({
        width: 900,
        height: 400,
        frame: true,
        layout:'fit',
        constrainHeader: true,
        closeAction: 'close',
        modal: true,
        id: 'slider2.WindowAddEdit',
        items: [form],
        listeners: {
            "show": function(){
               
            }
        },
        buttonAlign: 'right',
        buttons: [{
            text: '���������',
            iconCls: 'accept',
            handler: function(){
                Ext.ux.TinyMCE.initTinyMCE();
                tinyMCE.triggerSave();
                form.getForm().submit({
                    url: 'admincp.php',
					method:'POST',
                    params: {
                        module: 'slider2',
                        task: 'save'
                    },
                    waitMsg: '���������� ���������',
                    success: function(){
                        Ext.getCmp('slider2.WindowAddEdit').close();
                        slider2.base.reload();
                        
                    }
                });
            }
        }]
    }).show();
}


slider2.base = new Ext.data.Store({

    proxy: new Ext.data.HttpProxy({
        url: 'admincp.php',
        method: 'POST'
    }),
    baseParams: {
        xaction: "Listing",
        module: 'slider2'
    },
    
    reader: new Ext.data.JsonReader({
        root: 'results',
        totalProperty: 'total',
        id: 'id'
    }, [{
        name: 'id',
        mapping: 'id'
    }, {
        name: 'pos',
        mapping: 'pos'
    }, {
        name: 'name',
        mapping: 'name'
    }, {
        name: 'pageLink',
        mapping: 'pageLink'
    },{
		name:'notice',
		mapping:'notice'
	}, {
        name: 'desc',
        mapping: 'desc'
    },{
    	name:'date',
    	mapping:'date'
    }, {
        name: 'link',
        mapping: 'link'
    }, {
        name: 'TitlePage',
        mapping: 'TitlePage'
    }, {
        name: 'DescPage',
        mapping: 'DescPage'
    }, {
        name: 'KeysPage',
        mapping: 'KeysPage'
    }, {
        name: 'active',
        type: 'int',
        mapping: 'active'
    }])

});





// PagingBar for articlegrid
slider2.pagingBar = new Ext.PagingToolbar({
    pageSize: 25,
    store: slider2.base,
    paramNames: {
        start: 'start',
        limit: 'limit'
    },
    displayInfo: true


});
// End
// ArtclisRowAction

slider2.RowAction = new Ext.ux.grid.RowActions({

    actions: [{
        iconCls: 'delete',
        qtip: '�������'
    }, {
        iconCls: 'edit',
        qtip: '�������������'
    }],
    widthIntercept: Ext.isSafari ? 4 : 2,
    id: 'actions'
});
slider2.RowAction.on({
    action: function(grid, record, action, row, col){
        if (action == 'delete') {
            Ext.MessageBox.confirm('', '�� ������� ��� ������ ������� ��� ������', function(btn){
                if (btn == "yes") {
                    Ext.Ajax.request({
                        url: 'admincp.php',
                        params: {
                            module: 'slider2',
                            task: 'deleteItem',
                            id: record.data.id
                        },
                        method: 'post',
                        success: function(){
                            slider2.base.reload();
                        }
                    });
                }
            })
        }
        if (action == 'edit') {
        
            slider2.addedit(record.data.id);
            Ext.getCmp('slider2.form').getForm().loadRecord(record)
            
        }
    }
});
slider2.grid = new Ext.grid.EditorGridPanel({
    store: slider2.base,
    title: '',
    frame: true,
    loadMask: true,
    id: 'slider2.grid',
    layout: 'fit',
    enableColLock: false,
    clicksToEdit: 1,
    autoWidth: true,
    columns: [{
        id: 'id',
        header: "#",
        width: 15,
        sortable: false,
        dataIndex: 'id'
    }, {
        id: 'name',
        header: "����� ���������� �����",
        width: 150,
        sortable: false,
        dataIndex: 'name'
    }, {
        id: 'sk',
        header: "������",
        width: 150,
        sortable: false,
        dataIndex: 'desc'
    }, {
        id: 'Active',
        header: '',
		sortable:false,
        dataIndex: 'active',
        width: 50,
        editor: new Ext.form.ComboBox({
            typeAhead: true,
            triggerAction: 'all',
            store: new Ext.data.SimpleStore({
                fields: ['partyValue', 'partyName'],
                data: [['1', '��������'], ['0', '�� ��������']]
            }),
            mode: 'local',
            displayField: 'partyName',
            valueField: 'partyValue',
            lazyRender: true,
            listClass: 'x-combo-list-small'
        }),
        renderer: function(value){
            if (value == 1) {
                return "��������";
            }
            return "�� ��������";
        }
        
    }, slider2.RowAction],
    
    sm: new Ext.grid.RowSelectionModel({
        singleSelect: true
    }),
    viewConfig: {
        forceFit: true
    },
    height: 150,
    bbar: slider2.pagingBar,
    plugins: slider2.RowAction,
    iconCls: 'icon-grid',
    split: true,
    tbar: [{
        text: '�������� ����� ������',
        handler: function(){
        
            slider2.addedit(0);
            
            
        },
        iconCls: 'add'
    }],
    region: 'center'

});

slider2.grid.on('afteredit', slider2.updateRecord);


// End articleGrid

slider2.view = {
    id: 'slider2',
    title: '�������',
    layout: 'fit',
    
    items: [slider2.grid]
}
init_modules[init_modules.length] = slider2.view;


Ext.apply(actions, {
    'slider2': function(){
        if (Ext.getCmp('Content').layout.activeItem.id != 'slider2') {
            Ext.getCmp('Content').layout.setActiveItem('slider2');
            if (slider2.base.data.length < 1) {
                slider2.base.load({
                    params: {
                        start: 0,
                        limit: 25
                    }
                });
            };
            
                    }
    }
});
ModulesRightMenu += '<li><i><a id="slider2" href="#">���������� �����</a></i></li>';
